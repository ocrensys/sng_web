﻿class Promotion {
    constructor() {}

    addPromotion() {
        var self =  this;
        var code = "";
        var description = "";
        var modal = $("#new_promotion");

        var data =  {
            Name : $("#PromotionName").val(),
            Description : $("#PromotionDescription").val(),
            Descount : $("#PromotionDescount").val(),
            StartAt : $("#PromotionStartAt").val(),
            EndAt : $("#PromotionEndAt").val(),
            ProductId : $("#PromotionProductId").val(),
            ComerceId : $("#ComerceId").val(),
        };
        //console.log("Promotions data: " + JSON.stringify(data));

        $.ajax({
            type: "POST",
            url: "/Promotions/AddPromotion",
            data: data,
            success: function (response) {
                //console.log(response);
                $.each(response,
                    (index, value) => {
                        code = value.code;
                        description = value.description;
                    });
                if (code == "Saved") {
                    modal.hide();
                    DevExpress.ui.notify({
                        message: description || "¡Registro guardado con exito!",
                        type: "info",
                        displayTime: 5000,
                        closeOnClick: true
                    });
                    self.loadPromotionsTable();
                }
            },
            error: function (err) {
                console.log("Error: ");
                console.log(err);
            }
        });
    }

    getDataPromotion(req, isReport) {
        var promotions_data = new DevExpress.data.CustomStore({
            load: function (loadOptions) {
                var deferred = $.Deferred();
                var OrderBy, Skip, Take, Url, data;

                if (loadOptions.sort) {
                    OrderBy = loadOptions.sort[0].selector;
                    if (loadOptions.sort[0].desc)
                        Orderby += " desc";
                }

                Skip = loadOptions.skip || 0;
                Take = loadOptions.take || 5;

                if (req === "getAll") {
                    //console.log("getAllPromotions...")
                    Url = "/Promotions/GetPromotions";
                    data = {}
                } else {
                    //console.log("get Promotions By Comerce...")
                    Url = "/Promotions/GetPromotionsByComerce";
                    data = {
                        "ComerceId": ComerceId,
                        "skip": Skip,
                        "take": Take,
                        "orderBy": OrderBy
                    }
                }

                $.ajax({
                    url: Url,
                    dataType: "json",
                    data: data,
                    success: function (result) {
                        deferred.resolve(result, { totalCount: result.length });
                        if (isReport == true) {
                            var promotions = [];
                            $.each(result, (index, p) => {
                                promotions.push(p);
                            });
                            //self.datareport = (promotions);
                            deferred.resolve(promotions, { totalCount: promotions.length });
                        } else {
                            //self.datareport = (result);
                            deferred.resolve(result, { totalCount: result.length });
                        }
                        //console.log("getpromotions...");
                        //console.log(result)
                    },
                    error: function () {
                        deferred.reject("Problemas al cargar lista de promociones");
                    },
                    timeout: 5000
                });

                return deferred.promise();
            },

        });

        return promotions_data;
    }

    loadPromotionsTable(req) {
        var self = this;
        var data = self.getDataPromotion(req, false);
        $("#table_promotions").dxDataGrid({
            dataSource: data,
            accessKey: "name",
            allowColumnResizing: true,
            wordWrapEnabled: true,
            columnResizingMode: "nextColumn",
            columnMinWidth: 50,
            showBorders: false,
            groupPanel: {
                visible: true,
                emptyPanelText: "Arrastrar y soltar una columna...",
            },
            grouping: {
                texts: {
                    groupContinuesMessage: "Continua en la siguiente página >",
                    groupContinuedMessage: "< Continua en la pagina anterios",
                }
            },
            searchPanel: {
                visible: true,
                width: 300,
                placeholder: "Buscar promoción..."
            },
            loadPanel: {
                height: 100,
                width: 300,
                text: "Cargando...",
            },
            selection: {
                mode: "single"
            },
            onSelectionChanged: function (selectedItems) {
                //var promotion = selectedItems.selectedRowsData[0];
                //showPromotionPopup(promotion);
                //console.log(promotion)
            },
            onRowClick: function (component) {
                var data = component.data
                showPromotionPopup(data);
                //console.log(component.data)
            },
            remoteOperations: false,
            paging: {
                pageSize: 5
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20]
            },
            columns: [
                {
                    dataField: "comerceId",
                    caption: "Comercio",
                    minWidth: 150,
                    visible: (req === "getAll"),
                    //calculateCellValue: function (rowData) {
                    //    var comerceName = (rowData.comerce != null) ? rowData.comerce.name : "Comercio no disponible";
                    //    return comerceName;
                    //},
                    cellTemplate: function (container, options) {
                        var data = {};
                        if (options.data.comerce != null) {
                            data = {
                                name: options.data.comerce.name,
                                urlImage: options.data.comerce.UrlLogo,
                            }
                        } else {
                            data = {
                                name: "Comercio no disponible",
                                urlImage: "/images/comerce.png",
                            }
                        }
                        $("<div>")
                            .append($("<img>", {
                                "src": data.urlImage,
                                "width": "50",
                                "alt": ""
                            }).css({ "float": "left" }))
                            .append($("<p>" + data.name + "</p>")
                                .css({"padding-top": "15px" }))
                            .appendTo(container);
                    }
                },
                {
                    dataField: "productId",
                    caption: "Producto",
                    width: 70,
                    visible: false,
                    alignment: "center"
                },
                {
                    dataField: "promotionId",
                    caption: "Código",
                    width: 70,
                    visible: true,
                    alignment: "center"
                },
                {
                    dataField: "name",
                    caption: "Promoción",
                    //width: 100,
                    sortOrder: "asc",
                },
                {
                    dataField: "description",
                    caption: "Descripción",
                    sortOrder: "desc",
                    minWidth: 200,
                },
                {
                    dataField: "startAt",
                    caption: "Inicia",
                    dataType: "date",
                    calculateCellValue: function (rowData) {
                        return dateFormatShort(rowData.startAt);
                    },
                    minWidth: 50,
                },
                {
                    dataField: "endAt",
                    caption: "Finaliza",
                    dataType: "date",
                    calculateCellValue: function (rowData) {
                        return dateFormatShort(rowData.endAt);
                    },
                    minWidth: 50,
                },
                {
                    dataField: "isActive",
                    caption: "Habilitado",
                    dataType: "boolean",
                    width: 80,
                    calculateCellValue: function (rowData) {
                        return rowData.isActive;
                    },
                },
            ],
        });
    }

    loadPromotionsReport(req) {
        var self = this;
        var data = self.getDataPromotion(req, true);
        $("#table_reportPromotions").dxDataGrid({
            dataSource: data,
            accessKey: "name",
            allowColumnResizing: true,
            wordWrapEnabled: true,
            columnResizingMode: "nextColumn",
            columnMinWidth: 50,
            showBorders: false,
            selection: {
                mode: "multiple"
            },
            "export": {
                enabled: true,
                fileName: "Reporte de Promociones",
                allowExportSelectedData: true
            },
            groupPanel: {
                visible: true,
                emptyPanelText: "Arrastrar y soltar una columna para realizar un filtro de promociones..."
            },
            remoteOperations: false,
            paging: {
                pageSize: 5
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20]
            },
            onRowPrepared: function (e) {
                var data = (e.data);
                if (data != null) {
                    console.log
                    if (getDiff(data.endAt) < 0) {
                        e.rowElement.css('background', 'linear-gradient(to bottom, #fff, rgba(249, 86, 86, 0.1))'); 
                    }
                }
            },
            columns: [
                {
                    dataField: "comerceId",
                    caption: "Comercio",
                    width: 140,
                    visible: (req === "getAll"),
                    calculateCellValue: function (rowData) {
                        var comerceName = (rowData.comerce != null) ? rowData.comerce.name : "Comercio no disponible";
                        return comerceName;
                    },
                },
                {
                    dataField: "productId",
                    caption: "Producto",
                    width: 70,
                    visible: false,
                    alignment: "center"
                },
                {
                    dataField: "promotionId",
                    caption: "Código",
                    width: 70,
                    visible: true,
                    alignment: "center"
                },
                {
                    dataField: "name",
                    caption: "Promoción",
                    sortOrder: "asc",
                    minWidth: 150,
                },
                {
                    dataField: "description",
                    caption: "Descripción",
                    sortOrder: "desc",
                    minWidth: 150,
                },
                {
                    dataField: "startAt",
                    caption: "Inicia",
                    width: 90,
                    dataType: "date",
                    calculateCellValue: function (rowData) {
                        return dateFormatShort(rowData.startAt);
                    },
                },
                {
                    dataField: "endAt",
                    caption: "Vence",
                    dataType: "date",
                    width: 90,
                    calculateCellValue: function (rowData) {
                        return dateFormatShort(rowData.endAt);
                    },
                },
                {
                    dataField: "isActive",
                    caption: "Finaliza en",
                    //dataType: "boolean",
                    width: 100,
                    calculateCellValue: function (rowData) {
                        var diff = getDiff(rowData.endAt)
                        return ((diff > 0) ? (diff + " diaz") : "Expirado");
                    },
                },
            ],
        });
    }
}

function getDiff(endAt) {
    var now = new Date();
    var end = new Date(endAt);
    return Math.floor((Date.UTC(end.getFullYear(), end.getMonth(), end.getDate()) - Date.UTC(now.getFullYear(), now.getMonth(), now.getDate())) / (1000 * 60 * 60 * 24));
}
