﻿class Royaltiy {
    constructor() { }

    addRoyalty() {
        var self = this;
        var code = "";
        var description = "";
        var modal = UIkit.modal($("#new_royalty"));

        var data = {
            Name: $("#RoyaltyName").val(),
            Value: $("#RoyaltyValue").val(),
            Concept: $("#RoyaltyConcept").val(),
            Count: $("#RoyaltyCount").val(),
            Description: $("#RoyaltyDescription").val(),
            ComerceId: $("#ComerceId").val(),
        };

        $.ajax({
            type: "POST",
            url: "/Royalties/AddRoyalty",
            data: data,
            success: function (response) {
                console.log(response);
                $.each(response,
                    (index, value) => {
                        code = value.code;
                        description = value.description;
                    });
                if (code == "Saved") {
                    modal.hide();
                    DevExpress.ui.notify({
                        message: description || "¡Registro guardado con exito!",
                        type: "info",
                        displayTime: 5000,
                        closeOnClick: true
                    });
                    self.loadRoyaltiesTable();
                }
            },
            error: function (err) {
                console.log("Error: ");
                console.log(err);
            }
        });
    }
    
    getData(req, isReport) {
        var self = this;
        var royalties_data = new DevExpress.data.CustomStore({
            load: function (loadOptions) {
                var deferred = $.Deferred(),
                    args = {};
                var OrderBy, Skip, Take, Url, data;
                if (loadOptions.sort) {
                    args.orderby = loadOptions.sort[0].selector;
                    OrderBy = loadOptions.sort[0].selector;
                    if (loadOptions.sort[0].desc)
                        args.orderby += " desc";
                }

                Skip = loadOptions.skip || 0;
                Take = loadOptions.take || 10;
                OrderBy = loadOptions.orderby;

                args.skip = loadOptions.skip || 0;
                args.take = loadOptions.take || 10;
                args.orderby = loadOptions.orderby;

                if (req === "getAll") {
                    //console.log("getAllProducts...")
                    Url = "/Royalties/GetRoyalties";
                } else {
                    //console.log("getProductsByComerce...")
                    Url = "/Royalties/GetRoyaltiesByComerce";
                    args.comerceId = ComerceId;
                }

                $.ajax({
                    url: Url,
                    dataType: "json",
                    data: args,
                    success: function (result) {
                        //deferred.resolve(result, { totalCount: result.length });
                        if (isReport == true) {
                            var royalties = [];
                            $.each(result, (index, r) => {
                                if(r.delivered)
                                    royalties.push(r);
                            });
                            //self.datareport = (promotions);
                            deferred.resolve(royalties, { totalCount: royalties.length });
                        } else {
                            //self.datareport = (result);
                            deferred.resolve(result, { totalCount: result.length });
                        }
                        //console.log(result);
                    },
                    error: function () {
                        deferred.reject("Problemas al cargar lista de regalias");
                    },
                    timeout: 5000
                });

                return deferred.promise();
            },

        });

        return royalties_data;
    }

    loadRoyaltiesTable(req) {
        var self = this;
        var data = self.getData(req, false);

        $("#table_royalties").dxDataGrid({
            dataSource: data,
            accessKey: "description",
            allowColumnResizing: true,
            columnResizingMode: "nextColumn",
            columnMinWidth: 50,
            wordWrapEnabled: true,
            showBorders: false,
            groupPanel: {
                visible: true,
                emptyPanelText: "Arrastrar y soltar una columna...",
            },
            grouping: {
                texts: {
                    groupContinuesMessage: "Continua en la siguiente página >",
                    groupContinuedMessage: "< Continua en la pagina anterios",
                }
            },
            searchPanel: {
                visible: true,
                width: 300,
                placeholder: "Buscar regalias..."
            },
            loadPanel: {
                height: 100,
                width: 300,
                text: "Cargando...",
            },
            selection: {
                mode: "single"
            },
            onSelectionChanged: function (selectedItems) {
                //var data = selectedItems.selectedRowsData[0];
                //showRoyaltyPopup(data);
                //console.log(data)
            },
            onRowClick: function (component) {
                var data = component.data
                showRoyaltyPopup(data);
                //console.log(component.data)
            },
            remoteOperations: false,
            paging: {
                enabled: true,
                pageSize: 10
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20]
            },
            columns: [
                {
                    dataField: "comerceId",
                    caption: "Comercio",
                    minWidth: 150,
                    visible: (req === "getAll"),
                    //calculateCellValue: function (rowData) {
                    //    var comerceName = (rowData.comerce != null) ? rowData.comerce.name : "Comercio no disponible";
                    //    return comerceName;
                    //},
                    cellTemplate: function (container, options) {
                        var data = {};
                        if (options.data.comerce != null) {
                            data = {
                                name: options.data.comerce.name,
                                urlImage: options.data.comerce.UrlLogo,
                            }
                        } else {
                            data = {
                                name: "Comercio no disponible",
                                urlImage: "/images/comerce.png",
                            }
                        }
                        $("<div>")
                            .append($("<img>", {
                                "src": data.urlImage,
                                "width": "50",
                                "alt": ""
                            }).css({ "float": "left" }))
                            .append($("<p>" + data.name + "</p>")
                                .css({ "padding-top": "0px" }))
                            .appendTo(container);
                    }
                },
                {
                    dataField: "royaltyId",
                    caption: "Código",
                    width: 95,
                    alignment: "center",
                    sortOrder: "asc",
                },
                {
                    dataField: "name",
                    caption: "Nombre",
                    minWidth: 100,
                    sortOrder: "asc",
                    allowSorting: true,
                },
                {
                    dataField: "concept",
                    caption: "Concepto",
                    sortOrder: "desc",
                    alignment: "justify",
                    minWidth: 200,
                },
                {
                    dataField: "description",
                    caption: "Descripción",
                    sortOrder: "desc",
                    alignment: "justify",
                    minWidth: 200,
                },

                {
                    dataField: "count",
                    caption: "Cantidad",
                    width: 80,
                },
                {
                    dataField: "value",
                    caption: "Valor monetario",
                    width: 100,
                    alignment: "right",
                    calculateCellValue: function (rowData) {
                        return "C$ " + rowData.value;
                    },
                },
                {
                    dataField: "delivered",
                    caption: "Entregados",
                    dataType: "boolean",
                    calculateCellValue: function (rowData) {
                        return rowData.delivered;
                    },
                    width: 80,
                },
                {
                    dataField: "isActive",
                    caption: "Habilitado",
                    dataType: "boolean",
                    calculateCellValue: function (rowData) {
                        return rowData.isActive;
                    },
                    width: 80,
                },
            ],
        });
        
    }

    loadRoyaltiesReport(req) {
        var self = this;
        var data = self.getData(req, true);

        $("#table_royaltiesReport").dxDataGrid({
            dataSource: data,
            accessKey: "description",
            allowColumnResizing: true,
            columnResizingMode: "nextColumn",
            columnMinWidth: 50,
            wordWrapEnabled: true,
            showBorders: false,
            loadPanel: {
                height: 100,
                width: 300,
                text: "Cargando...",
            },
            searchPanel: {
                visible: false,
                width: 300,
                placeholder: "Buscar regalias..."
            },
            selection: {
                mode: "multiple"
            },
            "export": {
                enabled: true,
                fileName: "Regalias Entregadas",
                allowExportSelectedData: true
            },
            onSelectionChanged: function (selectedItems) {
                //var data = selectedItems.selectedRowsData[0];
                //showRoyaltyPopup(data);
                //console.log(data)
            },
            remoteOperations: false,
            paging: {
                enabled: true,
                pageSize: 5
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20]
            },
            columns: [
                {
                    dataField: "comerceId",
                    caption: "Comercio",
                    minWidth: 150,
                    visible: (req === "getAll"),
                    calculateCellValue: function (rowData) {
                        var comerceName = (rowData.comerce != null) ? rowData.comerce.name : "Comercio no disponible";
                        return comerceName;
                    },
                },
                {
                    dataField: "royaltyId",
                    caption: "Código",
                    width: 95,
                    alignment: "center",
                    sortOrder: "asc",
                },
                {
                    dataField: "name",
                    caption: "Nombre",
                    minWidth: 100,
                    sortOrder: "asc",
                    allowSorting: true,
                },
                {
                    dataField: "updatedAt",
                    caption: "Actualizado",
                    width: 100,
                    customizeText: function (cellInfo) {
                        return dateFormatShort(cellInfo.value);
                    }
                },
                {
                    dataField: "count",
                    caption: "Cantidad",
                    width: 80,
                    visible: false,
                },
                {
                    dataField: "concept",
                    caption: "Concepto",
                    sortOrder: "desc",
                    alignment: "justify",
                    minWidth: 300,
                },
                {
                    dataField: "description",
                    caption: "Descripción",
                    sortOrder: "desc",
                    alignment: "justify",
                    minWidth: 300,
                    visible: false,
                },
                {
                    dataField: "value",
                    caption: "Valor monetario",
                    width: 100,
                    alignment: "right",
                    visible: false,
                    calculateCellValue: function (rowData) {
                        return "C$ " + rowData.value;
                    },
                },
                {
                    dataField: "isActive",
                    caption: "Habilitado",
                    dataType: "boolean",
                    calculateCellValue: function (rowData) {
                        return rowData.isActive;
                    },
                    width: 80,
                    visible: false,
                },
            ],
        });

    }
}