﻿class Activities {
    constructor(){}

    loadLogsTable(req) {
        var data = new DevExpress.data.CustomStore({
            load: function (loadOptions) {
                var deferred = $.Deferred(),
                    args = {};
                var OrderBy, Skip, Take, Url, data;

                if (loadOptions.sort) {
                    args.orderby = loadOptions.sort[0].selector;
                    OrderBy = loadOptions.sort[0].selector;
                    if (loadOptions.sort[0].desc)
                        args.orderby += " desc";
                }

                if (req === "getAll") {
                    console.log("get all logs...")
                    Url = "/Comerces/GetLogs";
                } else {
                    Url = "/Comerces/GetLogsByComerce";
                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 5;
                    args.orderby = loadOptions.orderby || "desc";
                    args.comerceId = ComerceId;
                }
                console.log("args: " + JSON.stringify(args));

                $.ajax({
                    url: Url,
                    dataType: "json",
                    data: args,
                    success: function (result) {
                        deferred.resolve(result, { totalCount: result.length });
                        console.log("All logs available");
                        console.log(result);
                    },
                    error: function () {
                        deferred.reject("Problemas al cargar lista de Logs");
                    },
                    timeout: 5000
                });

                return deferred.promise();
            },

        });

        $("#logs_table").dxDataGrid({
            dataSource: data,
            allowColumnResizing: true,
            columnResizingMode: "nextColumn",
            columnMinWidth: 50,
            wordWrapEnabled: true,
            showBorders: false,
            searchPanel: {
                visible: true,
                width: 300,
                placeholder: "Buscar registro de actividad..."
            },
            "export": {
                enabled: true,
                fileName: "Registro de actividad",
                allowExportSelectedData: true
            },
            selection: {
                mode: "single"
            },
            onSelectionChanged: function (selectedItems) {
                var data = selectedItems.selectedRowsData[0];
                showProductPopup(data);
                console.log(data)
            },
            onRowPrepared: function (e) {
                var data = (e.data);
                if (data != null) {
                    switch (data.level){
                        case 1: e.rowElement.css('background', 'linear-gradient(to bottom, #fff, rgba(86, 238, 249, 0.1))'); break;
                        case 2: e.rowElement.css('background', 'linear-gradient(to bottom, #fff, rgba(247, 249, 86, 0.1))'); break;
                        case 3: e.rowElement.css('background', 'linear-gradient(to bottom, #fff, rgba(249, 86, 86, 0.1))'); break;
                    }
                }
            },
            remoteOperations: false,
            paging: {
                enabled: true,
                pageSize: 5
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20]
            },
            columns: [
                {
                    dataField: "firstname",
                    caption: "Nombre del Usuario",
                    calculateCellValue: function (rowData) {
                        return rowData.firstname + " " + rowData.lastName;
                    },
                    idth: 150,
                },
                {
                    dataField: "commerce",
                    caption: "Comercio",
                    minWidth: 150,
                },
                {
                    dataField: "date",
                    caption: "Fecha de Registro",
                    //alignment: "justify",
                    width: 150,
                    calculateCellValue: function (rowData) {
                        return dateTimeFormatShort(rowData.date);
                    },
                },
                {
                    dataField: "message",
                    caption: "Mensaje",
                    minWidth: 300,
                },
                {
                    dataField: "action",
                    caption: "Tipo de Registro",
                    width: 200,
                    calculateCellValue: function (rowData) {
                        switch (rowData.action) {
                            case 11: return "Ingreso de un nuevo registro"; break;
                            case 22: return "Actualización de un registro"; break;
                            case 33: return "Eliminación de un registro"; break;

                        }
                        return "Acción no definida";
                    },
                },
                {
                    dataField: "level",
                    caption: "Nivel",
                    width: 100,
                    calculateCellValue: function (rowData) {
                        switch (rowData.level) {
                            case 1: return "Información"; break;
                            case 2: return "Aviso"; break;
                            case 3: return "Error"; break;

                        }
                        return "Nivel no definido";
                    },
                },
            ],
        });
    }

    setChart(container, title) {
        //var chart = $("#chart").dxChart({
        var chart = container.dxChart({
            palette: "violet",
            dataSource: dataSource,
            commonSeriesSettings: {
                type: types[0],
                argumentField: "year"
            },
            commonAxisSettings: {
                grid: {
                    visible: true
                }
            },
            margin: {
                bottom: 20
            },
            series: [
                { valueField: "smp", name: "SMP" },
                { valueField: "mmp", name: "MMP" },
                { valueField: "cnstl", name: "Cnstl" },
                { valueField: "cluster", name: "Cluster" }
            ],
            tooltip: {
                enabled: true
            },
            legend: {
                verticalAlignment: "top",
                horizontalAlignment: "right"
            },
            "export": {
                enabled: true
            },
            argumentAxis: {
                label: {
                    format: {
                        type: "decimal"
                    }
                },
                allowDecimals: false,
                axisDivisionFactor: 60
            },
            //title: "Architecture Share Over Time (Count)"
            title: title
        }).dxChart("instance");

    }
}