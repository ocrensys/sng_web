﻿class Vacant {
    constructor() {

    }

    addVacant() {
        var self = this;
        var modal = UIkit.modal($("#new_vacant"));
        var code = "";
        var description = "";

        var data = {
            Name : $("#VacantName").val(),
            Description : $("#VacantDescription").val(),
            Area : $("#VacantArea").val(),
            Cargo : $("#VacantCargo").val(),
            AvailableVacants : $("#VacantAvailableVacants").val(),
            Schedule : $("#Vacantschedule option:selected").text(),
            Experience : $("#VacantExperience").val(),
            Genre : $("#VacantGenre option:selected").text(),
            Age : $("#VacantAge").val(),
            MaxSalary : $("#VacantMaxSalary").val(),
            MinSalary : $("#VacantMinSalary").val(),
            Department : $("#VacantDepartment option:selected").text(),
            Country : $("#VacantCountry option:selected").text(),
            ComerceId : $("#ComerceId").val(),
        };
        console.log("vacant data: " + JSON.stringify(data));

        $.ajax({
            type: "POST",
            url: "/Vacants/AddVacant/",
            data: data,
            success: function (response) {
                //console.log(response);
                $.each(response,
                    (index, value) => {
                        code = value.code;
                        description = value.description;
                    });
                if (code == "Saved") {
                    modal.hide();
                    DevExpress.ui.notify({
                        message: description || "¡Registro guardado con exito!",
                        type: "info",
                        displayTime: 5000,
                        closeOnClick: true
                    });
                    self.loadVacantsTable();
                }
            },
            error: function (err) {
                console.log("Error: ");
                console.log(err);
            }
        });
    }

    loadVacantsTable(req) {
        var vacants_data = new DevExpress.data.CustomStore({
            load: function (loadOptions) {
                var deferred = $.Deferred();
                var OrderBy, Skip, Take, Url, data;

                var groupOptions = loadOptions.group ? JSON.stringify(loadOptions.group) : ""; // Getting group options
                if (loadOptions.sort) {
                    OrderBy = loadOptions.sort[0].selector;
                    if (loadOptions.sort[0].desc)
                        OrderBy += " desc";
                }

                Skip = loadOptions.skip || 0;
                Take = loadOptions.take || 5;

                if (req === "getAll") {
                    //console.log("getAll...")
                    Url = "/Vacants/GetVacants";
                    data = {}
                } else {
                    //console.log("getByComerce...")
                    Url = "/Vacants/GetVacantsByComerce";
                    data = {
                        "ComerceId": ComerceId,
                        "skip": Skip,
                        "take": Take,
                        "orderBy": OrderBy
                    }
                }

                $.ajax({
                    url: Url,
                    dataType: "json",
                    data: data,
                    success: function (result) {
                        deferred.resolve(result, { totalCount: result.length });
                        //console.log("GetVacants...")
                        //console.log(result)
                    },
                    error: function () {
                        deferred.reject("Problemas al cargar lista de vacantes");
                    },
                    timeout: 5000
                });
                return deferred.promise();
                

                
            },

            insert: function (values) {
                var data = {
                    "Name": values.name,
                    "Description": values.description,
                    "Price": values.price,
                    "UrlImage": values.urlImage || "",
                    "ComerceId": values.comerceId || 3
                };

                return $.ajax({
                    url: "/Products/AddProduct",
                    method: "POST",
                    data: data,
                    success: function (res) {
                        $.each(res, (index, value) => {
                            DevExpress.ui.notify(value.description, "success", 3000);
                        });
                    },
                    error: function (err) {
                        console.log("error...  " + err)
                        $.each(err, (index, value) => {
                            DevExpress.ui.notify("Error al ingresar nuevo cupón", "error", 3000);
                        });
                    }
                })
            },

            update: function (key, values) {
                var product = JSON.parse(JSON.stringify(key));
                var data = {
                    "ProductId": product.productId,
                    "Name": (values.name != null && values.name != "") ? values.name : product.name,
                    "Description": (values.description != null && values.description != "") ? values.description : product.description,
                    "Price": (values.price != null && values.price >= 0) ? values.price : product.price,
                    "UrlImage": (values.urlImage != null && values.urlImage != "") ? values.urlImage : product.urlImage,
                    "CreatedAt": (values.createdAt != null && values.createdAt != "") ? values.createdAt : product.createdAt,
                    "IsActived": (values.isActived != null) ? values.isActived : product.isActived,
                    "ComerceId": (values.comerceId != null && values.comerceId >= 0) ? values.comerceId : product.comerceId
                };

                return $.ajax({
                    url: "/Products/UpdateProduct/" + product.productId,
                    method: "PUT",
                    data: data,
                    success: function (res) {
                        $.each(res, (index, value) => {
                            DevExpress.ui.notify(value.description, "info", 3000);
                        });
                    },
                    error: function (err) {
                        $.each(err, (index, value) => {
                            DevExpress.ui.notify("Error al actualizar datos del cupón", "error", 3000);
                        });
                    }
                })
            },

            remove: function (key) {
                var product = JSON.parse(JSON.stringify(key));

                return $.ajax({
                    url: "/Products/DeleteProduct/" + product.productId,
                    method: "DELETE",
                    data: {
                        "id": product.productId,
                        "name": product.name
                    },
                    success: function (res) {
                        $.each(res, (index, value) => {
                            console.log(value.description)
                            DevExpress.ui.notify(value.description, "warning", 3000);
                        });
                    },
                    error: function (err) {
                        $.each(err, (index, value) => {
                            DevExpress.ui.notify("Error al eliminar cupon", "error", 3000);
                        });
                    }
                })
            },

        });

        $("#table_vacants").dxDataGrid({
            dataSource: vacants_data,
            accessKey: "name",
            allowColumnResizing: true,
            columnResizingMode: "nextColumn",
            wordWrapEnabled: true,
            columnMinWidth: 100,
            showBorders: false,
            groupPanel: {
                visible: true,
                emptyPanelText: "Arrastrar y soltar una columna...",
            },
            grouping: {
                texts: {
                    groupContinuesMessage: "Continua en la siguiente página >",
                    groupContinuedMessage: "< Continua en la pagina anterios",
                }
            },
            searchPanel: {
                visible: true,
                width: 300,
                placeholder: "Buscar vacante..."
            },
            loadPanel: {
                height: 100,
                width: 300,
                text: "Cargando...",
            },
            selection: {
                mode: "single"
            },
            onSelectionChanged: function (selectedItems) {
                //var vacant = selectedItems.selectedRowsData[0];
                //showVacantPopup(vacant);
                //console.log(vacant)
            },
            onRowClick: function (component) {
                var data = component.data
                showVacantPopup(data);
                //console.log(component.data)
            },
            remoteOperations: false,
            paging: {
                pageSize: 5
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20]
            },
            editing: {
                mode: "popup",
                //allowUpdating: true,
                //allowDeleting: true,
                //allowAdding: true,
                popup: {
                    title: "Cupón",
                    showTitle: true,
                    width: 700,
                    height: 345,
                    position: {
                        my: "top",
                        at: "top",
                        of: window
                    },
                    //contentTemplate: function (contentElement) {
                    //    contentElement.append(vacants_data);
                    //    console.log(vacants_data);
                    //    console.log("..................................")
                    //    console.log(contentElement);
                    //    var template = "";
                    //        //+ "<div>                                                                                                                                           "
                    //        //+ "    <div class='uk-card-default uk-card-small uk-card-body'>                                                                            "
                    //        //+ "        <h4 class='uk-card-title'>" + vacant.name + "</h4>                                                                                    "
                    //        //+ "        <p> <strong>Cargo: </strong>" + vacant.cargo + "</p>                                                                          "
                    //        //+ "        <p> <strong>Área: </strong>" + vacant.area + "</p>                                                                     "
                    //        //+ "        <p> <strong>Horario: </strong>" + vacant.schedule + "</p>                                                                     "
                    //        //+ "        <p> <strong>Departamento: </strong>" + vacant.department + "</p>                                                                     "
                    //        //+ "        <p> <strong>Experiencia: </strong>" + vacant.department + "</p>                                                                     "
                    //        //+ "        <p> <strong>Vacantes: </strong>" + vacant.department + "</p>                                                                     "
                    //        //+ "        <p> <strong>Genero: </strong>" + vacant.department + "</p>                                                                     "
                    //        //+ "        <p> <strong>Salario máximo: </strong>" + vacant.maxSalary + "</p>                                                                     "
                    //        //+ "        <p> <strong>Salario Mínimo: </strong>" + vacant.minSalary + "</p>                                                                     "
                    //        //+ "        <p> <strong>Detalle: </strong>" + vacant.description + "</p>                                                                     "
                    //        //+ "        <p> <strong>Habilitado: </strong> <label><input class='uk-checkbox' type='checkbox' checked=" + vacant.isActive + "></label></p>     "
                    //        //+ "    </div>                                                                                                                                      "
                    //        //+ "    <div class='uk-modal-footer uk-text-right' >                                                                                                "
                    //        //+ "         <a href='/Vacants/Delete/" + vacant.vacantId + "' class='uk-button uk-button-danger  uk-button-small' type= 'button'> <span uk-icon='icon: trash'> Eliminar</a>"
                    //        //+ "         <a href='/Vacants/Edit/" + vacant.vacantId + "' class='uk-button uk-button-primary  uk-button-small' type= 'button'> <span uk-icon='icon: pencil'> Editar</a>"
                    //        //+ "    </div >                                                                                                                                     "
                    //        //+ "</div>                                                                                                                                          ";

                    //    return $("<div/>").append(contentElement);
                    //},
                }
            },
            columns: [
                {
                    dataField: "comerceId",
                    caption: "Comercio",
                    minWidth: 150,
                    visible: (req === "getAll"),
                    //calculateCellValue: function (rowData) {
                    //    var comerceName = (rowData.comerce != null) ? rowData.comerce.name : "Comercio no disponible";
                    //    return comerceName;
                    //},
                    cellTemplate: function (container, options) {
                        var data = {};
                        if (options.data.comerce != null) {
                            data = {
                                name: options.data.comerce.name,
                                urlImage: options.data.comerce.UrlLogo,
                            }
                        } else {
                            data = {
                                name: "Comercio no disponible",
                                urlImage: "/images/comerce.png",
                            }
                        }
                        $("<div>")
                            .append($("<img>", {
                                "src": data.urlImage,
                                "width": "50",
                                "alt": ""
                            }).css({ "float": "left" }))
                            .append($("<p>" + data.name + "</p>")
                                .css({ "padding-top": "15px" }))
                            .appendTo(container);
                    }
                },
                {
                    dataField: "vacantId",
                    caption: "Código",
                    width: 70,
                    visible: true,
                    alignment: "center"
                },
                {
                    dataField: "name",
                    caption: "Nombre",
                    sortOrder: "desc",
                },
                {
                    dataField: "cargo",
                    caption: "Cargo",
                    sortOrder: "desc",
                },
                {
                    dataField: "area",
                    caption: "Área",
                    sortOrder: "desc",
                },
                {
                    dataField: "schedule",
                    caption: "Horario",
                    sortOrder: "desc",
                },
                {
                    dataField: "department",
                    caption: "Departamento",
                    sortOrder: "desc",
                    visible: false,
                },
                {
                    dataField: "experience",
                    caption: "Experiencia",
                    sortOrder: "desc",
                    visible: false,
                },
                {
                    dataField: "availableVacants",
                    caption: "Vacantes",
                    sortOrder: "desc",
                    width: 120,
                    alignment: "center"
                },
                {
                    dataField: "genre",
                    caption: "Genero",
                    sortOrder: "desc",
                    visible: false,
                },  
                {
                    dataField: "maxSalary",
                    caption: "Salario Maximo",
                    format: "currency",
                    width: 100,
                    sortOrder: "desc",
                    visible: false,
                    alignment: "right",
                    calculateCellValue: function (rowData) {
                        return "C$ " + rowData.maxSalary;
                    },
                },
                {
                    dataField: "minSalary",
                    caption: "Salario Minimo",
                    format: "currency",
                    width: 100,
                    sortOrder: "desc",
                    alignment: "right",
                    calculateCellValue: function (rowData) {
                        return "C$ " + rowData.minSalary;
                    },
                },
                {
                    dataField: "description",
                    caption: "Detalle",
                    sortOrder: "desc",
                    visible: false,
                },
                {
                    dataField: "isActive",
                    caption: "Habilitado",
                    dataType: "boolean",
                    width: 80,
                    calculateCellValue: function (rowData) {
                        return rowData.isActive;
                    },
                },
            ],
        });
    }

    getVacant() {

    }

    getAllVacants() {

    }

    getVacantsByComerce() {

    }

    setData(response) {

    }

    clear() {

    }
}