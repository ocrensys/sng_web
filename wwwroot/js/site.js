﻿// Write your JavaScript code.

//class instances
var users = "";
var comerce = new Comerce("", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
var category = new Category("", "", "", "");
var owner = new Owner("", "", "", "", "", "");
var product = new Product("", "", "", "", "", "", "", "", "");
var promotion = new Promotion();
var cupon = new Cupon();
var vacant = new Vacant();
var royalty = new Royaltiy();
var comment = new UserComment();
var activity = new Activities();
var user = new User();


//config general methods
var setPercentField = (input) => {
    input.inputmask("99.99", {
        //positionCaretOnClick: "radixFocus",
        radixPoint: ".",
        //_radixDance: true,
        //numericInput: true,
        placeholder: "0",
    });
}

var setIntegerField = (input) => {
    input.inputmask("99", {
        //positionCaretOnClick: "radixFocus",
        //radixPoint: ".",
        //_radixDance: true,
        //numericInput: true,
        placeholder: "0",
    });
}

var dateFormat = (str) => {
    var d = new Date(str);
    var month = ['Enero', 'Frebrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    var day = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes"];
    var dd = day[d.getDay()] || "";
    var date = dd + " " + d.getDate() + " de " + month[d.getMonth()] + " del " + d.getFullYear();
    var time = d.toLocaleTimeString().toLowerCase();

    var res = (date + ", a las " + time);
    //18 de Mayo del 2018, a las 0:00:00
    //console.log(res);

    return res;
}

var dateTimeFormatShort = (str) => {
    var d = new Date(str);
    var time = d.toLocaleTimeString().toLowerCase();
    var res = d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear() + ", " + time;
    //18/05/2018
    //console.log(res);
    return res;
}

var dateFormatShort = (str) => {
    var d = new Date(str);
    var res = d.getDate() + "/" + (d.getMonth()+1) + "/" + d.getFullYear();
    //18/05/2018
    //console.log(res);
    return res;
}


var dateFormatMonth = (str) => {
    var d = new Date(str);
    var res = (d.getMonth() + 1);
    //18/05/2018
    //console.log(res);
    return res;
}

//config comerce methods ............................................................................
var addComerce = () => {
    comerce.addComerce("/Comerces/AddComerce");
}

var setRating= () => {
    category.setRating();
}

var getCategories = () => {
    category.getCategories();
}

var getOwner = () => {
    owner.getOwners();
}

var getOnlyComerce = () => {
    comerce.getOnlyComerce();
}

var getComerceById = (id) => {
    comerce.getComerceById(id);
}

var loadComercesTable = (option) => {
    comerce.loadComercesTable(option);
}

var getComerceFilter = (pageNumber) => {
    var value = $("#search_comerce").val();
    comerce.getComerceFilter(pageNumber, value);
}

var readURLComerce = (input, banner) => {
    var render = $("#"+banner);
    comerce.readURLComerce(input, render);
}

var setTabPanel = () => {
    comerce.setTabPanel();
}

//config product methods ............................................................................
var addProduct = (form) => {
    product.addProduct("/Products/AddProduct", form);
}

//Metodo de prueba para subir fotos
var uploadFilesAjax = () => {
    product.UploadFilesAjax("/Products/AddProduct");
}

var getProducts = () => {
    product.getProducts();
}

var loadProductsTable = (req) => {
    product.loadProductsTable(req);
}

var readURLProduct = (input, banner) => {
    var productBanner = $("#"+banner);
    product.readURLProduct(input, productBanner);
}

//config prmotion methods ............................................................................
var addPromotion = () => {
    promotion.addPromotion();
}

var loadPromotionsTable = (req) => {
    promotion.loadPromotionsTable(req);
}

//config cupon methods ............................................................................
var addCupon = () => {
    cupon.addCupon();
}

var loadCuponsTable = (req) => {
    cupon.loadCuponsTable(req);
}

var loadCuponsReport= (req) => {
    cupon.loadCuponsReport(req);
}

//config vacant methods ............................................................................
var addVacant = () => {
    vacant.addVacant();
}

var loadVacantsTable = (req) => {
    vacant.loadVacantsTable(req);
}

//config category methods ............................................................................
var addCategory = () => {
    category.addCategory();
}

var laodCategoryTable = () => {
    category.laodCategoryTable();
}

//config royalty methods ............................................................................
var addRoyalty = () => {
    royalty.addRoyalty();
}

var loadRoyaltiesTable = (req) => {
    royalty.loadRoyaltiesTable(req);
}

//config comment methods ............................................................................

var loadCommentsTable = (req) => {
    comment.loadCommentsTable(req);
}

//........................................................................................
//                    dialogs funtions to render details
//........................................................................................
function showProductPopup(product) {
    var popup = null;
    var popupOptions = {
        maxWidth: 700,
        maxHeight: 450,
        resizeEnabled: true,
        showTitle: true,
        title: "Detalle del producto",
        visible: true,
        shading: true,
        dragEnabled: true,
        closeOnOutsideClick: true,
        contentTemplate: function () {
            //console.log(product)
            var comerceName = (product.comerce != null) ? product.comerce.name : "Comercio no disponible";
            var currency = product.currency != null ? product.currency : "C$";

            var temp = ''
                + '<div class="box-body" >                                                                                                     '
                + '    <h3 class="comerceName">                                        '
                + '            ' + comerceName + '                                                '
                + '    </h3>                                                                                                                   '
                + '    <div class="uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>                                                   '
                + '        <div class="uk-card-media-left uk-cover-container">                                                                 '
                + '            <img src="' + product.urlImage +'" alt="" uk-cover>                                                                               '
                + '            <canvas width="600" height="400"></canvas>                                                                      '
                + '        </div>                                                                                                              '
                + '            <div>                                                                                                           '
                + '                <div class="uk-card-body">                                                                                  '
                + '                                                                                                                            '
                + '                    <p class="pull-right">                                                                                  '
                + '                         <span class="uk-label"><i class="fa fa-tags"></i> ' + "Nuevo" + '</span>  <br />                                                                                              '
                + '                    </p>                                                                                                    '
                + '                                                                                                                            '
                + '                    <h4 style="margin-top: 0"  class="uk-text-primary"><small>Código : #' + product.productId + '</small> ─ ' + product.name + '</h4>                                                         '
                + '                                                                                                                            '
                + '                    <p>' + product.description +'</p>  '
                + '                    <h5 style="margin-bottom: 0" class="uk-text-primary">                                                                            '
                + '                        <i class="fa fa-shopping-cart margin-r5"></i> '+ currency + '  ' + product.price +' <small>Precio de compra</small>                 '
                + '                    </h5>                                                                                                    '
                + '                </div>                                                                                                      '
                + "                 <div class='pull-right' > "
                + "                 <hr />"
                + "                 <div class='uk-display-inline'>"
                + "                     <a href='/Products/Edit/" + product.productId + "'class='uk-button uk-button-small uk-button-default'> <span uk-icon='icon: pencil'></span> Editar</a> |"
                + "                     <a href='/Products/Delete/" + product.productId + "' class='uk-button uk-button-small uk-button-danger'> <span uk-icon='icon: trash'></span> Eliminar</a>"
                + "                  </div>"
                + "                 <br /><br />"
                + "                 </div >"
                + '             </div>                                                                                                          '
                + '        </div>                                                                                                              '
                + '</div>';
            



            return $("<div/>").append(temp);
        },
    };

    if (popup) {
        $("#popup_product").remove();
    }
    var $popupContainer = $("<div />")
        .addClass("popup")
        .appendTo($("#popup_product"));
    popup = $popupContainer.dxPopup(popupOptions).dxPopup("instance");
    popup.show();
};

function showPromotionPopup(promotion) {
    var popup = null;
    var popupOptions = {
        maxWidth: 600,
        maxHeight: 400,
        resizeEnabled: true,
        showTitle: true,
        title: "Detalle de la promoción",
        visible: true,
        dragEnabled: true,
        closeOnOutsideClick: true,
        contentTemplate: function () {
            //console.log(promotion)
            var comerceName = (promotion.comerce != null) ? promotion.comerce.name : "Comercio no disponible";
            var productName = (promotion.products != null) ? promotion.products.name : "Producto no asignado";
            var checked = promotion.isActive == true ? "checked" : "";
 
            var temp = ''
                + '<div class="">                                            '
                + '    <div class="box-body" >                                            '
                + '        <h3 class="comerceName">                                        '
                + '            ' + comerceName + '                                                '
                + '        </h3>                                                          '
                + '        <div class="media">                                            '
                + '            <div class="media-body">                                   '
                + '                <div class="clearfix">                                 '
                + '                    <p class="pull-right">                             '
                + '                         <span class="uk-label"><i class="fa fa-tags"></i>                          '
                + '                            '+ "Nuevo" +'                                '
                + '                        </span>                                        '
                + '                    </p>                                               '
                + '                                                                       '
                + '                    <h4 style="margin-top: 0" class="uk-text-primary">                         '
                + '                         <small>Código : #' + promotion.promotionId + '</small> ─ ' + promotion.name + '                  '
                + '                    </h4>                                              '


                + '                     <div class="">                                                 '
                + '                         <div class="col-sm-6">                                                 '
                + '                             <dl class="uk-description-list ">                             '
                + '                                 <dt>Detalle</dt>                                   '
                + '                                 <dd>' + promotion.description + ' %</dd>                          '
                + '                                 <dt>Opciones</dt>                                   '
                + '                                 <dd>'
                + '                                     <div class="checkbox checbox-switch switch-primary">  Habilitado:                               '
                + '                                         <label>                                                                      '
                + '                                              <input type="checkbox" ' + checked +' value="' + promotion.isActive + '" disabled />      '
                + '                                              <span></span>                                                                '
                + '                                         </label>                                                                      '
                + '                                     </div>                                                 '
                + '                                 </dd>                          '
                + '                             </dl>                                                       '
                + '                         </div>                                                 '
                + '                         <div class"col-sm-6">                                                 '
                + '                             <dl class="uk-description-list ">                             '
                + '                                 <dt>Inicia</dt>                                   '
                + '                                 <dd class="uk-text-warning"><i class="date fa fa-calendar-alt margin-r5"></i>' + dateFormat(promotion.startAt) + '</dd>                          '
                + '                                 <dt>Finaliza</dt>                                   '
                + '                                 <dd class="uk-text-warning"><i class="date fa fa-calendar-alt margin-r5"></i>' + dateFormat(promotion.endAt) + '</dd>                          '
                + '                             </dl>                                                       '
                + '                         </div>                                                 '
                + '                     </div>                                                 '
                                    //col-sm
                + '            </div>                                                     '
                + '        </div>                                                         '
                + '         <div class="pull-right">                                                                                                                                                '
                + '          <hr />                                                                                                                                                                 '
                + '             <div class="uk-display-inline">                                                                                                                                     '
                + '                 <a href="/Promotions/Edit/' + promotion.promotionId + '" class="uk-button uk-button-small uk-button-default"> <span uk-icon="icon: pencil"></span> Editar</a> | '
                + '                 <a href="/Promotions/Delete/' + promotion.promotionId + '" class="uk-button uk-button-small uk-button-danger"> <span uk-icon="icon: trash"></span> Eliminar</a> '
                + '             </div>                                                                                                                                                              '
                + '              <br /><br />                                                                                                                                                        '
                + '         </div>                                                             '
                + '    </div>                                                             '
                + '</div>                                                                 ';

            return $("<div/>").append(temp);
        },
    };

    if (popup) {
        $("#popup_promotions").remove();
    }
    var $popupContainer = $("<div />")
        .addClass("popup")
        .appendTo($("#popup_promotions"));
    popup = $popupContainer.dxPopup(popupOptions).dxPopup("instance");
    popup.show();

}

function showCuponPopup(cupon) {
    var popup = null;
    var popupOptions = {
        maxWidth: 700,
        maxHeight: 450,
        resizeEnabled: true,
        showTitle: true,
        title: "Detalle del cupón",
        visible: true,
        dragEnabled: true,
        closeOnOutsideClick: true,
        contentTemplate: function () {
            //console.log(cupon)
            var comerceName = (cupon.comerce != null) ? cupon.comerce.name : "Comercio no disponible";
            var checkedIsActive = cupon.isActive == true ? "checked" : "";
            var checkedIsChange = promotion.isActive == true ? "checked" : "";

            var temp = ''
                + '<div class="box-body" >                                                      '
                + '     <h3 class="comerceName">                                                '
                + '        ' + comerceName + '                                                  '
                + '     </h3>                                                                   '
                + '     <div class="media">                                                     '
                + '        <div class="media-body">                                             '
                + '            <div class="clearfix">                                           '

                + '                <p class="pull-right">                                       '
                + '                     <span class="uk-label"><i class="fa fa-tags"></i>                                    '
                + '                        ' + "Nuevo" + '                                      '
                + '                    </span>                                                  '
                + '                </p>                                                         '

                + '                <h4 style="margin-top: 0" class="uk-text-primary">           '
                + '                     <small>Código : #' + cupon.cuponId + '</small> - Cupón del ' + (cupon.descount*100) + ' % de descuento.               '
                + '                </h4>                                                        '

                + '                <div class="">                                               '
                + '                     <div class="col-sm-6">                                  '
                + '                         <dl class="uk-description-list ">                   '
                + '                             <dt>Valor monetario</dt>                        '
                + '                             <dd>C$ ' + cupon.moneyValue + '</dd>            '
                + '                             <dt>Duración</dt>                               '
                + '                             <dd>' + cupon.validity + ' díaz</dd>            '
                + '                         </dl>                                               '
                + '                     </div>                                                  '
                + '                     <div class"col-sm-6">                                   '
                + '                         <dl class="uk-description-list ">                   '
                + '                             <dt>Fecha de creación</dt>                      '
                + '                             <dd class="uk-text-warning"><i class="date fa fa-calendar-alt margin-r5"></i>' + dateFormat(cupon.createdAt) + '</dd>                          '
                + '                             <dt>Última actualización</dt>                                   '
                + '                             <dd class="uk-text-warning"><i class="date fa fa-calendar-alt margin-r5"></i>' + dateFormat(cupon.updatedAt) + '</dd>                          '
                + '                             <dt>Opciones</dt>                                          '
                + '                             <dd>'
                + '                                 <div class="checkbox checbox-switch switch-primary">                                '
                + '                                    <label class="pull-left"> Habilitado:                                                                      '
                + '                                         <input type="checkbox" ' + checkedIsActive + ' value="' + cupon.isActive + '" disabled />      '
                + '                                         <span></span>                                                                '
                + '                                    </label>                                                                      '
                + '                                    <label class="pull-right">   Canjeado                                                                   '
                + '                                         <input type="checkbox" ' + checkedIsChange + ' value="' + cupon.isChanged + '" disabled />      '
                + '                                         <span></span>                                                                '
                + '                                    </label>                                                                      '
                + '                                 </div>                                                 '
                + '                             </dd > '
                + '                         </dl>                                                       '
                + '                     </div>                                                 '
                + '                </div>                                                 '
                + '        </div>                                                     '
                + '     </div>                                                         '
                + '     <div class="pull-right">                                                                                                                                                '
                + '         <hr />                                                                                                                                                                 '
                + '         <div class="uk-display-inline">                                                                                                                                     '
                + '             <a href="/Cupons/Edit/' + cupon.cuponId + '" class="uk-button uk-button-small uk-button-default"> <span uk-icon="icon: pencil"></span> Editar</a> | '
                + '             <a href="/Cupons/Delete/' + cupon.cuponId + '" class="uk-button uk-button-small uk-button-danger"> <span uk-icon="icon: trash"></span> Eliminar</a> '
                + '         </div>                                                                                                                                                              '
                + '          <br /><br />                                                                                                                                                        '
                + '     </div>                                                             '
                + '</div>                                                             ';
            
            return $("<div/>").append(temp);
        },
    };

    if (popup) {
        $("#popup_cupons").remove();
    }
    var $popupContainer = $("<div />")
        .addClass("popup")
        .appendTo($("#popup_cupons"));
    popup = $popupContainer.dxPopup(popupOptions).dxPopup("instance");
    popup.show();
};

function showVacantPopup(vacant) {
    var popup = null;
    var popupOptions = {
        maxWidth: 700,
        maxHeight: 500,
        resizeEnabled: true,
        showTitle: true,
        title: "Detalle de la Vacante",
        visible: true,
        dragEnabled: true,
        closeOnOutsideClick: true,
        contentTemplate: function () {
            //console.log(vacant)
            var comerceName = (vacant.comerce != null) ? vacant.comerce.name : "Comercio no disponible";
            var checkedIsActive = vacant.isActive == true ? "checked" : "";

            var temp = ''
                + '<div class="box-body" >                                                      '
                + '     <h3 class="comerceName">                                                '
                + '        ' + comerceName + '                                                  '
                + '     </h3>                                                                   '
                + '     <div class="media">                                                     '
                + '        <div class="media-body">                                             '
                + '            <div class="clearfix">                                           '

                + '                <p class="pull-right">                                       '
                + '                     <span class="uk-label"><i class="fa fa-tags"></i>                                    '
                + '                        ' + "Nuevo" + '                                      '
                + '                    </span>                                                  '
                + '                </p>                                                         '

                + '                <h4 style="margin-top: 0" class="uk-text-primary">           '
                + '                     <small>Código #: ' + vacant.vacantId + '</small> - ' + vacant.name + '               '
                + '                </h4>                                                        '

                + '                <div class="">                                               '
                + '                     <div class="col-sm-4">                                  '
                + '                         <dl class="uk-description-list ">                   '
                + '                             <dt>Cargo</dt>                               '
                + '                             <dd>' + vacant.cargo + '</dd>            '
                + '                             <dt>Experiencia</dt>                        '
                + '                             <dd>' + vacant.experience + '</dd>            '
                + '                             <dt>Detalle</dt>                                '
                + '                             <dd>' + vacant.description + '</dd>               '
                + '                         </dl>                                               '
                + '                     </div>                                                  '
                + '                     <div class="col-sm-4">                                  '
                + '                         <dl class="uk-description-list ">                   '
                + '                             <dt>Horario</dt>                                '
                + '                             <dd>' + vacant.schedule + '</dd>               '
                + '                             <dt>Salario Minimo</dt>                                '
                + '                             <dd>C$ ' + vacant.minSalary + '</dd>               '
                + '                             <dt>Departamento del pais</dt>                                '
                + '                             <dd>' + vacant.department + '</dd>               '
                + '                         </dl>                                               '
                + '                     </div>                                                  '
                + '                     <div class"col-sm-4">                                   '
                + '                         <dl class="uk-description-list ">                   '
                + '                             <dt>Vacantes disponibles</dt>                                '
                + '                             <dd>' + vacant.availableVacants + ' vacante</dd>               '
                + '                             <dt>Edad Minima</dt>                                '
                + '                             <dd>' + vacant.age + '</dd>               '
                + '                             <dt>Opciones</dt>                                   '
                + '                             <dd>'
                + '                                 <div class="checkbox checbox-switch switch-primary">  Habilitado:                               '
                + '                                     <label>                                                                      '
                + '                                          <input type="checkbox" ' + checkedIsActive + ' value="' + vacant.isActive + '" disabled />      '
                + '                                          <span></span>                                                                '
                + '                                     </label>                                                                      '
                + '                                 </div>                                                 '
                + '                             </dd>                          '
                + '                         </dl>                                                       '
                + '                     </div>                                                  '
                + '                </div>                                                       '
                + '        </div>                                                               '
                + '     </div>                                                                  '
                + '     <div class="pull-right">                                                                                                                                                '
                + '         <hr />                                                                                                                                                                 '
                + '         <div class="uk-display-inline">                                                                                                                                     '
                + '             <a href="/Vacants/Edit/' + vacant.vacantId + '" class="uk-button uk-button-small uk-button-default"> <span uk-icon="icon: pencil"></span> Editar</a> | '
                + '             <a href="/Vacants/Delete/' + vacant.vacantId + '" class="uk-button uk-button-small uk-button-danger"> <span uk-icon="icon: trash"></span> Eliminar</a> '
                + '         </div>                                                                                                                                                              '
                + '          <br /><br />                                                                                                                                                        '
                + '     </div>                                                             '
                + '</div>                                                             ';
            return $("<div/>").append(temp);
        },
    };

    if (popup) {
        $("#popup_vacants").remove();
    }
    var $popupContainer = $("<div />")
        .addClass("popup")
        .appendTo($("#popup_vacants"));
    popup = $popupContainer.dxPopup(popupOptions).dxPopup("instance");
    popup.show();
};

function showCategoryPopup(category) {
    var popup = null;
    var popupOptions = {
        maxWidth: 500,
        maxHeight: 350,
        resizeEnabled: true,
        showTitle: true,
        title: "Detalle de la categoria",
        visible: true,
        dragEnabled: true,
        closeOnOutsideClick: true,
        contentTemplate: function () {
            //console.log(category)
            var template = ""
                + "<div class='uk-card-default'>"

                + "     <div class='col-sm-4 uk-card-media-left uk-cover-container text-center'>"
                + "         <i class='picker-target fa-5x undefined " + category.icon + "' ></i>"
                + "     </div>"

                + "     <div class='col-sm-8'>"
                + "         <div class='uk-card-body'>"
                + "         <h4 class='uk-card-title'>Categoria <small>" + category.name + "</small></h4>"
                + "           <dl class='uk-description-list'>"
                + "             <dt>Habilitado</dt>"
                + "             <dd>"
                + "                  <div class='checkbox checbox-switch switch-primary'>"
                + "                        <label>"
                + "                        <input type='checkbox' checked='" + category.isActive + "' disabled />"
                + "                        <span></span>"
                + "                    </label>"
                + "                </div>"
                + "             </dd> "
                + "            </dl>"
                + "         </div>"
                + "         </div >"

                + "         <div class='pull-right'>"
                + "          <hr />"
                + "             <div class='uk-display-inline'>"
                + "                 <a href='/Categories/Edit/" + category.categoryId + "' class='uk-button uk-button-small uk-button-default'> <span uk-icon='icon: pencil'></span> Editar</a> |"
                + "                 <a href='/Categories/Delete/" + category.categoryId + "' class='uk-button uk-button-small uk-button-danger'> <span uk-icon='icon: trash'></span> Eliminar</a>"
                + "             </div>"
                + "              <br /><br />"
                + "         </div >"

                + "</div>";
            return $("<div/>").append(template);
        },
    };

    if (popup) {
        $("#popup_category").remove();
    }
    var $popupContainer = $("<div />")
        .addClass("popup")
        .appendTo($("#popup_category"));
    popup = $popupContainer.dxPopup(popupOptions).dxPopup("instance");
    popup.show();
};

function showRoyaltyPopup(royalty) {
    var popup = null;
    var popupOptions = {
        maxWidth: 600,
        maxHeight: 400,
        resizeEnabled: true,
        showTitle: true,
        title: "Detalle de la Regalia",
        visible: true,
        dragEnabled: true,
        closeOnOutsideClick: true,
        contentTemplate: function () {
            //console.log(royalty)
            var comerceName = (royalty.comerce != null) ? royalty.comerce.name : "Comercio no disponible";
            var checkedIsActive = royalty.isActive == true ? "checked" : "";

            var temp = ''
                + '<div class="box-body" >                                                      '
                + '     <h3 class="comerceName">                                                '
                + '        ' + comerceName + '                                                  '
                + '     </h3>                                                                   '
                + '     <div class="media">                                                     '
                + '        <div class="media-body">                                             '
                + '            <div class="clearfix">                                           '

                + '                <p class="pull-right">                                       '
                + '                     <span class="uk-label"><i class="fa fa-tags"></i>                                    '
                + '                        ' + "Nuevo" + '                                      '
                + '                    </span>                                                  '
                + '                </p>                                                         '

                + '                <h4 style="margin-top: 0" class="uk-text-primary">           '
                + '                     <small>Código #: ' + royalty.royaltyId + '</small> - ' + royalty.name + '               '
                + '                </h4>                                                        '

                + '                <div class="">                                               '
                + '                     <div class="col-sm-6">                                  '
                + '                         <dl class="uk-description-list ">                   '
                + '                             <dt>Detalle</dt>                    '
                + '                             <dd>' + royalty.description + '</dd>               '
                + '                             <dt>Concepto</dt>                        '
                + '                             <dd>' + royalty.concept + '</dd>            '
                + '                             <dt>Valor monetario</dt>                               '
                + '                             <dd>C$ ' + royalty.value + '</dd>            '
                + '                         </dl>                                               '
                + '                     </div>                                                  '
                + '                     <div class"col-sm-6">                                   '
                + '                         <dl class="uk-description-list ">                   '
                + '                             <dt>Fecha de creación</dt>                      '
                + '                             <dd class="uk-text-warning"><i class="date fa fa-calendar-alt margin-r5"></i>' + dateFormat(royalty.createdAt) + '</dd>                          '
                + '                             <dt>Última actualización</dt>                                   '
                + '                             <dd class="uk-text-warning"><i class="date fa fa-calendar-alt margin-r5"></i>' + dateFormat(royalty.updatedAt) + '</dd>                          '
                + '                             <dt>Opciones</dt>                                          '
                + '                             <dd>'
                + '                                 <div class="checkbox checbox-switch switch-primary">                                '
                + '                                    <label class="pull-left"> Habilitado:                                                                      '
                + '                                         <input type="checkbox" ' + checkedIsActive + ' value="' + royalty.isActive + '" disabled />      '
                + '                                         <span></span>                                                                '
                + '                                    </label>                                                                      '
                + '                             </dd '
                + '                         </dl>                                                       '
                + '                     </div>                                                 '
                + '                </div>                                                 '
                + '        </div>                                                     '
                + '     </div>                                                         '
                + '     <div class="pull-right">                                                                                                                                                '
                + '         <hr />                                                                                                                                                                 '
                + '         <div class="uk-display-inline">                                                                                                                                     '
                + '             <a href="/Royalties/Edit/' + royalty.royaltyId + '" class="uk-button uk-button-small uk-button-default"> <span uk-icon="icon: pencil"></span> Editar</a> | '
                + '             <a href="/Royalties/Delete/' + royalty.royaltyId + '" class="uk-button uk-button-small uk-button-danger"> <span uk-icon="icon: trash"></span> Eliminar</a> '
                + '         </div>                                                                                                                                                              '
                + '          <br /><br />                                                                                                                                                        '
                + '     </div>                                                             '
                + '</div>                                                             ';

            return $("<div/>").append(temp);
        },
    };

    if (popup) {
        $("#popup_royalties").remove();
    }
    var $popupContainer = $("<div />")
        .addClass("popup")
        .appendTo($("#popup_royalties"));
    popup = $popupContainer.dxPopup(popupOptions).dxPopup("instance");
    popup.show();
}

//........................................................................................
//                   Validations modals forms
//........................................................................................

var formProduct = (form) =>
{
    form.validate({
        rules: {
            ProductName: {
                required: true,
                minlength: 5,
                maxlength: 50,
            },
            ProductPrice: {
                //required: true,
                min: 0
            },
            ProductDescription: {
                required: true,
            },
        },
        messages: {
            ProductName: {
                required: "Nombre del producto requerido",
                minlength: jQuery.validator.format("Ingresar mas de {0} caracteres"),
                maxlength: jQuery.validator.format("Nombre no debe contener mas de {0} caracteres"),
            },
            ProductPrice: {
                //required: "Precio del producto requerido",
                min: "Valor incorrecto"
            },
            ProductDescription: {
                required: "Descripcion del producto requerido",
            }
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "ProductName") {
                error.insertAfter("#groupProductName");
            } else {
                if (element.attr("name") == "ProductPrice") {
                    error.insertAfter("#groupProductPrice");
                } else {
                    error.insertAfter(element);
                }
            }
               
        },
        submitHandler: function (form) {
            console.log("addProduct validated...", form, $(form).serialize());
            addProduct(form);
        }
    });
}

var formPromotion = (form) => {
    form.validate({
        rules: {
             PromotionName: {
                required: true,
                minlength: 5,
                maxlength: 50,
            },
            PromotionDescription: {
                required: true,
                minlength: 5,
                maxlength: 200,
            },
            PromotionStartAt: {
                required: true,
            },
            PromotionEndAt: {
                required: true,
            },
            PromotionDescount: {
                required: true,
            }
        },
        messages: {
            PromotionName: {
                required: "Ingresar Nombre de la Promoción"
            },
            PromotionDescription: {
                required: "Ingresar una Descripción",
                minlength: jQuery.validator.format("Ingresar mas de {0} caracteres"),
                maxlength: jQuery.validator.format("Descripción no debe contener mas de {0} caracteres"),
            },
            PromotionStartAt: {
                required: "Fecha de inicio requerida",
            },
            PromotionEndAt: {
                required: "Fecha de finalización requerida",
            },
            PromotionDescount: {
                required: "Ingresar descuento",
            }
        },
        submitHandler: function (form) {
            //console.log("formPromotion validated...");
            addPromotion();
        }
    });
}

var formCupon = (form) => {
   form.validate({
        rules: {
            CuponDescount: {
                required: true,
                min: 1,
            },
            CuponValidity: {
                required: true,
                min: 1,
            },
            CuponMoneyValue: {
                required: true,
                min: 1,
            },
        },
        messages: {
            CuponDescount: {
                required: "Asignar descuento",
                min: "Valor incorrecto",
            },
            CuponValidity: {
                required: "Asignar los días de validez",
                min: "Valor incorrecto",
            },
            CuponMoneyValue: {
                required: "Asignar valor monetario",
                min: "Valor incorrecto",
            },
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "CuponDescount") {
                error.insertAfter("#groupCuponDescount");
            } 
            if (element.attr("name") == "CuponValidity") {
                error.insertAfter("#groupCuponValidity");
            }
            if (element.attr("name") == "CuponMoneyValue") {
                error.insertAfter("#groupCuponMoneyValue");
            }
        },
        submitHandler: function (form) {
            //console.log("formCupon validated...");
            addCupon();
            //isValid.resetForm();
        }
   }).resetForm();;
}

var formRoyalty = (form) => {
    form.validate({
        rules: {
            RoyaltyName: {
                required: true,
                minlength: 5,
                maxlength: 50,
            },
            RoyaltyValue: {
                required: true,
                min: 1,
            },
            RoyaltyCount: {
                required: true,
                min: 1,
            },
            RoyaltyConcept: {
                required: true,
                minlength: 10,
                maxlength: 200,
            },
            RoyaltyDescription: {
                required: true,
                minlength: 10,
                maxlength: 200,
            },
        },
        messages: {
            RoyaltyName: {
                required: "Nombre requerido",
                minlength: jQuery.validator.format("Ingresar mas de {0} caracteres"),
                maxlength: jQuery.validator.format("Nombre no debe contener mas de {0} caracteres"),
            },
            RoyaltyValue: {
                required: "Valor requerido",
                min: "Valor incorrecto"
            },
            RoyaltyCount: {
                required: "Valor requerido",
                min: "Valor incorrecto"
            },
            RoyaltyConcept: {
                required: "Concepto requerido",
                minlength: jQuery.validator.format("Ingresar mas de {0} caracteres"),
                maxlength: jQuery.validator.format("Concepto no debe contener mas de {0} caracteres"),
            },
            RoyaltyDescription: {
                required: "Descripción requerido",
                minlength: jQuery.validator.format("Ingresar mas de {0} caracteres"),
                maxlength: jQuery.validator.format("Descripción no debe contener mas de {0} caracteres"),
            },
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "RoyaltyValue") {
                error.insertAfter("#groupRoyaltyValue");
            }else
                if (element.attr("name") == "RoyaltyCount") {
                    error.insertAfter("#groupRoyaltyCount");
                }else 
                    if (element.attr("name") == "CuponMoneyValue") {
                        error.insertAfter("#groupCuponMoneyValue");
                    } else 
                        error.insertAfter(element);
        },
        submitHandler: function (form) {
            ///console.log("formRoyalty validated...");
            addRoyalty();
        }
    });
}

var formVacant = (form) => {
    form.validate({
        rules: {
            VacantName: {
                required: true,
                minlength: 5,
                maxlength: 50,
            },
            VacantDescription: {
                required: true,
                minlength: 5,
                maxlength: 50,
            },
            VacantArea: {
                required: true,
                minlength: 5,
                maxlength: 50,
            },
            VacantCargo: {
                required: true,
                minlength: 5,
                maxlength: 50,
            },
            VacantAvailableVacants: {
                required: true,
                min: 1,
            },
            VacantExperience: {
                required: true,
                minlength: 2,
                maxlength: 100,
            },
            VacantAge: {
                required: true,
                min: 15,
                max: 60,
            },
            VacantMaxSalary: {
                required: true,
                min: 1,
            },
            VacantMinSalary: {
                required: true,
                min: 1,
            },

        },
        messages: {
            VacantName: {
                required: "Nombre requerido",
                minlength: jQuery.validator.format("Ingresar mas de {0} caracteres"),
                maxlength: jQuery.validator.format("Nombre no debe contener mas de {0} caracteres"),
            },
            VacantDescription: {
                required: "Descripción requerido",
                minlength: jQuery.validator.format("Ingresar mas de {0} caracteres"),
                maxlength: jQuery.validator.format("Descripción no debe contener mas de {0} caracteres"),
            },
            VacantArea: {
                required: "Area requerido",
                minlength: jQuery.validator.format("Ingresar mas de {0} caracteres"),
                maxlength: jQuery.validator.format("Area no debe contener mas de {0} caracteres"),
            },
            VacantCargo: {
                required: "Cargo requerido",
                minlength: jQuery.validator.format("Ingresar mas de {0} caracteres"),
                maxlength: jQuery.validator.format("Cargo no debe contener mas de {0} caracteres"),
            },
            VacantAvailableVacants: {
                required: "Vacantes disponibles requeridas",
                min: "Valor incorrecto"
            },
            VacantExperience: {
                required: "Experiencia requerido (Ej. Ninguna)",
                minlength: jQuery.validator.format("Ingresar mas de {0} caracteres"),
                maxlength: jQuery.validator.format("Nombre no debe contener mas de {0} caracteres"),
            },
            VacantAge: {
                required: "Edad requerido",
                min: jQuery.validator.format("Edad minima requerida {0}"),
                max: jQuery.validator.format("Edad inválida"),
            },
            VacantMaxSalary: {
                required: "Salario Máximo requerido",
                min: "Valor incorrecto"
            },
            VacantMinSalary: {
                required: "Salario Mínimo requerido",
                min: "Valor incorrecto"
            },
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "VacantAvailableVacants") {
                error.insertAfter("#groupVacantAvailableVacants");
            } else
                if (element.attr("name") == "VacantMaxSalary") {
                    error.insertAfter("#groupVacantMaxSalary");
                } else
                    if (element.attr("name") == "VacantMinSalary") {
                        error.insertAfter("#groupVacantMinSalary");
                    } else
                        error.insertAfter(element);
        },
        submitHandler: function (form) {
            //console.log("form validated...");
            addVacant();
        }
    });
}

