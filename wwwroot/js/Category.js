﻿class Category {
    constructor(CategoryId, Name, Icon, IsActive) {
        this.CategoryId = CategoryId;
        this.Icon = Icon;
        this.IsActive = IsActive;
    }

    addCategory() {
        var self = this;

        var comerceName = $("#ComerceName").val();

   
        var code = "";
        var description = "";

        $.ajax({
            type: "POST",
            url: "",
            data: {

            },
            success: function (response) {
                //console.log(response);
                $.each(response, (index, value) => {
                    code = value.code;
                    description = value.description;
                });
                if (code == "Saved") {
                    self.laodCategoryTable();
                }
            },
            error: function (err) {
                console.log("Error: ");
                console.log(err);
            }
        });
    }

    laodCategoryTable() {
        var categories_data = new DevExpress.data.CustomStore({
            load: function (loadOptions) {
                var deferred = $.Deferred(),
                    args = {};
                console.log(loadOptions)
                if (loadOptions.sort) {
                    args.orderby = loadOptions.sort[0].selector;
                    if (loadOptions.sort[0].desc)
                        args.orderby += " desc";
                }

                args.skip = loadOptions.skip || 0;
                args.take = loadOptions.take || 5;

                $.ajax({
                    url: "/Categories/GetCategories",
                    dataType: "json",
                    data: args,
                    success: function (result) {
                        console.log(result)
                        deferred.resolve(result, { totalCount: 5 });
                    },
                    error: function () {
                        deferred.reject("Problemas al cargar los datos de las categorias");
                    },
                    timeout: 5000
                });

                return deferred.promise();
            },

        });

        $("#category_Container").dxDataGrid({
            dataSource: categories_data,
            accessKey: "name",
            allowColumnResizing: true,
            columnResizingMode: "nextColumn",
            columnMinWidth: 50,
            showBorders: false,
            searchPanel: {
                visible: true,
                width: 450,
                placeholder: "Buscar categorias..."
            },
            selection: {
                mode: "single"
            },
            onSelectionChanged: function (selectedItems) {
                var data = selectedItems.selectedRowsData[0];
                showCategoryPopup(data);
                console.log(data)
            },
            remoteOperations: false,
            paging: {
                pageSize: 5
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20]
            },
            columns: [
                {
                    dateFiel: "urlImage",
                    caption: "Imagen",
                    width: 150,
                    allowFiltering: false,
                    allowSorting: false,
                    alignment: "center",
                    cellTemplate: function (container, options) {
                        var icon = options.data.icon || "fas fa-chess-board";
                        console.log("icono: " + icon);
                        $("<div>")
                            .append($("<i class='picker-target fa-2x undefined " + icon + "' ></i>"))
                            .appendTo(container);
                    }

                },
                {
                    dataField: "categoryId",
                    caption: "Código",
                    width: 100,
                    sortOrder: "desc",
                    alignment: "center"
                },
                {
                    dataField: "name",
                    caption: "Nombre",
                    sortOrder: "desc",
                },
                {
                    dataField: "isActive",
                    caption: "Habilitado",
                    dataType: "boolean",
                    calculateCellValue: function (rowData) {
                        return rowData.isActive;
                    },
                    width: 150,
                },
            ],
        });
    }

    getCategories() {
        $.ajax({
            type: "GET",
            url: "/Categories/GetCategories",
            data: {},
            success: function (response) {
                var select = $("#ComerceCategoryid");
                select.html("");
                select.append("<option value='0' disabled>" + " -Categoria del comercio- " + "</option>");
                $.each(response, (index, value) => {
                    console.log("response: " + value.name + ", icon: " + value.icon);
                    select.append("<option value='" + value.categoryId + "'>" + value.name + "</option>");
                });
            },
            error: function (err) {
                console.log("Error: " + err);
            }
        });
    }

    setRating() {
        var priorities = ["1", "2", "3", "4", "5"]
        
        $("#setRating").dxRadioGroup({
            items: priorities,
            value: priorities[0],
            layout: "horizontal",
            onValueChanged: function (e) {
                console.log("click in checkbox");
                $("#Rating").val(e.value);
                console.log($("#Rating").val());
            },
            itemTemplate: function (itemData, _, itemElement) {
                //$(".dx-radiobutton-icon").addClass("paint")
                //itemData.addClass("paint");
                itemElement.parent().text(itemData);
            }
        });
    }
}



    