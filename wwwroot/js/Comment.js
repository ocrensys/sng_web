﻿class UserComment {
    constructor() { }

    delete(comment_id) {
        var self = this;
        var code = "";
        var description = "";

        $.ajax({
            url: "/Comerces/DeleteUserComment/" + comment_id,
            data: {},
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function(response) {               
                if (response[0].code == "Deleted") {
                    console.log("success")
                    DevExpress.ui.notify({
                        message: response[0].description || "¡Comentario eliminado!",
                        type: "info",
                        displayTime: 5000,
                        closeOnClick: true
                    });
                    self.loadCommentsTable();
                }
            },
            error: function(err) {
                console.log("Error: ");
                console.log(err);
                DevExpress.ui.notify({
                    message: "No fue posible eliminar el comentario :(",
                    type: "error",
                    displayTime: 5000,
                    closeOnClick: true
                });
            }
        });
    }
    
    getData(req) {
        var self = this;
        var royalties_data = new DevExpress.data.DataSource({
            load: function (loadOptions) {
                var deferred = $.Deferred(),
                    args = {};
                var OrderBy, Skip, Take, Url, data;
                if (loadOptions.sort) {
                    args.orderby = loadOptions.sort[0].selector;
                    OrderBy = loadOptions.sort[0].selector;
                    if (loadOptions.sort[0].desc)
                        args.orderby += " desc";
                }

                Skip = loadOptions.skip || 0;
                Take = loadOptions.take || 10;
                OrderBy = loadOptions.orderby;

                args.skip = loadOptions.skip || 0;
                args.take = loadOptions.take || 10;
                args.orderby = loadOptions.orderby;

                if (req === "getAll") {
                    Url = "/Royalties/GetRoyalties";
                } else {
                    //console.log("getCommentsByComerce...")
                    Url =  "/Comerces/GetUserComment",
                    args.comerceId = ComerceId;
                }

                $.ajax({
                    url: Url,
                    dataType: "json",
                    data: args,
                    success: function (result) {
                        console.log('comentarios', result);
                        deferred.resolve(result, { totalCount: result.length });
                    },
                    error: function () {
                        deferred.reject("Problemas al cargar lista de comentarios");
                    },
                    timeout: 5000
                });

                return deferred.promise();
            },
            map: function(item) { 
                console.log('item', item);
                return $.extend({ 
                    nombre: item.applicationUser.firstName + ' ' + item.applicationUser.lastName
                }, item);
            }
        });
        
        return royalties_data;
    }

    loadCommentsTable(req) {
        var self = this;
        var data = self.getData(req);

        $("#table_comments").dxDataGrid({
            dataSource: data,
            accessKey: "description",
            allowColumnResizing: true,
            columnResizingMode: "nextColumn",
            columnMinWidth: 50,
            wordWrapEnabled: true,
            showBorders: false,
            groupPanel: {
                visible: true,
                emptyPanelText: "Arrastrar y soltar una columna...",
            },
            grouping: {
                texts: {
                    groupContinuesMessage: "Continua en la siguiente página >",
                    groupContinuedMessage: "< Continua en la pagina anterios",
                }
            },
            searchPanel: {
                visible: true,
                width: 300,
                placeholder: "Buscar comentarios..."
            },
            loadPanel: {
                height: 100,
                width: 300,
                text: "Cargando...",
            },
            selection: {
                mode: "single"
            },
            onSelectionChanged: function (selectedItems) {
                //var data = selectedItems.selectedRowsData[0];
                //showRoyaltyPopup(data);
                //console.log(data)
            },
            remoteOperations: false,
            paging: {
                enabled: true,
                pageSize: 10
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20]
            },
            columns: [
                {
                    dataField: "nombre",
                    caption: "Nombre",
                    width: 150,
                    cellTemplate: function (container, options) {
                        if (options.data.avatarURL) {
                            $("<div>")
                                .append($("<img>", {
                                    "src": options.data.avatarURL,
                                    "width": "50",
                                    "alt": "Imagen"
                                }))
                                .append("<br>")
                                .append("<span>" +  options.data.nombre + "</span>")
                                .appendTo(container);
                        } else {
                            $("<div>")
                                .append($("<img>", {
                                    "src": "/images/user.png",
                                    "width": "50",
                                    "alt": "Imagen"
                                }))
                                .appendTo(container);
                        }
                    }
                },
                {
                    dataField: "comment",
                    caption: "Comentario",
                    minWidth: 150,
                    alignment: "center",
                    sortOrder: "asc",
                },
                {
                    dataField: "timeSpan",
                    caption: "Fecha",
                    minWidth: 100,
                    sortOrder: "asc",
                    allowSorting: true,
                    calculateCellValue: function (rowData) {
                        return dateFormat(rowData.timeSpan);
                    },
                },
                {
                    dataField: "Id",
                    caption: "Eliminar",
                    width: 120, 
                    alignment: "center",
                    sortOrder: "asc",
                    allowSorting: true,
                    cellTemplate: function (container, options) {                    
                        $("<div/>").dxButton({
                            icon: 'trash',
                            onClick: function (e) {
                                var myDialog = DevExpress.ui.dialog.custom({
                                    title: "Confirmar",
                                    message: "¿Seguro quieres eliminar este Comentario?",
                                    buttons: [
                                        {
                                        text: "Eliminar",
                                        onClick: function(e) {
                                            return true;
                                        }
                                    }, 
                                    {
                                        text: "Cancelar",
                                        onClick: function(e) {
                                            return false;
                                        }
                                    }, 
                                    ]
                                });
                                myDialog.show().done(function(dialogResult) {
                                    if(dialogResult) {
                                        var comment_id = options.data.id;
                                        console.log("id comment", comment_id);
                                        self.delete(comment_id);  
                                    }
                                });
                            }
                        }).appendTo(container);
                    }
                }

            ],
        });
        
    }

}