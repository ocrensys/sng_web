﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using sng_web.Common;
using sng_web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace sng_web.Data
{
    public class AppClaimsPrincipalFactory : UserClaimsPrincipalFactory<ApplicationUser, IdentityRole>
    {
        public AppClaimsPrincipalFactory(
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager,
            IOptions<IdentityOptions> optionsAccessor) : base(userManager, roleManager, optionsAccessor)
        {
        }

        public override async Task<ClaimsPrincipal> CreateAsync(ApplicationUser user)
        { 
            if(user.FirstName==null)
                user.FirstName="";
            if(user.LastName==null)
                user.LastName="";
            if (user.AvatarURL == null)
                user.AvatarURL = "/images/user.png";
            if (user.NickName == null)
                user.NickName = "";
            if(user.Position == null)
                user.Position = "";
            if (user.DateRegistered == null)
                user.DateRegistered = DateTime.UtcNow.ToString();

            var principal = await base.CreateAsync(user);

            ((ClaimsIdentity)principal.Identity).AddClaims(new[] {
                new Claim(CustomClaimTypes.GivenName, user.FirstName),
                new Claim(CustomClaimTypes.Surname, user.LastName),
                new Claim(CustomClaimTypes.AvatarURL, user.AvatarURL),
                new Claim(CustomClaimTypes.Position, user.Position),
                new Claim(CustomClaimTypes.NickName, user.NickName),
                new Claim(CustomClaimTypes.DateRegistered, user.DateRegistered),
            });

            if (user.Comerce != null)
            {
                ((ClaimsIdentity)principal.Identity).AddClaim(new Claim(CustomClaimTypes.ComerceId, Convert.ToString(user.Comerce.ComerceId)));
            }

            return principal;
        }
    }
}
