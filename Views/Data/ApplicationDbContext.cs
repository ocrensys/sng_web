﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using sng_web.Models;

namespace sng_web.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            builder.Entity<Proudct_Image>()
                .HasKey(pi => new { pi.ProductId, pi.ImageId });

            builder.Entity<Proudct_Image>()
                .HasOne(pi => pi.Product)
                .WithMany(p => p.galeria)
                .HasForeignKey(pi => pi.ProductId);

            builder.Entity<Proudct_Image>()
                .HasOne(pi => pi.Image)
                .WithMany(c => c.product_images)
                .HasForeignKey(pi => pi.ImageId);


            builder.Entity<User_Favorite_Comerces>()
                .HasKey(pi => new { pi.Id, pi.ComerceId });

            builder.Entity<User_Favorite_Comerces>()
                .HasOne(pi => pi.User)
                .WithMany(p => p.Favorite_Comerces)
                .HasForeignKey(pi => pi.Id);

            builder.Entity<User_Favorite_Comerces>()
                .HasOne(pi => pi.Comerce)
                .WithMany(p => p.user_favorites)
                .HasForeignKey(pi => pi.ComerceId);


            builder.Entity<User_Favorite_Products>()
                .HasKey(pi => new { pi.Id, pi.ProductId });

            builder.Entity<User_Favorite_Products>()
                .HasOne(pi => pi.User)
                .WithMany(p => p.Favorite_Products)
                .HasForeignKey(pi => pi.Id);

            builder.Entity<User_Favorite_Products>()
                .HasOne(pi => pi.Product)
                .WithMany(p => p.user_favorites)
                .HasForeignKey(pi => pi.ProductId);
        }

        public DbSet<sng_web.Models.Category> Category { get; set; }

        public DbSet<sng_web.Models.Comerce> Comerce { get; set; }

        public DbSet<sng_web.Models.Cupon> Cupon { get; set; }

        public DbSet<sng_web.Models.Owner> Owner { get; set; }

        public DbSet<sng_web.Models.Product> Product { get; set; }

        public DbSet<sng_web.Models.Promotion> Promotion { get; set; }

        public DbSet<sng_web.Models.Vacant> Vacant { get; set; }

        public DbSet<VacantAplication> VacantAplications { get; set; }

        public DbSet<sng_web.Models.Royalty> Royalty { get; set; }

        public DbSet<sng_web.Models.ApplicationUser> ApplicationUser { get; set; }

        public DbSet<UserAudit> UserAuditEvents { get; set; }

        public DbSet<sng_web.Models.Image> Images { get; set; }

        public DbSet<sng_web.Models.ApplicationLog> ApplicationLog { get; set; }

        public DbSet<sng_web.Models.Subscriptions> Subscriptions { get; set; }

        public DbSet<sng_web.Models.TasaCambio> TasaCambio { get; set; }

        public DbSet<sng_web.Models.UserComerceRating> UserComerceRating { get; set; }

        public DbSet<sng_web.Models.UserMessageComment> UserMessageComment { get; set; }

         public DbSet<sng_web.Models.About> About { get; set; }
    }
}