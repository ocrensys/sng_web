﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sng_web.Services
{
    public class DataNotification
    {
        public string Model = "";
        public string ObjectId = "";
        public string ComerceId = "";
    }

    public interface INotifications
    {
        Task CreateNotification(string content_message, IDictionary<string,string> data);
        Task CreateNotification(string content_message, DataNotification data);
    }
}
