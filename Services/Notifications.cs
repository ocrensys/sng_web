﻿using Microsoft.Extensions.Configuration;
using System;
using System.Net;
using System.Threading.Tasks;

using System.IO;
using System.Text;

using OneSignal.CSharp.SDK;
using OneSignal.CSharp.SDK.Resources;
using OneSignal.CSharp.SDK.Resources.Notifications;
using System.Collections.Generic;

namespace sng_web.Services
{
    public class Notifications : INotifications
    {
        private readonly IConfiguration _configuration;
        private const string AppId = "41f069e7-5b25-429f-b807-c2a92696041e";

        public Notifications() {} 

        public async Task CreateNotification(string content_message, IDictionary<string,string> data)
        {
            var client = new OneSignalClient("OTA2ZjlkMDItMDNjNi00YmQxLTgyNzgtNDYyNzE5YTdjOWJl");
            var options = new NotificationCreateOptions();
            options.AppId = new Guid(AppId);
            options.IncludedSegments = new List<string> { "All" };
            options.Contents.Add(LanguageCodes.English, content_message);
            options.Contents.Add(LanguageCodes.Spanish, content_message);
            options.Data = data;
            //options.Data.Add("model", data.Model);
            //options.Data.Add("id", data.ObjectId.ToString());
            //options.Data.Add("comerceid", data.ComerceId.ToString());

            var respuesta = client.Notifications.Create(options);
        }

        public async Task CreateNotification(string content_message, DataNotification data)
        {
             var client = new OneSignalClient("OTA2ZjlkMDItMDNjNi00YmQxLTgyNzgtNDYyNzE5YTdjOWJl");
            var options = new NotificationCreateOptions();
            options.AppId = new Guid(AppId);
            options.IncludedSegments = new List<string> { "All" };
            options.Contents.Add(LanguageCodes.English, content_message);
            options.Contents.Add(LanguageCodes.Spanish, content_message);

            var respuesta = client.Notifications.Create(options);
        }
    }
}
