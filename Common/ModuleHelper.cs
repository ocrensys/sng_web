﻿using sng_web.Models;
using System;
using System.Collections.Generic;

namespace sng_web.Common
{
    /// <summary>
    /// This is where you customize the navigation sidebar
    /// </summary>
    public static class ModuleHelper
    {
        public enum Module
        {
            Home,
            ComerceHome,
            Login,
            Register,
            Categorias,
            Comercios,
            Cupones,
            Productos,
            Promociones,
            Vacantes,
            Regalias,
            Usuarios,
            Aplicaciones,
            Actividades,
            Reportes,
            TasaCambio,
            About
        }

        public static SidebarMenu AddHeader(string name)
        {
            return new SidebarMenu
            {
                Type = SidebarMenuType.Header,
                Name = name,
            };
        }

        public static SidebarMenu AddTree(string name, string iconClassName = "fa fa-link")
        {
            return new SidebarMenu
            {
                Type = SidebarMenuType.Tree,
                IsActive = false,
                Name = name,
                IconClassName = iconClassName,
                URLPath = "#",
            };
        }

        public static SidebarMenu AddModule(Module module, Tuple<int, int, int> counter = null)
        {
            if (counter == null)
                counter = Tuple.Create(0, 0, 0);

            switch (module)
            {
                case Module.Home:
                    return new SidebarMenu
                    {
                        Type = SidebarMenuType.Link,
                        Name = "Inicio",
                        IconClassName = "fa fa-home",
                        URLPath = "/",
                        LinkCounter = counter,
                    };
                case Module.ComerceHome:

                case Module.Login:
                    return new SidebarMenu
                    {
                        Type = SidebarMenuType.Link,
                        Name = "Login",
                        IconClassName = "fa fa-sign-in",
                        URLPath = "/Account/Login",
                        LinkCounter = counter,
                    };
                case Module.Register:
                    return new SidebarMenu
                    {
                        Type = SidebarMenuType.Link,
                        Name = "Register",
                        IconClassName = "fa fa-user-plus",
                        URLPath = "/Account/Register",
                        LinkCounter = counter,
                    };
                case Module.Categorias:
                    return new SidebarMenu
                    {
                        Type = SidebarMenuType.Link,
                        Name = "Categorias",
                        IconClassName = "fa fa-group",
                        URLPath = "/Categories",
                        LinkCounter = counter,
                    };
                case Module.Comercios:
                    return new SidebarMenu
                    {
                        Type = SidebarMenuType.Link,
                        Name = "Comercios",
                        IconClassName = "fa fa-shopping-cart",
                        URLPath = "/Comerces",
                        LinkCounter = counter,
                    };
                case Module.Cupones:
                    return new SidebarMenu
                    {
                        Type = SidebarMenuType.Link,
                        Name = "Cupones",
                        IconClassName = "fa fa-tags",
                        URLPath = "/Cupons",
                        LinkCounter = counter,
                    };
                case Module.Productos:
                    return new SidebarMenu
                    {
                        Type = SidebarMenuType.Link,
                        Name = "Productos",
                        IconClassName = "fa fa-cubes",
                        URLPath = "/Products",
                        LinkCounter = counter,
                    };
                case Module.Promociones:
                    //A TODOS LOS MENUS HAY QUE PONERLES EL ICONO CORRECTO ESO TE LO DEJO A TI.
                    return new SidebarMenu
                    {
                        Type = SidebarMenuType.Link,
                        Name = "Promociones",
                        IconClassName = "fa fa-percent",
                        URLPath = "/Promotions",
                        LinkCounter = counter,
                    };
                case Module.Vacantes:
                    return new SidebarMenu
                    {
                        Type = SidebarMenuType.Link,
                        Name = "Vacantes",
                        IconClassName = "fa fa-briefcase",
                        URLPath = "/Vacants",
                        LinkCounter = counter,
                    };
                case Module.Regalias:
                    return new SidebarMenu
                    {
                        Type = SidebarMenuType.Link,
                        Name = "Regalias",
                        IconClassName = "fa fa-shopping-basket",
                        URLPath = "/Royalties",
                        LinkCounter = counter,
                    };
                case Module.Usuarios:
                    return new SidebarMenu
                    {
                        Type = SidebarMenuType.Link,
                        Name = "Usuarios",
                        IconClassName = "fa fa-users",
                        URLPath = "/Account/List",
                        LinkCounter = counter,
                    };
                case Module.Actividades:
                    return new SidebarMenu
                    {
                        Type = SidebarMenuType.Link,
                        Name = "Actividades",
                        IconClassName = "fa fa-clipboard-list",
                        URLPath = "/Comerces/Activities",
                        LinkCounter = counter,
                    };
                case Module.Reportes:
                    return new SidebarMenu
                    {
                        Type = SidebarMenuType.Link,
                        Name = "Cajeados",
                        IconClassName = "fa fa-clipboard-list",
                        URLPath = "/Comerces/Reports",
                        LinkCounter = counter,
                    };
                case Module.Aplicaciones:
                    return new SidebarMenu
                    {
                        Type = SidebarMenuType.Link,
                        Name = "Aplicaciones",
                        IconClassName = "fa fa-suitcase",
                        URLPath = "/VacantAplications",
                        LinkCounter = counter,
                    };
                case Module.TasaCambio:
                    return new SidebarMenu
                    {
                        Type = SidebarMenuType.Link,
                        Name = "Tasa de Cambio",
                        IconClassName = "fa fa-calculator",
                        URLPath = "/TasaCambios",
                        LinkCounter = counter,
                    };
                case Module.About:
                    return new SidebarMenu
                    {
                        Type = SidebarMenuType.Link,
                        Name = "Acerca de",
                        IconClassName = "fa fa-info",
                        URLPath = "/About",
                        LinkCounter = counter,
                    };
                default:
                    break;
            }
            //
            return null;
        }
        
    }
}
