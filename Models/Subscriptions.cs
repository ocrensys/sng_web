﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sng_web.Models
{
    public class Subscriptions
    {

        public int Id { get; set; }
        public string  Email { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
