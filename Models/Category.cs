﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sng_web.Models
{
    public class Category
    {
        public int CategoryId { get; set; }

        [Required(ErrorMessage = "¡Nombre de la categoria requerido!")]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "Nombre de la categoria debe contener entre 5 y 50 caracteres!")]
        [Display(Name = "Categoria")]
        public string Name { get; set; }

        [Display(Name = "Icono")]
        public string Icon { get; set; }

        [Display(Name = "Habilitado")]
        public bool IsActive { get; set; }

        public string UrlImage { get; set; }

        /*Relation ships*/
        public ICollection<Comerce> Comerces { get; set; }
    }
}
