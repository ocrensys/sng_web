﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace sng_web.Models
{
    public class Owner
    {
        public int OwnerId { get; set; }

        [Required(ErrorMessage = "¡Nombre del comerciante es requerido!")]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "Nombre del comerciante debe contener entre 5 y 50 caracteres!")]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Required(ErrorMessage = "¡Apellido del comerciante es requerido!")]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "Nombre del comerciante debe contener entre 5 y 50 caracteres!")]
        [Display(Name = "Apellido")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "¡Cammpo Dirección es requerido!")]
        [StringLength(400, MinimumLength = 5, ErrorMessage = "Direccion del comerciante debe contener entre 5 y 400 caracteres!")]
        [Display(Name = "Dirección")]
        public string Address { get; set; }

        [Required(ErrorMessage = "¡Telefono requerido!")]
        [StringLength(50, MinimumLength = 8, ErrorMessage = "Campo Teléfono debe contener almenos 8 digitos!")]
        [Display(Name = "Teléfono")]
        public string Phone { get; set; }

        public Boolean IsActive { get; set; }

        [Display(Name = "Full Name")]
        [NotMapped]
        public string FullName
        {
            get
            {
                return Name + " " + LastName;
            }
        }

        /*Relation ships*/
        public ApplicationUser User { get; set; }
        public ICollection<Comerce> Comerces { get; set; }
    }
}
