﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace sng_web.Models
{

    public class User_Favorite_Comerces
    {
        public string Id { get; set; }
        public ApplicationUser User { get; set; }
        public long ComerceId { get; set; }
        public Comerce Comerce { get; set; }
    }
    public class User_Favorite_Products
    {
        public string Id { get; set; }
        public ApplicationUser User { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
    }

    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        [Display(Name="Nombre")]
        public string FirstName { get; set; }


        [Display(Name = "Apellidos")]
        public string LastName { get; set; }


        [Display(Name = "Foto")]
        public string AvatarURL { get; set; }


        [Display(Name = "Fecha de Registro")]
        public string DateRegistered { get; set; }


        [Display(Name = "Ubicación")]
        public string Position { get; set; }


        [Display(Name = "Alias")]
        public string NickName { get; set; }


       //[Display(Name = "Comercio correspondiente")]
        [Display(Name = "Comercio")]
        public long? ComerceId { get; set; }

        [ForeignKey("ComerceId")]
        public virtual Comerce Comerce { get; set; }

        public ICollection<User_Favorite_Comerces> Favorite_Comerces { get; set; }
        public ICollection<User_Favorite_Products> Favorite_Products { get; set; }
        public ICollection<UserComerceRating> UserComerceRating { get; set; }
        public ICollection<UserMessageComment> UserMessageComment { get; set; }

        public ApplicationUser()
        {
            this.Favorite_Comerces = new HashSet<User_Favorite_Comerces>();
            this.Favorite_Products = new HashSet<User_Favorite_Products>();
        }
    }
}
