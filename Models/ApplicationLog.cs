﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using sng_web.Data;

namespace sng_web.Models
{
    public class ApplicationLog
    {

        public enum LevelType
        {
            Information = 1,
            Warnning = 2,
            Error = 3
        }

        public enum ActionType
        {
            Crear = 11,
            Actualizar =  22,
            Borrar = 13,
        }

        public int ID { get; set; }
        public string UserId { get; set; }
        public DateTime Date { get; set; }
        public string Message { get; set; }
        public LevelType Level { get; set; }
        public ActionType Action { get; set; }
        public int ComerceId { get; set; }

        public static async Task RegisterLog(ApplicationDbContext _context, string module, string name, ActionType action, LevelType level, string userId, int ComerceId)
        {
            var applicationLog = new ApplicationLog()
            {
                Action = action,
                Date = DateTime.UtcNow,
                UserId = userId,
                Level = level,
                ComerceId = ComerceId,
                Message = string.Format("{0} los datos del modulo {1}: {2} el dia {3} por el usuario {4}", action, module, name, DateTime.UtcNow, userId)
            };

            _context.Add(applicationLog);
            await _context.SaveChangesAsync();
        }

        public static async Task RegisterLogError(ApplicationDbContext _context, string module, string name, ActionType action, LevelType level, string userId, int ComerceId)
        {
            var applicationLog = new ApplicationLog()
            {
                Action = action,
                Date = DateTime.UtcNow,
                UserId = userId,
                Level = level,
                ComerceId = ComerceId,
                Message = string.Format("{0} los datos del modulo {1}: {2} el dia {3} por el usuario {4}", action, module, name, DateTime.UtcNow, userId)
            };

            _context.Add(applicationLog);
            await _context.SaveChangesAsync();
        }
    }
}
