﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace sng_web.Models
{
    public class Promotion
    {
        public int PromotionId { get; set; }

        [Required(ErrorMessage = "¡Nombre de la promoción es requerido!")]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "Nombre de la prooción debe contener entre 5 y 50 caracteres!")]
        [Display(Name = "Nombre de la promocion")]
        public string Name { get; set; }

        [Required(ErrorMessage = "¡Descripción de la promoción requerido!")]
        [StringLength(200, MinimumLength = 5, ErrorMessage = "Descripción de la promoción debe contener entre 5 y 200 caracteres!")]
        [Display(Name = "Descripción")]
        public string Description { get; set; }
        
        //[Required(ErrorMessage = "¡Descuento aplicado es requerido!")]
        //[Range(0, 1, ErrorMessage = "¡El descuento debe ser mayor o igual a 0 ó menor que 1!")]
        [Display(Name = "Descuento de la promoción")]
        public double Descount { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = false)]
        [Display(Name = "Fecha de inicio")]
        public DateTime StartAt { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = false)]
        [Display(Name = "Fecha de expiración")]
        public DateTime EndAt { get; set; }

        [Display(Name = "Habilitado")]
        public Boolean IsActive { get; set; }

        /*Relation ships*/
        [Display(Name = "Comercio")]
        public long? ComerceId { get; set; }

        [ForeignKey("ComerceId")]
        public virtual Comerce Comerce { get; set; }

        //[Display(Name = "Producto correspondiente")]
        public int ProductId { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
