﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using sng_web.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace sng_web.Models
{
    public class Comerce
    {
        private const int LgString = 400;
        private const int MdString = 200;
        private const int SmString = 100;
        private const int XsString = 2;

        public long ComerceId { get; set; }

        [Required(ErrorMessage = "¡Campo Nombre del comercio requerido!")]
        [StringLength(SmString, MinimumLength =2, ErrorMessage = "¡Nombre debe contener entre 2 y 100 caracteres!")]
        [Display(Name = "Comercio")]
        public string Name { get; set; }

        [Required(ErrorMessage = "¡Campo Descripción del comercio requerido!")]
        [StringLength(LgString, MinimumLength = 5, ErrorMessage = "Descripción debe contener entre 5 y 400 caracteres!")]
        [Display(Name = "Descripción")]
        public string Description { get; set; }

        [Required(ErrorMessage = "¡Campo Dirección del comercio requerido!")]
        [StringLength(LgString, MinimumLength = 5, ErrorMessage = "Dirección debe contener entre 5 y 400 caracteres!")]
        [Display(Name = "Dirección del comercio")]
        public string Address { get; set; }

        [Required(ErrorMessage = "¡Campo RUC del comercio requerido!")]
        [StringLength(20, MinimumLength = 5, ErrorMessage = "Dirección debe contener entre 5 y 20 caracteres!")]
        [Display(Name = "Ruc")]
        public string Ruc { get; set; }

        [Range(1, 5)]
        [Display(Name = "Puntuación")]
        public int Rating { get; set; }

        [Display(Name = "Logo")]
        public string UrlLogo { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Fecha de Creación ")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = false)]
        public DateTime CreatedAt { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Fecha de Modificación")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = false)]
        public DateTime UpdatedAt { get; set; }

        [Required(ErrorMessage = "¡Telefono requerido!")]
        [StringLength(50, MinimumLength = 8, ErrorMessage = "Campo Teléfono debe contener almenos 8 digitos!")]
        [Display(Name = "Teléfono")]
        public string Phone { get; set; }

        [BindRequired]
        public Double Longitude { get; set; }
        [BindRequired]
        public Double Latitude { get; set; }

        [Display(Name = "Aprovado")]
        public bool IsAproved { get; set; }

        [Display(Name = "Habilitado")]
        public Boolean IsActived { get; set; }

        /*Relation ships*/
        [Display(Name = "Categoria")]
        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public int? OwnerId { get; set; }
        [Display(Name = "Propietario")]
        public Owner Owner { get; set; }

        public virtual ICollection<Promotion> Promotions { get; set; }

        public virtual ICollection<Product> Products { get; set; } 

        public virtual ICollection<Cupon> Cupons { get; set; }

        public virtual ICollection<Vacant> Vacants { get; set; }

        public virtual ICollection<Royalty> Royalties { get; set; }

        public virtual ICollection<UserComerceRating> UserComerceRating { get; set; }

        public virtual ICollection<UserMessageComment> UserMessageComment { get; set; }

        public virtual ICollection<User_Favorite_Comerces> user_favorites { get; set; }

        public ICollection<ApplicationUser> Users { get; set; }
    }

    public class UserComerceRating
    {
        public long UserComerceRatingId { get; set; }

        public string UserId { get; set; }
        [Display(Name = "Usuario")]
        public ApplicationUser applicationUser { get; set; }

        public int? ComerceId { get; set; }
        [Display(Name = "Comercio")]
        public Comerce comerce { get; set; }

        [Range(1, 5)]
        [Display(Name = "Puntuación")]
        public int Rating { get; set; }
    }

    public class UserMessageComment
    {
        public long Id { get; set; }
        public string UserId { get; set; }
        [Display(Name = "Usuario")]
        [ForeignKey("UserId")]
        public ApplicationUser applicationUser { get; set; }

        public long? ComerceId { get; set; }
        [Display(Name = "Comercio")]
        [ForeignKey("ComerceId")]
        public Comerce comerce { get; set; }

        [StringLength(100, MinimumLength = 3, ErrorMessage = "¡El nombre de usuario no es válido!")]
        [Display(Name = "Nombre de usuario")]
        public string DisplayName { get; set; }

        public string AvatarURL { get; set; }

        public string URLPath { get; set; }

        [StringLength(500, MinimumLength = 3, ErrorMessage = "¡El comentario no es válido!")]
        [Display(Name = "Comentario")]
        public string Comment { get; set; }

        public DateTime TimeSpan { get; set; }
    }

    public class UserComerce
    {
        [NotMapped]
        public long Id { get; set; }

        [NotMapped]
        [Required(ErrorMessage = "¡Campo Nombre del comercio requerido!")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "¡Nombre debe contener entre 2 y 100 caracteres!")]
        [Display(Name = "Comercio")]
        public string Name { get; set; }

        [NotMapped]
        [Required(ErrorMessage = "¡Campo Dirección del comercio requerido!")]
        [StringLength(400, MinimumLength = 5, ErrorMessage = "Dirección debe contener entre 5 y 400 caracteres!")]
        [Display(Name = "Dirección del comercio")]
        public string Address { get; set; }

        [NotMapped]
        [Required(ErrorMessage = "¡Campo RUC del comercio requerido!")]
        [StringLength(20, MinimumLength = 5, ErrorMessage = "Ruc debe contener entre 5 y 20 caracteres!")]
        [Display(Name = "Ruc")]
        public string Ruc { get; set; }

        [NotMapped]
        [Display(Name = "Longitud")]
        public Double Longitud { get; set; }

        [NotMapped]
        [Display(Name = "Latitud")]
        public Double Latitud { get; set; }

        public int CategoryId { get; set; }

        //User
        [NotMapped]
        [Required(ErrorMessage = "¡Ingresar un EMail!")]
        [DataType(DataType.EmailAddress)]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "EMail debe contener entre 5 y 50 caracteres!")]
        [Display(Name = "Correo")]
        public string Email { get; set; }

        [NotMapped]
        [Required(ErrorMessage = "¡Ingresar un Teléfono!")]
        [DataType(DataType.PhoneNumber)]
        [StringLength(20, MinimumLength = 8, ErrorMessage = "Teléfono debe contener entre 5 y 20 caracteres!")]
        [Display(Name = "Teléfono")]
        public string Phone { get; set; }


        [NotMapped]
        [Required(ErrorMessage = "¡Campo Nombre del propietario requerido!")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "¡Nombre debe contener entre 2 y 100 caracteres!")]
        [Display(Name = "Nombre del propietario")]
        public string FirstName { get; set; }

        [NotMapped]
        [Required(ErrorMessage = "¡Campo Apellido del propietario requerido!")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "¡Apellido debe contener entre 2 y 100 caracteres!")]
        [Display(Name = "Apellido del propietario")]
        public string LastName { get; set; }


        [NotMapped]
        [Required(ErrorMessage = "¡Ingresar su Contraseña!")]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Contraseña debe contener mas 6 caracteres!")]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [NotMapped]
        [Required(ErrorMessage = "¡Confirmar Contraseña!")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "¡Verificación de Contraseña no valida!")]
        //[StringLength(20, MinimumLength = 6, ErrorMessage = "Contraseña debe contener mas 6 caracteres!")]
        [Display(Name = "Confirmar Contraseña")]
        public string ConfirmPassword { get; set; }
    }
}
