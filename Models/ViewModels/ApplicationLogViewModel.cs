﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static sng_web.Models.ApplicationLog;

namespace sng_web.Models.ViewModels
{
    public class ApplicationLogViewModel
    {
        public String Firstname { get; set; }
        public String LastName { get; set; }
        public String Commerce { get; set; }
        public String Message { get; set; }
        public LevelType Level { get; set; }
        public ActionType Action { get; set; }

        public DateTime Date { get; set; }
    }
}
