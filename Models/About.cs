using Microsoft.AspNetCore.Mvc.ModelBinding;
using sng_web.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace sng_web.Models
{
    public class About
    {
        public int AboutId { get; set; }

        [Required(ErrorMessage = "¡Campo Nombre de la empresa es obligatorio!")]
        [StringLength(400, MinimumLength =2, ErrorMessage = "¡Nombre de la empresa invalido!")]
        [Display(Name = "Nombre de la empresa")]
        public string Name { get; set; }

        [Required(ErrorMessage = "¡Campo Descripcion de la empresa es obligatorio!")]
        [StringLength(400, MinimumLength =2, ErrorMessage = "Descripcion de la empresa invalido!")]
        [Display(Name = "Descripcion de la empresa")]
        public string Description { get; set; }
        
        [Required(ErrorMessage = "¡Campo Direccion de la empresa es obligatorio!")]
        [StringLength(400, MinimumLength =2, ErrorMessage = "Direccion de la empresa invalido!")]
        [Display(Name = "Direccion de la empresa")]
        public string Address { get; set; }

        [Required(ErrorMessage = "¡Campo Telefono de la empresa es obligatorio!")]
        [StringLength(400, MinimumLength =2, ErrorMessage = "Telefono la empresa invalido!")]
        [Display(Name = "Telefono de la empresa")]
        public string Phone { get; set; }

        [StringLength(400, ErrorMessage = "Telefono de ventas invalido!")]
        [Display(Name = "Telefono de ventas")]
        public string PhoneSales { get; set; }

        [StringLength(400, ErrorMessage = "Telefono de ventas invalido!")]
        [Display(Name = "Pagina de soporte")]
        public string SuportPage { get; set; }

        [StringLength(400, ErrorMessage = "Correo de contacto invalido!")]
        [Display(Name = "Correo de contacto")]
        public string ContactEmail { get; set; }

        [StringLength(400,  ErrorMessage = "Terminos de privacidad invalido!")]
        [Display(Name = "Terminos de privacidad")]
        public string TermsPrivacy { get; set; }

        [Display(Name = "Tasas de cambio")]
        public string Exchanges { get; set; }

    }
}
