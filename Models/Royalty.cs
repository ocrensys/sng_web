﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace sng_web.Models
{
    public class Royalty
    {
        public int RoyaltyId { get; set; }

        [Required(ErrorMessage = "¡Nombre de la Regalia es requerido!")]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "Nombre de la regalia debe contener entre 5 y 50 caracteres!")]
        [Display(Name = "Nombre de la regalia")]
        public string Name { get; set; }

        [Required(ErrorMessage = "¡Descripción de la Regalia requerido!")]
        [StringLength(200, MinimumLength = 10, ErrorMessage = "Descripción de la promoción debe contener entre 10 y 200 caracteres!")]
        [Display(Name = "Descripción")]
        public string Description { get; set; }

        [Required(ErrorMessage = "¡Concepto de la Regalia requerido!")]
        [StringLength(200, MinimumLength = 10, ErrorMessage = "Descripción de la promoción debe contener entre 10 y 200 caracteres!")]
        [Display(Name = "En Concepto de")]
        public string Concept { get; set; }

        [Required(ErrorMessage = "¡Valor asignado es requerido!")]
        [DataType(DataType.Currency, ErrorMessage = "¡Ingrese un dato de tipo numérico!")]
        [Display(Name = "Valor de la Regalia")]
        public double Value { get; set; }

        [Required(ErrorMessage = "¡La Cantidad disponible es requerida!")]
        [Display(Name = "Cantidad disponible")]
        public int Count { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = false)]
        [Display(Name = "Fecha de creación")]
        public DateTime CreatedAt { get; set; }


        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = false)]
        [Display(Name = "Última actualización")]
        public DateTime UpdatedAt { get; set; }

        [Display(Name = "Habilitado")]
        public Boolean IsActive { get; set; }

        [Display(Name = "Entregado")]
        public Boolean Delivered { get; set; }

        /*Relation ships*/
        [Display(Name = "Comercio")]
        public long? ComerceId { get; set; }

        [ForeignKey("ComerceId")]
        public virtual Comerce Comerce { get; set; }
    }
}
