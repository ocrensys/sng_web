﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace sng_web.Models
{
    public class Cupon
    {
        public int CuponId { get; set; }

        //[Required(ErrorMessage = "¡Descuento del cupón es requerido!")]
        //[Range(0.00, 1.00)]
        [Display(Name = "Descuento del cupón")]
        public double Descount { get; set; }

        [Required(ErrorMessage = "¡Vigencia del cupón es requerida!")]
        [Display(Name = "Vigencia del cupón")]
        public int Validity { get; set; }

        [DataType(DataType.Currency)]
        [Display(Name = "Valor monetario")]
        public Decimal MoneyValue { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Fecha de creación")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = false)]
        public DateTime CreatedAt { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Ultima actualización")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = false)]
        public DateTime UpdatedAt { get; set; }

        [Display(Name = "Canjeado")]
        public Boolean IsChanged { get; set; }

        [Display(Name = "Habilitado")]
        public Boolean isActive { get; set; }

        /*Relation ships*/
        //[Display(Name = "Comercio correspondiente")]
        [Display(Name = "Comercio")]
        public long? ComerceId { get; set; }

        [ForeignKey("ComerceId")]
        public virtual Comerce Comerce { get; set; }
    }
}
