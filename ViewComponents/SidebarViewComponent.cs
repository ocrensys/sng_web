﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using sng_web.Models;
using System.Security.Claims;
using System;
using sng_web.Common;
using Microsoft.AspNetCore.Identity;
using sng_web.Data;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using sng_web.Common.Extensions;

namespace sng_web.ViewComponents
{
    public class SidebarViewComponent : ViewComponent
    {

        public SidebarViewComponent(){}

        public IViewComponentResult Invoke(string filter)
        {
            //you can do the access rights checking here by using session, user, and/or filter parameter
            var sidebars = new List<SidebarMenu>();
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
            var aplicationVacant_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.AplicationVacants);
            var count = (aplicationVacant_claim != null ) ? Convert.ToInt32(aplicationVacant_claim.Value) : 0; 
            //MENU ADMINISTRADOR
            if (this.User.IsInRole(sng_web.Controllers.AccountController.ROLE_ADMIN))
            {
                //COMO SE PUEDE OBSERVAR TIENE LOS MENUS COMPLETOS. NO SE SI HAY QUE AGREGAR OTROS PARA EL ADMINISTRADOR
                sidebars.Add(ModuleHelper.AddHeader("Menu de Navegación"));
                sidebars.Add(ModuleHelper.AddModule(ModuleHelper.Module.Home));
                sidebars.Add(ModuleHelper.AddModule(ModuleHelper.Module.Comercios, Tuple.Create(0, 0, 0)));
                sidebars.Add(ModuleHelper.AddTree("Productos"));
                sidebars.Last().TreeChild = new List<SidebarMenu>()
                {
                    ModuleHelper.AddModule(ModuleHelper.Module.Categorias),
                    ModuleHelper.AddModule(ModuleHelper.Module.Productos, Tuple.Create(0,0,0)),
                };
                sidebars.Add(ModuleHelper.AddModule(ModuleHelper.Module.Promociones, Tuple.Create(0, 0, 0)));
                sidebars.Add(ModuleHelper.AddModule(ModuleHelper.Module.Cupones, Tuple.Create(0, 0, 0)));
                sidebars.Add(ModuleHelper.AddModule(ModuleHelper.Module.Vacantes, Tuple.Create(0, 0, 0)));
                sidebars.Add(ModuleHelper.AddModule(ModuleHelper.Module.Regalias, Tuple.Create(0, 0, 0)));
                sidebars.Add(ModuleHelper.AddModule(ModuleHelper.Module.Usuarios, Tuple.Create(0, 0, 0)));
                sidebars.Add(ModuleHelper.AddModule(ModuleHelper.Module.TasaCambio, Tuple.Create(0, 0, 0)));
                sidebars.Add(ModuleHelper.AddModule(ModuleHelper.Module.About, Tuple.Create(0, 0, 0)));
            }
            //MENU COMERCIO
            else if(this.User.IsInRole(sng_web.Controllers.AccountController.ROLE_COMERCE))
            {
                sidebars.Add(ModuleHelper.AddModule(ModuleHelper.Module.Home));
                sidebars.Add(ModuleHelper.AddHeader("Menu de Navegación"));
                //SE BUSCA EN LOS CLAIMS DEL USUARIO AUTENTICADO CUAL ES SU COMERCIO
                sidebars.Add(new SidebarMenu
                {
                    Type = SidebarMenuType.Link,
                    Name = "Mi Comercio",
                    IconClassName = "fa fa-briefcase",
                    URLPath = Url.Action("Details", 
                                        "Comerces", 
                                        new { id = Convert.ToInt32(comerce_claim.Value) }),
                    LinkCounter = Tuple.Create(0, 0, 0),
                });
            }
            sidebars.Add(ModuleHelper.AddTree("Reportes"));
            sidebars.Last().TreeChild = new List<SidebarMenu>() {
                ModuleHelper.AddModule(ModuleHelper.Module.Actividades, Tuple.Create(0, 0, 0)),
                ModuleHelper.AddModule(ModuleHelper.Module.Reportes, Tuple.Create(0,0,0)),
            };
            sidebars.Add(ModuleHelper.AddModule(ModuleHelper.Module.Aplicaciones, Tuple.Create(count, 0, 0)));

            if (!this.User.Identity.IsAuthenticated)
            {
                sidebars.Add(ModuleHelper.AddTree("Account"));
                sidebars.Last().TreeChild = new List<SidebarMenu>()
                {
                    ModuleHelper.AddModule(ModuleHelper.Module.Login),
                    ModuleHelper.AddModule(ModuleHelper.Module.Register, Tuple.Create(1, 1, 1)),
                };
            }
           

            return View(sidebars);
        }
    }
}
