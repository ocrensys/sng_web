﻿using sng_web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Security.Claims;
using sng_web.Common;
using sng_web.Common.Extensions;
using System.Linq;
using sng_web.Data;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System;

namespace sng_web.ViewComponents
{
    public class MenuMessageViewComponent : ViewComponent
    {
        private readonly ApplicationDbContext _context;
        private object Date;

        public MenuMessageViewComponent(ApplicationDbContext context)
        {
            _context = context;
        }

        //public  IViewComponentResult Invoke (string filter)
        public async Task<IViewComponentResult> InvokeAsync(string filter)
        {
            var messages =  await GetData();
            return View(messages);
        }

        //private async  Task<IEnumerable<UserMessageComment>> GetData()
        private async Task<IEnumerable<UserMessageComment>> GetData()
        {
            var messages = new List<UserMessageComment>();

            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            if (User.IsInRole("Admin"))
            {
                messages = await _context.UserMessageComment.Include(m=>m.applicationUser).ToListAsync();
            }
            else // is comerce roles
            {
                var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                var ComerceId = int.Parse(comerce_claim.Value);
                messages = await _context.UserMessageComment
                    .Include(m=>m.applicationUser)
                    .Where(comment => comment.ComerceId == ComerceId)
                    .ToListAsync();
            }

            //messages = await _context.UserMessageComment.ToListAsync();


            return messages;
        }
    }
}

//messages.Add(new UserMessageComment
//{
//    Id = 1,
//    UserId = ((ClaimsPrincipal)User).GetUserProperty(CustomClaimTypes.NameIdentifier),
//    DisplayName = "Support Team",
//    AvatarURL = "/images/user.png",
//    ShortDesc = "Why not buy a new awesome theme?",
//    TimeSpan = "5 mins",
//    URLPath = "#",
//});
