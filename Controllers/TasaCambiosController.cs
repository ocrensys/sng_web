﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sng_web.Data;
using sng_web.Models;

namespace sng_web.Controllers
{
    public class TasaCambiosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TasaCambiosController(ApplicationDbContext context)
        {
            _context = context;
        }
        // GET: TasaCambios
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.TasaCambio.ToListAsync());
        }

        // GET: TasaCambios/Details/5
        //public async Task<IActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var tasaCambio = await _context.TasaCambio
        //        .SingleOrDefaultAsync(m => m.ID == id);
        //    if (tasaCambio == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(tasaCambio);
        //}

        // GET: TasaCambios/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: TasaCambios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Nombre_Banco,Compra,Venta,Fecha")] TasaCambio tasaCambio)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tasaCambio);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tasaCambio);
        }

        // GET: TasaCambios/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tasaCambio = await _context.TasaCambio.SingleOrDefaultAsync(m => m.ID == id);
            if (tasaCambio == null)
            {
                return NotFound();
            }
            return View(tasaCambio);
        }

        // POST: TasaCambios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Nombre_Banco,Compra,Venta,Fecha")] TasaCambio tasaCambio)
        {
            if (id != tasaCambio.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tasaCambio);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TasaCambioExists(tasaCambio.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tasaCambio);
        }

        // GET: TasaCambios/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tasaCambio = await _context.TasaCambio
                .SingleOrDefaultAsync(m => m.ID == id);
            if (tasaCambio == null)
            {
                return NotFound();
            }

            return View(tasaCambio);
        }

        // POST: TasaCambios/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tasaCambio = await _context.TasaCambio.SingleOrDefaultAsync(m => m.ID == id);
            _context.TasaCambio.Remove(tasaCambio);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TasaCambioExists(int id)
        {
            return _context.TasaCambio.Any(e => e.ID == id);
        }
    }
}
