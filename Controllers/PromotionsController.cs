﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sng_web.Common;
using sng_web.Data;
using sng_web.Models;
using sng_web.Services;

namespace sng_web.Controllers
{
    public class PromotionsController : Controller
    {
        private readonly INotifications _notification;
        private readonly ApplicationDbContext _context;

        public PromotionsController(ApplicationDbContext context, INotifications notification)
        {
            _context = context;
            _notification = notification;
        }

        public async Task<ICollection<Promotion>> GetPromotions()
        {
            var promotions = await _context.Promotion.Include(c => c.Comerce)
                                                    .OrderBy(p => p.ComerceId)
                                                    .ToListAsync();
            List<Promotion> list = new List<Promotion>();
            foreach (var p in promotions)
            {
                p.Comerce = await _context.Comerce.FirstOrDefaultAsync(c => c.ComerceId == p.ComerceId);
                list.Add(p);
            }

            return (list);
        }

        public async Task<ICollection<Promotion>> GetPromotionsByComerce(int ComerceId, int skip, int take, string orderby)
        {
            var promotions = await _context.Promotion.Where(p => p.ComerceId == ComerceId)
                                                    .Include(c => c.Comerce)
                                                    .Include(p => p.Products)
                                                    .OrderBy(p => p.ProductId)
                                                    .ToListAsync();
            List<Promotion> list = new List<Promotion>();
            foreach (var p in promotions)
            {
                var products = await _context.Product.Where(c => c.ProductId == p.ProductId).ToListAsync();
                if (products.Count > 0)
                    p.Products = products;
                p.Comerce = await _context.Comerce.FirstOrDefaultAsync(c => c.ComerceId == ComerceId);
                list.Add(p);
            }

            return (list);
        }

        public async Task<List<IdentityError>> AddPromotion(string Name, string Description, double Descount, DateTime StartAt, DateTime EndAt, int ProductId, int ComerceId)
        {
            List<IdentityError> errors = new List<IdentityError>();
            var comerce = await _context.Comerce.SingleOrDefaultAsync(m => m.ComerceId == ComerceId);
            Promotion promotion = new Promotion()
            {
                Name = Name,
                Description = Description,
                Descount = (Descount / 100),
                StartAt = StartAt,
                EndAt = EndAt,
                ProductId = ProductId,
                ComerceId = ComerceId,
                Comerce = comerce,
                IsActive = true,
            };
            _context.Add(promotion);
            await _context.SaveChangesAsync();

            if(promotion.IsActive && promotion.Comerce.IsAproved){
                var data = new Dictionary<string,string>(){
                    {"Model", "promotion"},
                    {"ObjectId", promotion.PromotionId.ToString()},
                    {"ComerceId", promotion.ComerceId.ToString()}
                };
                await _notification.CreateNotification("¡Nueva promocion disponible!", data);
            }
            

            errors.Add(new IdentityError()
            {
                Code = "Saved",
                Description = "Promoción guardada exitosamente."
            });

            return errors;
        }
        
        [Authorize(Roles = "Admin")]
        // GET: Promotions
        public async Task<IActionResult> Index()
        {
            return View(await _context.Promotion.ToListAsync());
        }

        // GET: Promotions/Details/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var promotion = await _context.Promotion
                .SingleOrDefaultAsync(m => m.PromotionId == id);
            if (promotion == null)
            {
                return NotFound();
            }

            return View(promotion);
        }

        // GET: Promotions/Create
        [Authorize(Roles ="Admin")]
        public IActionResult Create()
        {
            ViewData["ProductId"] = new SelectList(_context.Product, "ProductId", "Name");
            ViewData["ComerceId"] = new SelectList(_context.Comerce, "ComerceId", "Name");

            return View();
        }

        // POST: Promotions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("PromotionId,Name,Description,Descount,StartAt,EndAt,IsActive,ProductId")] Promotion promotion)
        {
            if (ModelState.IsValid)
            {
                promotion.Descount = promotion.Descount/100;
                _context.Add(promotion);

                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(promotion);
        }

        // GET: Promotions/Edit/5
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var promotion = await _context.Promotion.SingleOrDefaultAsync(m => m.PromotionId == id);
            if (promotion == null)
                return NotFound();

            //Validate authentication from user
            ViewBag.Refer = Request.Headers["Referer"].ToString();
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            if (!User.IsInRole("Admin"))
            {
                var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                if (comerce_claim == null || Convert.ToInt32(comerce_claim.Value) != promotion.ComerceId)
                    return NotFound();
            }

            promotion.Descount *= 100;
            return View(promotion);
        }

        // POST: Promotions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> Edit(int id, [Bind("PromotionId,Name,Description,Descount,StartAt,EndAt,IsActive,ProductId, ComerceId")] Promotion promotion)
        {
            if (id != promotion.PromotionId)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    promotion.Descount /= 100;
                    _context.Update(promotion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PromotionExists(promotion.PromotionId))
                        return NotFound();
                    else
                        throw;
                }

                if (Request.Form["Refer"].ToString() != "")
                    return Redirect(Request.Form["Refer"].ToString());
                else
                    return RedirectToAction(nameof(Index));
            }
            return View(promotion);
        }

        // GET: Promotions/Delete/5
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var promotion = await _context.Promotion.SingleOrDefaultAsync(m => m.PromotionId == id);
            if (promotion == null)
                return NotFound();

            var comerce = await _context.Comerce.SingleOrDefaultAsync(m => m.ComerceId == promotion.ComerceId);
            if (comerce != null)
                promotion.Comerce = comerce;

            //Validate authentication from user
            ViewBag.Refer = Request.Headers["Referer"].ToString();
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            if (!User.IsInRole("Admin"))
            {
                var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                if (comerce_claim == null || Convert.ToInt32(comerce_claim.Value) != promotion.ComerceId)
                    return NotFound();
            }

            return View(promotion);
        }

        // POST: Promotions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var promotion = await _context.Promotion.SingleOrDefaultAsync(m => m.PromotionId == id);
            _context.Promotion.Remove(promotion);
            await _context.SaveChangesAsync();

            if (Request.Form["Refer"].ToString() != "")
                return Redirect(Request.Form["Refer"].ToString());
            else
                return RedirectToAction(nameof(Index));
        }

        private bool PromotionExists(int id)
        {
            return _context.Promotion.Any(e => e.PromotionId == id);
        }
    }
}
