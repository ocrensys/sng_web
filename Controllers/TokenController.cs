using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Protocols;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using sng_web.Auth;
using sng_web.Common;
using sng_web.Data;
using sng_web.Helpers;
using sng_web.Models;
using sng_web.Models.AccountViewModels;

namespace sng_web.Controllers
{
    public class EntityViewModel
    {

        [Required]
        public int id { get; set; }
        

    }

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/")]
    public class TokenController : Controller
    {

        public static string ROLE_ADMIN = "Admin";
        public static string ROLE_COMERCE = "Comerce";
        public static string ROLE_CLIENT = "Client";

        private IConfiguration _config;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IJwtFactory _jwtFactory;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public TokenController(IConfiguration config, 
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager,
            ILogger<AccountController> logger,
            IJwtFactory jwtFactory, 
            IOptions<JwtIssuerOptions> jwtOptions,
            ApplicationDbContext context)
        {
            _config = config;
            _userManager = userManager;
            _roleManager = roleManager;
            _jwtFactory = jwtFactory;
            _jwtOptions = jwtOptions.Value;
            _context = context;
            _logger = logger;
        }
        

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Post([FromBody]LoginViewModel credentials)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var identity = await GetClaimsIdentity(credentials.Email, credentials.Password);
            if (identity == null)
            {
                return BadRequest(Errors.AddErrorToModelState("login_failure", "Invalid username or password.", ModelState));
            }

            var jwt = await Tokens.GenerateJwt(identity, _jwtFactory, credentials.Email, _jwtOptions, new JsonSerializerSettings { Formatting = Formatting.Indented });
            return new OkObjectResult(jwt);
        }

        [HttpPost("refresh")]
        [AllowAnonymous]
        public async Task<IActionResult> RefreshToken(String token)
        {
            var jwtToken = await _jwtFactory.DecodeToken(token);
            var claim_email = jwtToken.Claims.Where(c => c.Type == "sub").FirstOrDefault();

            var identity = await GetClaimsIdentity(claim_email.Value);
            if (identity == null)
            {
                return BadRequest(Errors.AddErrorToModelState("login_failure", "Invalid username or password.", ModelState));
            }

            var jwt = await Tokens.GenerateJwt(identity, _jwtFactory, claim_email.Value, _jwtOptions, new JsonSerializerSettings { Formatting = Formatting.Indented });
            return new OkObjectResult(jwt);
            
        }

        [HttpPost("changepassword")]
        public async Task<IActionResult> ChangePassword(String oldpassword, String newpassword)
        {
            ClaimsPrincipal currentUser = this.User;
            var user_id = currentUser.Claims.Where(c => c.Type == "id").FirstOrDefault().Value;

            ApplicationUser user = await _context.ApplicationUser
                .Include(u => u.Favorite_Products)
                .Include(u => u.Favorite_Comerces)
                .Where(u => u.Id == user_id).FirstOrDefaultAsync();

            if (await _userManager.CheckPasswordAsync(user, oldpassword))
            {
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var result = await _userManager.ResetPasswordAsync(user, code, newpassword);
                if (result.Succeeded)
                {
                    return StatusCode(200, new { Message = "Password successfuly changed" });

                }
                else
                {
                    return StatusCode(500, new { Message = "Could not change password", result.Errors });
                }
            }
            else
            {
                return StatusCode(500, new { Message = "Invalid old password" });

            }

        }


        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register(String token, String firstName, String lastName)
        {
            const string auth0Domain = "https://securetoken.google.com/sng-app-112d7"; // Your Auth0 domain
            const string auth0Audience = "sng-app-112d7"; // Your API Identifier

            IConfigurationManager<OpenIdConnectConfiguration> configurationManager = 
                new ConfigurationManager<OpenIdConnectConfiguration>($"{auth0Domain}/.well-known/openid-configuration", new OpenIdConnectConfigurationRetriever());
            OpenIdConnectConfiguration openIdConfig = await configurationManager.GetConfigurationAsync(CancellationToken.None);

            TokenValidationParameters validationParameters =
            new TokenValidationParameters
            {
                ValidIssuer = auth0Domain,
                ValidAudiences = new[] { auth0Audience },
                IssuerSigningKeys = openIdConfig.SigningKeys
            };

            SecurityToken validatedToken;
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            var tokenuser = handler.ValidateToken(token, validationParameters, out validatedToken);

            var Email = tokenuser.Identities.FirstOrDefault().Claims.Where(c => c.Type == CustomClaimTypes.Email).FirstOrDefault().Value;
            var FirstName = tokenuser.Identities.FirstOrDefault().Claims.Where(c => c.Type == "name").FirstOrDefault() !=null ?
                            tokenuser.Identities.FirstOrDefault().Claims.Where(c => c.Type == "name").FirstOrDefault().Value: null;
            var AvatarURL = tokenuser.Identities.FirstOrDefault().Claims.Where(c => c.Type == "picture").FirstOrDefault() !=null ?
                            tokenuser.Identities.FirstOrDefault().Claims.Where(c => c.Type == "picture").FirstOrDefault().Value: null;

            if (FirstName == null)
                FirstName = firstName;

            if (ModelState.IsValid)
            {


                var user = await _userManager.FindByEmailAsync(Email);
                if (user == null)
                {
                    user = new ApplicationUser
                    {
                        UserName = Email,
                        Email = Email,
                        //extended properties
                        FirstName = FirstName,
                        LastName = lastName,
                        AvatarURL = AvatarURL == null ? "/images/user.png" : AvatarURL,
                        DateRegistered = DateTime.UtcNow.ToString(),
                        Position = "",
                        NickName = "",
                    };

                    var result = await _userManager.CreateAsync(user);
                    if (result.Succeeded)
                    {

                        _logger.LogInformation("User created with firebase auth.");

                        var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                        var callbackUrl = Url.EmailConfirmationLink(user.Id, code, Request.Scheme);


                        var exist_roleAdmin = await _roleManager.RoleExistsAsync(ROLE_ADMIN);
                        var exist_roleComerce = await _roleManager.RoleExistsAsync(ROLE_COMERCE);
                        var exist_roleClient = await _roleManager.RoleExistsAsync(ROLE_CLIENT);

                        //Admin Role 
                        if (!exist_roleAdmin)
                        {
                            var role = new IdentityRole(ROLE_ADMIN);
                            var response = await _roleManager.CreateAsync(role);

                            if (response.Succeeded)
                            {
                                _logger.LogInformation("Admin role created.");
                            }
                        }


                        //Comerce Role
                        if (!exist_roleComerce)
                        {
                            var role = new IdentityRole(ROLE_COMERCE);
                            var response = await _roleManager.CreateAsync(role);

                            if (response.Succeeded)
                            {
                                _logger.LogInformation("Comerce role created.");
                            }
                        }

                        //Client Role --CREA Y ASIGNA AL USUARIO AL ROLE DE USUARIO--
                        if (!exist_roleClient)
                        {
                            var role = new IdentityRole(ROLE_CLIENT);
                            var response = await _roleManager.CreateAsync(role);

                            if (response.Succeeded)
                            {
                                _logger.LogInformation("Client role created.");
                                await _userManager.AddToRoleAsync(user, ROLE_CLIENT);
                                _logger.LogInformation("User created a new account with password.");
                            }
                        }
                        else
                        {
                            await _userManager.AddToRoleAsync(user, ROLE_CLIENT);
                            _logger.LogInformation("User created a new account with password.");
                        }

                        _logger.LogInformation(3, "User created a new account with password.");

                        await _userManager.AddClaimAsync(user, new Claim(CustomClaimTypes.GivenName, user.FirstName));
                        await _userManager.AddClaimAsync(user, new Claim(CustomClaimTypes.Surname, user.LastName==null?"":user.LastName));
                        await _userManager.AddClaimAsync(user, new Claim(CustomClaimTypes.AvatarURL, user.AvatarURL));



                        var identity = await GetClaimsIdentity(Email);
                        if (identity == null)
                        {
                            return BadRequest(Errors.AddErrorToModelState("login_failure", "Invalid username or password.", ModelState));
                        }
                        var jwt = await Tokens.GenerateJwt(identity, _jwtFactory, Email, _jwtOptions, new JsonSerializerSettings { Formatting = Formatting.Indented });
                        return new OkObjectResult(jwt);

                    }
                    else
                        return BadRequest(Errors.AddErrorToModelState("register_failure", "Cliente could not be created", ModelState));
                }
                else
                {
                    var identity = await GetClaimsIdentity(Email);
                    if (identity == null)
                    {
                        return BadRequest(Errors.AddErrorToModelState("login_failure", "Invalid username", ModelState));
                    }
                    var jwt = await Tokens.GenerateJwt(identity, _jwtFactory, Email, _jwtOptions, new JsonSerializerSettings { Formatting = Formatting.Indented });
                    return new OkObjectResult(jwt);
                }
            }
            else
            {
                return BadRequest(Errors.AddErrorToModelState("register_failure", "Incomplete data.", ModelState));
            }

        }

        [Produces("application/json")]
        [HttpPost("set_user")]
        public async Task<IActionResult> SetUser(
            String first_name,
            String last_name,
            String email,
            String phone
        )
        {
            try
            {
                ClaimsPrincipal currentUser = this.User;
                var user_id = currentUser.Claims.Where(c => c.Type == "id").FirstOrDefault().Value;

                ApplicationUser user = await _context.ApplicationUser
                    .Include(u => u.Favorite_Products)
                    .Include(u => u.Favorite_Comerces)
                    .Where(u => u.Id == user_id).FirstOrDefaultAsync();

                if(first_name!=null || !String.IsNullOrEmpty(first_name))
                    user.FirstName = first_name;
                if (last_name != null || !String.IsNullOrEmpty(last_name))
                    user.LastName = last_name;
                if (email != null || !String.IsNullOrEmpty(email))
                    user.Email = email;
                if (phone != null || !String.IsNullOrEmpty(phone))
                    user.PhoneNumber = phone;


                var file = HttpContext.Request.Form.Files.FirstOrDefault(f => f.Name == "avatar");
                if ((file != null && file.Length > 0))
                    user.AvatarURL= await Utilities.upload_fileAsync(HttpContext, "images/uploads/accounts/");
                _context.Update(user);
                await _context.SaveChangesAsync();
                return StatusCode(200, new {
                    Message = "Usuario actualizado exitosamente",
                    usuario = new {
                        user.Id,
                        user.FirstName,
                        user.LastName,
                        user.Email,
                        user.PhoneNumber,
                        user.AvatarURL,
                        user.NickName,
                        user.UserName
                    }});
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return StatusCode(500, new { Message = "internal error " + ex.Message });
            }catch (Exception ex)
            {
                return StatusCode(500, new { Message = "internal error " + ex.Message });
            }

        }

        private async Task<ClaimsIdentity> GetClaimsIdentity(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                return await Task.FromResult<ClaimsIdentity>(null);

            // get the user to verifty
            var userToVerify = await _userManager.FindByNameAsync(userName);

            if (userToVerify == null) return await Task.FromResult<ClaimsIdentity>(null);

            // check the credentials
            if (await _userManager.CheckPasswordAsync(userToVerify, password))
            {
                return await Task.FromResult(_jwtFactory.GenerateClaimsIdentity(userToVerify));
            }

            // Credentials are invalid, or account doesn't exist
            return await Task.FromResult<ClaimsIdentity>(null);
        }

        private async Task<ClaimsIdentity> GetClaimsIdentity(string userName)
        {
            if (string.IsNullOrEmpty(userName))
                return await Task.FromResult<ClaimsIdentity>(null);

            // get the user to verifty
            var userToVerify = await _userManager.FindByEmailAsync(userName);

            if (userToVerify == null) return await Task.FromResult<ClaimsIdentity>(null);

            return await Task.FromResult(_jwtFactory.GenerateClaimsIdentity(userToVerify));

        }


        [HttpPost("get_categories")]
        [AllowAnonymous]
        public async Task<ICollection<Object>> GetCategories()
        {
            var modelobjects = await _context.Category
            .Include(c=>c.Comerces)
            .Select(c=> new {
                c.Name,
                c.IsActive,
                c.UrlImage,
                c.Icon,
                c.CategoryId,
                comerces = _context.Comerce.Where(cm=>cm.CategoryId== c.CategoryId && cm.IsActived && cm.IsAproved).ToList()
            })
            .OrderBy(c=>c.Name)
            .ToListAsync();
            
            List<Object> list = new List<Object>();
            foreach (var o in modelobjects)
            {
                list.Add(o);
            }
            return (list);
        }

        [HttpPost("get_comerces")]
        [AllowAnonymous]
        public async Task<ICollection<Object>> GetComerces()
        {
            var modelobjects = await _context.Comerce.Include(c=>c.Category)
            .Select(c=> new
                {
                    c.ComerceId,
                    c.Address,
                    c.CreatedAt,
                    c.IsActived,
                    c.IsAproved,
                    c.Latitude,
                    c.Longitude,
                    c.Name,
                    c.Owner,
                    c.Rating,
                    c.Ruc,
                    c.UpdatedAt,
                    c.UrlLogo,
                    c.Description,
                    categoria = c.Category.Name,
                    c.CategoryId
                }
            )
            .Where(c=>c.IsActived && c.IsAproved)
            .OrderBy(c=>c.Name)
            .ToListAsync();

            List<Object> list = new List<Object>();
            foreach (var o in modelobjects)
            {
                list.Add(o);
            }
            return (list);
        }

        [HttpPost("get_comerce")]
        [AllowAnonymous]
        public async Task<ICollection<Object>> GetComerce([FromBody] EntityViewModel entity)
        {
            var modelobjects = await _context.Comerce
                .Include(c=>c.Promotions)
                .Include(c => c.Products)
                .Include(c => c.Vacants)
                .Include(c => c.Royalties)
                .Include(c => c.Cupons)
                .Include(c => c.Category)
                .Where(c=>c.ComerceId==entity.id)
                .Where(c=>c.IsActived && c.IsAproved)
                .OrderBy(c=>c.Name)
                .ToListAsync();

            
            List<Object> list = new List<Object>();
            foreach (var o in modelobjects)
            {
                list.Add(o);
            }
            return (list);
        }

        
        [HttpPost("get_products")]
        [AllowAnonymous]
        public async Task<ICollection<Product>> GetProducts()
        {
            var products = await _context.Product.Include(c => c.Comerce)
                                                    .OrderBy(p => p.ComerceId)
                                                .ToListAsync();
            List<Product> list = new List<Product>();
            foreach (var p in products)
            {
                p.Comerce = await _context.Comerce.FirstOrDefaultAsync(c => c.ComerceId == p.ComerceId);
                list.Add(p);
            }
            list.OrderBy(p => p.ComerceId);
            return (list);
        }

        [HttpPost("get_product")]
        [AllowAnonymous]
        public async Task<ICollection<Product>> GetProduct([FromBody] EntityViewModel entity)
        {
            var products = await _context.Product.Include(c => c.Comerce)
                                                    .Where(p=>p.ProductId==entity.id)
                                                .ToListAsync();
            List<Product> list = new List<Product>();
            foreach (var p in products)
            {
                p.Comerce = await _context.Comerce.FirstOrDefaultAsync(c => c.ComerceId == p.ComerceId);
                list.Add(p);
            }
            list.OrderBy(p => p.ComerceId);
            return (list);
        }

        [Produces("application/json")]
        [HttpPost("get_favorite")]
        public async Task<IActionResult> GetFavorite()
        {
            ClaimsPrincipal currentUser = this.User;
            var user_id = currentUser.Claims.Where(c => c.Type == "id").FirstOrDefault().Value;

            ApplicationUser user = await _context.ApplicationUser
                .Include(u => u.Favorite_Products)
                    .ThenInclude(p=>p.Product)
                .Include(u => u.Favorite_Comerces)
                    .ThenInclude(c=>c.Comerce)
                .Where(u => u.Id == user_id)
                .FirstOrDefaultAsync();


            var favorit_products = new HashSet<Object>();
            var favorit_comerces = new HashSet<Object>();

            foreach (var favorite in user.Favorite_Products)
            {
                var product = favorite.Product;
                favorit_products.Add(new {
                    product.ProductId,
                    product.Name,
                    product.Description,
                    product.Price,
                    product.UrlImage,
                    product.IsActived,
                    product.Currency
                });
            }
            foreach (var favorite in user.Favorite_Comerces)
            {
                favorit_comerces.Add(Json(favorite.Comerce).Value);
            }
            

            return StatusCode(200, new { favorit_products, favorit_comerces });

        }

        [Produces("application/json")]
        [HttpPost("set_favorite")]
        public async Task<IActionResult> SetFavorite(
            String favorite_type,
            int id_instance
        )
        {
            ClaimsPrincipal currentUser = this.User;
            var user_id = currentUser.Claims.Where(c => c.Type == "id").FirstOrDefault().Value;

            ApplicationUser user = await _context.ApplicationUser
                .Include(u=>u.Favorite_Products)
                .Include(u=>u.Favorite_Comerces)
                .Where(u => u.Id == user_id).FirstOrDefaultAsync();

            switch ( favorite_type )
            {
                case "product":
                    
                    var product = await _context.Product
                        .Where(p => p.ProductId == id_instance)
                        .FirstOrDefaultAsync();

                    if( user.Favorite_Products.Where( f=> f.Product == product).Count()>0 )
                        return StatusCode(200, new { Message = favorite_type + " favorite allready added" });

                    if ( product != null)
                    {
                        user.Favorite_Products.Add(new User_Favorite_Products
                        {
                            ProductId = product.ProductId,
                            Product = product,
                            Id = user.Id,
                            User = user
                        });

                        await _context.SaveChangesAsync();
                        return StatusCode(200, new { Message = favorite_type + " favorite succesfuly added" });

                    }
                    else
                        return StatusCode(400, new { Message = favorite_type + " not found" });

                case "comerce":
                    var comerce = await _context.Comerce
                        .Where(p => p.ComerceId == id_instance)
                        .FirstOrDefaultAsync();

                    if (user.Favorite_Comerces.Where(f => f.Comerce == comerce).Count() > 0)
                        return StatusCode(200, new { Message = favorite_type + " favorite allready added" });

                    if (comerce != null)
                    {
                        user.Favorite_Comerces.Add(new User_Favorite_Comerces
                        {
                            ComerceId = comerce.ComerceId,
                            Comerce = comerce,
                            Id = user.Id,
                            User = user
                        });

                        await _context.SaveChangesAsync();
                        return StatusCode(200, new { Message = favorite_type + " favorite succesfuly added" });

                    }
                    else
                        return StatusCode(400, new { Message = favorite_type + " not found" });
            }
            return StatusCode(400, new { Message = favorite_type + " type not supported", favorite_type, id_instance } );
            
        }

        [Produces("application/json")]
        [HttpPost("del_favorite")]
        public async Task<IActionResult> DelFavorite(
            String favorite_type,
            int id_instance
        )
        {
            ClaimsPrincipal currentUser = this.User;
            var user_id = currentUser.Claims.Where(c => c.Type == "id").FirstOrDefault().Value;

            ApplicationUser user = await _context.ApplicationUser
                .Include(u => u.Favorite_Products)
                .Include(u => u.Favorite_Comerces)
                .Where(u => u.Id == user_id).FirstOrDefaultAsync();

            switch (favorite_type)
            {
                case "product":

                    var product = await _context.Product
                        .Where(p => p.ProductId == id_instance)
                        .FirstOrDefaultAsync();

                    
                    if (product != null)
                    {
                        if (user.Favorite_Products.Where(f => f.Product == product).Count() > 0)
                        {
                            var favorite = user.Favorite_Products.Where(f => f.Product == product).FirstOrDefault();
                            user.Favorite_Products.Remove(favorite);
                            await _context.SaveChangesAsync();
                            return StatusCode(200, new { Message = favorite_type + " favorite allready removed" });
                        }
                        else
                        {
                            return StatusCode(400, new { Message = favorite_type + " favorite instance not found" });
                        }
                    }
                    else
                        return StatusCode(400, new { Message = favorite_type + " not found" });

                case "comerce":
                    var comerce = await _context.Comerce
                        .Where(p => p.ComerceId == id_instance)
                        .FirstOrDefaultAsync();
                    
                    if (comerce != null)
                    {

                        if (user.Favorite_Comerces.Where(f => f.Comerce == comerce).Count() > 0)
                        {
                            var favorite = user.Favorite_Comerces.Where(f => f.Comerce == comerce).FirstOrDefault();
                            user.Favorite_Comerces.Remove(favorite);
                            await _context.SaveChangesAsync();
                            return StatusCode(200, new { Message = favorite_type + " favorite allready removed" });
                        }
                        else
                        {
                            return StatusCode(400, new { Message = favorite_type + " favorite instance not found" });
                        }
                    }
                    else
                        return StatusCode(400, new { Message = favorite_type + " not found" });
            }
            return StatusCode(400, new { Message = favorite_type + " type not supported", favorite_type, id_instance });

        }


        [Produces("application/json")]
        [HttpPost("aplie_vacant")]
        public async Task<IActionResult> Aplie_Vacant(
            [Bind("VacantId,Applicant,ApplicantPhone,ApplicantEmail,Comments,UrlCurriculum")] VacantAplication vacantapplication)
        {
            ClaimsPrincipal currentUser = this.User;
            var user_id = currentUser.Claims.Where(c => c.Type == "id").FirstOrDefault().Value;

            ApplicationUser user = await _context.ApplicationUser
                .Include(u => u.Favorite_Products)
                .Include(u => u.Favorite_Comerces)
                .Where(u => u.Id == user_id).FirstOrDefaultAsync();

            var vacant = await _context.Vacant.Where(v => v.VacantId == vacantapplication.VacantId).FirstOrDefaultAsync();
            if (vacant != null)
            {
                vacantapplication.AplicationDate = DateTime.UtcNow;
                vacantapplication.UserId = user.Id;
                if (TryValidateModel(vacantapplication))
                {
                    try
                    {
                        var file = HttpContext.Request.Form.Files.FirstOrDefault(f => f.Name == "urlCurriculum");
                        if ((file != null && file.Length > 0))
                            vacantapplication.UrlCurriculum = await Utilities.upload_fileAsync(HttpContext, "images/uploads/vacants/");
                        _context.Add(vacantapplication);
                        await _context.SaveChangesAsync();
                        return StatusCode(200, vacantapplication);
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                    return StatusCode(500, new { Message = "internal error " + ex.Message });
                    }

                }
                else
                {
                    var errorList = ModelState.ToDictionary(
                                        kvp => kvp.Key,
                                        kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
                                    );
                    return StatusCode(500, new { Message = "internal error", errorList });
                }
                
            }
            else
            {
                return StatusCode(400, new { Message = "Vacant not fund" });
            }


        }

        [Produces("application/json")]
        [HttpPost("get_vacants")]
        public async Task<IActionResult> GetVacants()
        {
            ClaimsPrincipal currentUser = this.User;
            var user_id = currentUser.Claims.Where(c => c.Type == "id").FirstOrDefault().Value;

            var vacants = await _context.VacantAplications
            .Include(v => v.Vacant).ThenInclude(v=>v.Comerce)
            .Where(v=>v.User.Id== user_id).ToListAsync();
            
            return StatusCode(200,  vacants );

        }

        [Produces("application/json")]
        [HttpPost("get_tasacambio")]
        [AllowAnonymous]
        public async Task<IActionResult> get_TasaCambio()
        {
            var Tc = await _context.TasaCambio.ToListAsync();
            return StatusCode(200,  Tc );

        }

        [Produces("application/json")]
        [HttpPost("get_about")]
        [AllowAnonymous]
        public async Task<IActionResult> get_About()
        {
            var about = await _context.About.ToListAsync();
            return StatusCode(200,  about.Last() );

        }

        [Produces("application/json")]
        [HttpPost("set_comment")]
        public async Task<IActionResult> SetComment(
            string _UserId,
            int _ComerceId,
            string _Comment
        )
        {
            ApplicationUser user = await _context.ApplicationUser
                .Where(u => u.Id == _UserId).FirstOrDefaultAsync();

            var comerce = await _context.Comerce.Where(c => c.ComerceId == _ComerceId).FirstAsync();
            if (comerce != null)
            {
                var user_comment = new UserMessageComment
                {
                    UserId = user.Id,
                    AvatarURL = user.AvatarURL,
                    DisplayName = user.FirstName +" "+ user.LastName,
                    ComerceId = _ComerceId,
                    Comment = _Comment,
                    TimeSpan = DateTime.Now,
                    URLPath = ""
                };

                try
                {
                    _context.Add(user_comment);
                    await _context.SaveChangesAsync();

                    return StatusCode(200, new
                    {
                        Data = user_comment,
                    });
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    return StatusCode(500, new { Message = "internal error " + ex.Message });
                }
            }
            else 
                return StatusCode(404, new { Message = "Comercio no encontrado " });
        }

        [Produces("application/json")]
        [HttpGet("get_comments")]
        public async Task<IActionResult> GetComments(
            int _ComerceId
        )
        {

            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            if (User.IsInRole("Admin"))
            {
                string comerceId = HttpContext.Request.Query["comerceId"].ToString();
                if (comerceId!=null) {
                    var messages = await _context.UserMessageComment
                                            .Include(c => c.applicationUser)
                                            .Where(c=>c.ComerceId==Convert.ToInt32(comerceId)).ToListAsync();
                    return StatusCode(200,  messages );

                }else {
                    var messages = await _context.UserMessageComment.Include(c => c.applicationUser).ToListAsync();
                    return StatusCode(200,  messages );
                }
                
            }
            else // is comerce roles
            {
                var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                var ComerceId = int.Parse(comerce_claim.Value);
                var messages = await _context.UserMessageComment.Include(c => c.applicationUser).ToListAsync();
                return StatusCode(200,  messages );
            }
        }


        [Produces("application/json")]
        [HttpPost("set_rating")]
        public async Task<IActionResult> SetRating(
            string UserId, 
            int ComerceId,
            int Rating
        )
        {
           
            ApplicationUser user = await _context.ApplicationUser
                .Where(u => u.Id == UserId).FirstOrDefaultAsync();

            var comerce = await _context.Comerce.Where(c => c.ComerceId == ComerceId).FirstAsync();
            if (comerce != null)
            {
                var user_rating = new UserComerceRating
                {
                    UserId = user.Id,
                    ComerceId = ComerceId,
                    Rating = Rating
                };

                try
                {
                    _context.Add(user_rating);
                    await _context.SaveChangesAsync();

                    var message = await setRatingCalc(user_rating.ComerceId, comerce);
                    return StatusCode(200, new
                    {
                        Data = user_rating,
                        Message = message
                    });
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    return StatusCode(500, new { Message = "internal error " + ex.Message });
                }
            }
            else
                return StatusCode(404, new { Message = "Comercio no encontrado " });

                   
        }

        public async Task<string> setRatingCalc(int? comerceId, Comerce comerce)
        {
            var rating_comerce_list = await _context.UserComerceRating.Where(c=>c.ComerceId==comerceId).ToListAsync();

            int count = 0;
            int r = 0;
            foreach(var rating_comerce in rating_comerce_list)
            {
                r += rating_comerce.Rating;
                count++;
            }

            decimal d = Math.Round( Convert.ToDecimal(r/count));
            int rating = Decimal.ToInt32(d);

            comerce.Rating = rating;
            _context.Update(comerce);
            await _context.SaveChangesAsync();

            return "The new rating is: "+ rating ;
        }
    }
}
