﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using sng_web.Common;
using sng_web.Data;
using sng_web.Models;
using sng_web.Models.AccountViewModels;
using sng_web.Services;

namespace sng_web.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;

        //Upload Images to server
        private readonly IHostingEnvironment _iHostingEnvironment;

        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;

        public static string ROLE_ADMIN = "Admin";
        public static string ROLE_COMERCE = "Comerce";
        public static string ROLE_CLIENT = "Client";


        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ILogger<AccountController> logger,
            IHostingEnvironment env,
            ApplicationDbContext context,
            RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
            _context = context;
            _roleManager = roleManager;
            _iHostingEnvironment = env;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        
        //[Authorize(Roles = "Client")]
        [HttpGet]
        public IActionResult List()
        {
            var usuers = _context.ApplicationUser.ToList();
            return View(usuers);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(String id = null)
        {
            if (id == null)
            {
                return NotFound();
            }

            var usuario = await _userManager.FindByIdAsync(id);
            if (usuario == null)
            {
                return NotFound();
            }

            var user = new UserViewModel()
            {
                FirstName = usuario.FirstName,
                LastName = usuario.LastName,
                PhoneNumber = usuario.PhoneNumber,
                NickName = usuario.NickName,
                Position = usuario.Position,    
                AvatarURL = usuario.AvatarURL,
                Email = usuario.Email,
                ComerceId = usuario.ComerceId
            };
            

            user.Roles = await _userManager.GetRolesAsync(usuario);
            
            
            ViewBag.Roles = _context.Roles.ToList();
            ViewBag.Comercios = _context.Comerce.Where(c=> c.IsActived==true).ToList();

            if (TempData["mensaje_ok"] != null)
                ViewBag.mensaje_ok = TempData["mensaje_ok"].ToString();
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(UserViewModel user)
        {
            if (user.Id==null)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                Comerce Comercio = _context.Comerce.SingleOrDefault(cm=>cm.ComerceId==user.ComerceId);
                

                ApplicationUser usuario =  _context.ApplicationUser.Where(m => m.Id ==user.Id).ToList().FirstOrDefault();

                usuario.FirstName = user.FirstName;
                usuario.LastName = user.LastName;
                usuario.PhoneNumber = user.PhoneNumber;
                usuario.NickName = user.NickName;
                usuario.Position = user.Position;
                if ( Comercio != null) {
                    usuario.Comerce = Comercio;
                }
                

                var file = HttpContext.Request.Form.Files.FirstOrDefault(f => f.Name == "AvatarURL");
                if ((file != null && file.Length > 0) || usuario.AvatarURL == null)
                    usuario.AvatarURL = await Utilities.upload_fileAsync(HttpContext, "images/uploads/ccounts/", "/images/user.jpg");
                
                _context.Update(usuario);
                var result =await _context.SaveChangesAsync();

                if (result!=null)
                {
                    //SE GUARDAN LOS ROLES
                    var usuario_manager = await _userManager.FindByIdAsync(user.Id);
                    var roles_sistema = await _userManager.GetRolesAsync(usuario_manager);
                    var result2 = await _userManager.RemoveFromRolesAsync(usuario_manager, roles_sistema);
                    if(result2.Succeeded)
                        await _userManager.AddToRolesAsync(usuario, user.Roles);
                }
                //se actualiza el comercio relacionado con el usuario
                if (user.Comerce!=null){
                    await _userManager.AddClaimAsync(usuario, new Claim(CustomClaimTypes.ComerceId, Convert.ToString(user.Comerce.ComerceId)));
                }else{
                    IEnumerable<Claim> claims = await _userManager.GetClaimsAsync(usuario);
                    var CustomClaimTypes_ComerceId = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                    if (CustomClaimTypes_ComerceId != null)
                        await _userManager.RemoveClaimAsync(usuario,CustomClaimTypes_ComerceId);
                }
                /*if(usuario.Id==User.Identity.){
                    await _signInManager.RefreshSignInAsync(usuario);
                } */
                
            }
            TempData["mensaje_ok"] = "Datos actualizados exitosamente";
            return RedirectToAction(nameof(Edit), new { id= user.Id });
        }

        [HttpGet]
        public IActionResult Profile(String id = null)
        {
            if (id == null)
                return NotFound();

            var userId = _userManager.GetUserId(User);
            if (userId != id)
                return NotFound();

            var usuario = _context.ApplicationUser.SingleOrDefault(m => m.Id == id);
            if (usuario == null)
                return NotFound();

            //var user = new UserViewModel()
            //{
            //    Id = usuario.Id,
            //    FirstName = usuario.FirstName,
            //    LastName = usuario.LastName,
            //    PhoneNumber = usuario.PhoneNumber,
            //    NickName = usuario.NickName,
            //    Position = usuario.Position,
            //    AvatarURL = usuario.AvatarURL,
            //    Email = usuario.Email
            //};

            if (TempData["mensaje_ok"] != null)
                ViewBag.mensaje_ok = TempData["mensaje_ok"].ToString();
            return View(usuario);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditProfile(ApplicationUser user)
        {
            if (user.Id == null)            
                return NotFound();

            if (ModelState.IsValid)
            {
                ApplicationUser usuario = await _userManager.FindByIdAsync(user.Id);
                usuario.UserName = user.UserName;
                usuario.Email = user.Email;
                usuario.FirstName = user.FirstName;
                usuario.LastName = user.LastName;
                usuario.PhoneNumber = user.PhoneNumber;
                
                var file = HttpContext.Request.Form.Files.FirstOrDefault(f => f.Name == "AvatarURL");
                if ((file != null && file.Length > 0) || usuario.AvatarURL == null)
                    usuario.AvatarURL = await Utilities.upload_fileAsync(HttpContext, "images/uploads/ccounts/", "/images/user.jpg");

                var identity = (ClaimsIdentity) User.Identity;
                IEnumerable<Claim> claims = identity.Claims;
                var CustomClaimTypes_AvatarURL = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.AvatarURL);
                var CustomClaimTypes_GivenName = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.GivenName); 
                var CustomClaimTypes_Surname = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.Surname);

                await _userManager.RemoveClaimAsync(usuario, CustomClaimTypes_AvatarURL);
                await _userManager.RemoveClaimAsync(usuario, CustomClaimTypes_GivenName);
                await _userManager.RemoveClaimAsync(usuario, CustomClaimTypes_Surname);

                await _userManager.AddClaimAsync(usuario, new Claim(CustomClaimTypes.AvatarURL, usuario.AvatarURL));
                await _userManager.AddClaimAsync(usuario, new Claim(CustomClaimTypes.GivenName, usuario.FirstName));
                await _userManager.AddClaimAsync(usuario, new Claim(CustomClaimTypes.Surname, usuario.LastName));

                var result = await _userManager.UpdateAsync(usuario);

                await _signInManager.RefreshSignInAsync(usuario);

            }
            TempData["mensaje_ok"] = "Datos actualizados exitosamente";
            return RedirectToAction(nameof(Profile), new { id = user.Id });
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;

            var exist_roleAdmin = await _roleManager.RoleExistsAsync(ROLE_ADMIN);
            if (!exist_roleAdmin)
            {
                var role = new IdentityRole(ROLE_ADMIN);
                var response = await _roleManager.CreateAsync(role);

                if (response.Succeeded)
                {
                    _logger.LogInformation("Admin role created.");
                }
            }

            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                //var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                ApplicationUser signedUser = await _userManager.FindByEmailAsync(model.Email);
                //comentar en produccion sirve solo para asignar permiso al adinistrador por primera vez
                //await _userManager.AddToRoleAsync(signedUser, ROLE_ADMIN);
                var result = await _signInManager.PasswordSignInAsync(signedUser.UserName, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    _logger.LogInformation("User logged in.");
                        return RedirectToLocal(returnUrl);
                }
                if (result.RequiresTwoFactor)
                {
                    return RedirectToAction(nameof(LoginWith2fa), new { returnUrl, model.RememberMe });
                }
                if (result.IsLockedOut)
                {
                    _logger.LogWarning("User account locked out.");
                    return RedirectToAction(nameof(Lockout));
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Usuario o contraseña incorrecta.");
                    return View(model);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> LoginWith2fa(bool rememberMe, string returnUrl = null)
        {
            // Ensure the user has gone through the username & password screen first
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();

            if (user == null)
            {
                throw new ApplicationException($"Unable to load two-factor authentication user.");
            }

            var model = new LoginWith2faViewModel { RememberMe = rememberMe };
            ViewData["ReturnUrl"] = returnUrl;

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginWith2fa(LoginWith2faViewModel model, bool rememberMe, string returnUrl = null)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var authenticatorCode = model.TwoFactorCode.Replace(" ", string.Empty).Replace("-", string.Empty);

            var result = await _signInManager.TwoFactorAuthenticatorSignInAsync(authenticatorCode, rememberMe, model.RememberMachine);

            if (result.Succeeded)
            {
                _logger.LogInformation("User with ID {UserId} logged in with 2fa.", user.Id);
                return RedirectToLocal(returnUrl);
            }
            else if (result.IsLockedOut)
            {
                _logger.LogWarning("User with ID {UserId} account locked out.", user.Id);
                return RedirectToAction(nameof(Lockout));
            }
            else
            {
                _logger.LogWarning("Invalid authenticator code entered for user with ID {UserId}.", user.Id);
                ModelState.AddModelError(string.Empty, "Invalid authenticator code.");
                return View();
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> LoginWithRecoveryCode(string returnUrl = null)
        {
            // Ensure the user has gone through the username & password screen first
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load two-factor authentication user.");
            }

            ViewData["ReturnUrl"] = returnUrl;

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginWithRecoveryCode(LoginWithRecoveryCodeViewModel model, string returnUrl = null)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load two-factor authentication user.");
            }

            var recoveryCode = model.RecoveryCode.Replace(" ", string.Empty);

            var result = await _signInManager.TwoFactorRecoveryCodeSignInAsync(recoveryCode);

            if (result.Succeeded)
            {
                _logger.LogInformation("User with ID {UserId} logged in with a recovery code.", user.Id);
                return RedirectToLocal(returnUrl);
            }
            if (result.IsLockedOut)
            {
                _logger.LogWarning("User with ID {UserId} account locked out.", user.Id);
                return RedirectToAction(nameof(Lockout));
            }
            else
            {
                _logger.LogWarning("Invalid recovery code entered for user with ID {UserId}", user.Id);
                ModelState.AddModelError(string.Empty, "Invalid recovery code entered.");
                return View();
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Lockout()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        [NonAction]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    //extended properties
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    AvatarURL = "/images/user.png",
                    DateRegistered = DateTime.UtcNow.ToString(),
                    Position = "",
                    NickName = "",

                };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    _logger.LogInformation("User created a new account with password.");

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.EmailConfirmationLink(user.Id, code, Request.Scheme);
                    await _emailSender.SendEmailConfirmationAsync(model.Email, callbackUrl);

                    var exist_roleAdmin = await _roleManager.RoleExistsAsync(ROLE_ADMIN);
                    var exist_roleComerce = await _roleManager.RoleExistsAsync(ROLE_COMERCE);
                    var exist_roleClient = await _roleManager.RoleExistsAsync(ROLE_CLIENT);

                    //Admin Role 
                    if (!exist_roleAdmin)
                    {
                        var role = new IdentityRole(ROLE_ADMIN);
                        var response = await _roleManager.CreateAsync(role);

                        if (response.Succeeded)
                        {
                            _logger.LogInformation("Admin role created.");
                        }
                    }
                    

                    //Comerce Role
                    if (!exist_roleComerce)
                    {
                        var role = new IdentityRole(ROLE_COMERCE);
                        var response = await _roleManager.CreateAsync(role);

                        if (response.Succeeded)
                        {
                            _logger.LogInformation("Comerce role created.");
                        }
                    }

                    //Client Role --CREA Y ASIGNA AL USUARIO AL ROLE DE USUARIO--
                    if (!exist_roleClient)
                    {
                        var role = new IdentityRole(ROLE_CLIENT);
                        var response = await _roleManager.CreateAsync(role);

                        if(response.Succeeded)
                        {
                            _logger.LogInformation("Client role created.");
                            await _userManager.AddToRoleAsync(user, ROLE_CLIENT);
                            await _signInManager.SignInAsync(user, isPersistent: false);
                            _logger.LogInformation("User created a new account with password.");
                        }
                    }
                    else
                    {
                        await _userManager.AddToRoleAsync(user, ROLE_CLIENT);
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        _logger.LogInformation("User created a new account with password.");
                    }

                    _logger.LogInformation(3, "User created a new account with password.");

                    await _userManager.AddClaimAsync(user, new Claim(CustomClaimTypes.GivenName, user.FirstName));
                    await _userManager.AddClaimAsync(user, new Claim(CustomClaimTypes.Surname, user.LastName));
                    await _userManager.AddClaimAsync(user, new Claim(CustomClaimTypes.AvatarURL, user.AvatarURL));

                    return RedirectToLocal(returnUrl);
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            Response.Cookies.Delete(".AspNetCore.ApplicationCookie");
            _logger.LogInformation("User logged out.");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult ExternalLogin(string provider, string returnUrl = null)
        {
            // Request a redirect to the external login provider.
            var redirectUrl = Url.Action(nameof(ExternalLoginCallback), "Account", new { returnUrl });
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return Challenge(properties, provider);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
        {
            if (remoteError != null)
            {
                ErrorMessage = $"Error from external provider: {remoteError}";
                return RedirectToAction(nameof(Login));
            }
            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return RedirectToAction(nameof(Login));
            }

            // Sign in the user with this external login provider if the user already has a login.
            var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false, bypassTwoFactor: true);
            if (result.Succeeded)
            {
                //TODO: Comentar en produccion
                //esto es solo para dar permiso de administrador al usuario actual a fin de 
                //completar el menu de adminstrador
                //String email = info.Principal.FindFirstValue(ClaimTypes.Email);
                //var user = await _userManager.FindByEmailAsync(email);
                //await _userManager.AddToRoleAsync(user, ROLE_ADMIN);


                _logger.LogInformation("User logged in with {Name} provider.", info.LoginProvider);
                return RedirectToLocal(returnUrl);
            }
            if (result.IsLockedOut)
            {
                return RedirectToAction(nameof(Lockout));
            }
            else
            {
                // If the user does not have an account, then ask the user to create an account.
                ViewData["ReturnUrl"] = returnUrl;
                ViewData["LoginProvider"] = info.LoginProvider;
                var email = info.Principal.FindFirstValue(ClaimTypes.Email);
                return View("ExternalLogin", new ExternalLoginViewModel { Email = email });
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ExternalLoginConfirmation(ExternalLoginViewModel model, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await _signInManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    throw new ApplicationException("Error loading external login information during confirmation.");
                }
                var user = new ApplicationUser {
                    UserName = model.Email,
                    Email = model.Email,
                    FirstName = info.Principal.FindFirstValue(ClaimTypes.GivenName),
                    LastName = info.Principal.FindFirstValue(ClaimTypes.Surname),
                    AvatarURL = info.Principal.FindFirstValue(CustomClaimTypes.AvatarURL) ==null? "/images/user.png": info.Principal.FindFirstValue(CustomClaimTypes.AvatarURL),
                    DateRegistered = DateTime.UtcNow.ToString(),
                    Position = "",
                    NickName = "",
                };
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await _userManager.AddLoginAsync(user, info);


                    var exist_roleAdmin = await _roleManager.RoleExistsAsync(ROLE_ADMIN);
                    var exist_roleComerce = await _roleManager.RoleExistsAsync(ROLE_COMERCE);
                    var exist_roleClient = await _roleManager.RoleExistsAsync(ROLE_CLIENT);

                    //Admin Role 
                    if (!exist_roleAdmin)
                    {
                        var role = new IdentityRole(ROLE_ADMIN);
                        var response = await _roleManager.CreateAsync(role);

                        if (response.Succeeded)
                        {
                            _logger.LogInformation("Admin role created.");
                        }
                    }


                    //Comerce Role
                    if (!exist_roleComerce)
                    {
                        var role = new IdentityRole(ROLE_COMERCE);
                        var response = await _roleManager.CreateAsync(role);

                        if (response.Succeeded)
                        {
                            _logger.LogInformation("Comerce role created.");
                        }
                    }

                    //Client Role --CREA Y ASIGNA AL USUARIO AL ROLE DE USUARIO--
                    if (!exist_roleClient)
                    {
                        var role = new IdentityRole(ROLE_CLIENT);
                        var response = await _roleManager.CreateAsync(role);

                        if (response.Succeeded)
                        {
                            _logger.LogInformation("Client role created.");
                            await _userManager.AddToRoleAsync(user, ROLE_CLIENT);
                        }
                    }
                    else
                    {
                        await _userManager.AddToRoleAsync(user, ROLE_CLIENT);
                    }
                    

                    if (result.Succeeded)
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        _logger.LogInformation("User created an account using {Name} provider.", info.LoginProvider);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewData["ReturnUrl"] = returnUrl;
            return View(nameof(ExternalLogin), model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    ViewData["error_message"] = "La cuenta " + model.Email + " no esta registrada";
                    return View();
                }

                // For more information on how to enable account confirmation and password reset please
                // visit https://go.microsoft.com/fwlink/?LinkID=532713
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.ResetPasswordCallbackLink(user.Id, code, Request.Scheme);
                await _emailSender.SendEmailAsync(model.Email, "Resetear contrase",
                   $"Porfavor reinicie su contraseña haciendo click en este: <a href='{callbackUrl}'>link</a>");
                return RedirectToAction(nameof(ForgotPasswordConfirmation));
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string code = null)
        {
            if (code == null)
            {
                throw new ApplicationException("Un codigo de confirmacion debe ser provisto para resetear la contraseña.");
            }
            var model = new ResetPasswordViewModel { Code = code };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            if (model.Code == null && User.IsInRole("Admin")) {
                model.Code = await _userManager.GeneratePasswordResetTokenAsync(user);
            }
            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            AddErrors(result);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            model.Code = await _userManager.GeneratePasswordResetTokenAsync(user);
            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            AddErrors(result);
            return View();
        }

        public async Task<List<IdentityError>> ChangePasswordAjax(String id, String newpassword)
        {
            List<IdentityError> errors = new List<IdentityError>();
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                errors.Add(new IdentityError()
                {
                    Code = "Error",
                    Description = "¡Contraseña no pudo ser actualizada!"
                });
                return errors;
            }
            var code = await _userManager.GeneratePasswordResetTokenAsync(user);
            var result = await _userManager.ResetPasswordAsync(user, code, newpassword);
            if (result.Succeeded)
            {
                errors.Add(new IdentityError()
                {
                    Code = "Saved",
                    Description = "¡Contraseña actualizada exitosamente!"
                });
            }
            else
            {
                errors.Add(new IdentityError()
                {
                    Code = "Error",
                    Description = "La contraseña debe contener almenos: Una letra mayusculas, un caracter numerico y un caracter alfanumerico (.)"
                });
            }
            AddErrors(result);
            return errors;
        }



        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }


        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion
    }
}
