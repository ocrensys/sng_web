﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sng_web.Common;
using sng_web.Data;
using sng_web.Models;

namespace sng_web.Controllers
{
    public class VacantAplicationsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;



        public VacantAplicationsController(ApplicationDbContext context, 
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        // GET: VacantAplications
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> Index()
        {
            var applicationList = new List<VacantAplication>();
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            var CustomClaimTypes_AplicationVacants = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.AplicationVacants);

            var userId = _userManager.GetUserId(User);
            var usuario = await _userManager.FindByIdAsync(userId);

            if (!User.IsInRole("Admin"))
            {
                var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                if (comerce_claim == null)
                    return NotFound();

                var aplicaciones =await _context.VacantAplications
                    .Include(v => v.Vacant)
                    .Where(v => v.Vacant.ComerceId== Convert.ToInt32(comerce_claim.Value))
                    .OrderBy(v=>v.Reviewed)
                    .ThenBy(v=>v.AplicationDate)
                    .ToListAsync();

                applicationList = aplicaciones;
            }
            else
            {
                var applicationDbContext = await _context.VacantAplications
                    .Include(v => v.Vacant)
                    .OrderBy(v => v.Reviewed)
                    .ThenBy(v => v.AplicationDate)
                    .ToListAsync();
                foreach (var item in applicationDbContext)
                {
                    if (item.Vacant != null)
                        if (item.Vacant.Comerce == null)
                            item.Vacant.Comerce = await _context.Comerce.SingleOrDefaultAsync(m => m.ComerceId == item.Vacant.ComerceId);
                    applicationList.Add(item);
                }
            }

            var results = applicationList.Where(av => av.Reviewed == false);
            int  count = results.Count();

            if (CustomClaimTypes_AplicationVacants != null)
                await _userManager.RemoveClaimAsync(usuario, CustomClaimTypes_AplicationVacants);
            await _userManager.AddClaimAsync(usuario, new Claim(CustomClaimTypes.AplicationVacants, count.ToString()));
            await _signInManager.RefreshSignInAsync(usuario);
            await _signInManager.RefreshSignInAsync(usuario);

            return View(applicationList);
        }

        // GET: VacantAplications/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vacantAplication = await _context.VacantAplications
                .Include(v => v.Vacant)
                .SingleOrDefaultAsync(m => m.VacantAplicationId == id);
            if (vacantAplication == null)
            {
                return NotFound();
            }

            return View(vacantAplication);
        }

        // GET: VacantAplications/Create
        [Authorize(Roles = "Admin,Comerce")]
        public IActionResult Create()
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            if (!User.IsInRole("Admin"))
            {
                var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                ViewData["VacantId"] = new SelectList(_context.Vacant.Where(v=>v.ComerceId== Convert.ToInt32(comerce_claim.Value)), "VacantId", "Name");
            }
            else
            {
                ViewData["VacantId"] = new SelectList(_context.Vacant, "VacantId", "Name");
            }
            return View();
        }

        // POST: VacantAplications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> Create([Bind("VacantAplicationId,AplicationDate,Comments,VacantId,UrlCurriculum,Reviewed,Response")] VacantAplication vacantAplication)
        {
            if (ModelState.IsValid)
            {
                _context.Add(vacantAplication);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["VacantId"] = new SelectList(_context.Vacant, "VacantId", "Department", vacantAplication.VacantId);
            return View(vacantAplication);
        }

        // GET: VacantAplications/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vacantAplication = await _context.VacantAplications
                .Include(v=>v.Vacant).SingleOrDefaultAsync(m => m.VacantAplicationId == id);
            if (vacantAplication == null)
            {
                return NotFound();
            }

            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            if (!User.IsInRole("Admin"))
            {
                var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                ViewData["VacantId"] = new SelectList(_context.Vacant.Where(v => v.ComerceId == Convert.ToInt32(comerce_claim.Value)), "VacantId", "Name");
            }
            else
            {
                ViewData["VacantId"] = new SelectList(_context.Vacant, "VacantId", "Name");
            }
            
            return View(vacantAplication);
        }

        // POST: VacantAplications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Applicant,ApplicantPhone,ApplicantEmail,VacantAplicationId,AplicationDate,Comments,VacantId,UrlCurriculum,Reviewed,Response,UserId")] VacantAplication vacantAplication)
        {
            if (id != vacantAplication.VacantAplicationId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(vacantAplication);
                    await _context.SaveChangesAsync();

                    var identity = (ClaimsIdentity)User.Identity;
                    IEnumerable<Claim> claims = identity.Claims;
                    var CustomClaimTypes_AplicationVacants = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.AplicationVacants);
                    

                    var userId = _userManager.GetUserId(User);
                    var usuario = await _userManager.FindByIdAsync(userId);

                    if (usuario != null)
                    {
                        var count = 0;
                        long comerceId = 0;
                        if (!User.IsInRole("Admin"))
                        {
                            var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                            comerceId = Convert.ToInt32(comerce_claim.Value);
                        }
                        else
                        {
                            var vacant = await _context.Vacant
                                .FirstOrDefaultAsync(v => v.VacantId == vacantAplication.VacantId);
                            comerceId = Convert.ToInt32(vacant.ComerceId);
                        }
                        var aplications = await _context.VacantAplications
                            .Include(v => v.Vacant)
                            .Where(v => v.Reviewed == false)
                            .Where(v => v.Vacant.ComerceId == comerceId)
                            .ToListAsync();
                        count = aplications.Count;

                        if (CustomClaimTypes_AplicationVacants != null)
                            await _userManager.RemoveClaimAsync(usuario, CustomClaimTypes_AplicationVacants);
                        await _userManager.AddClaimAsync(usuario, new Claim(CustomClaimTypes.AplicationVacants, count.ToString()));
                        await _signInManager.RefreshSignInAsync(usuario);
                    }

                    
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VacantAplicationExists(vacantAplication.VacantAplicationId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["VacantId"] = new SelectList(_context.Vacant, "VacantId", "Department", vacantAplication.VacantId);
            return View(vacantAplication);
        }

        private void RefereshBadge()
        {

        }

        // GET: VacantAplications/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vacantAplication = await _context.VacantAplications
                .Include(v => v.Vacant)
                .SingleOrDefaultAsync(m => m.VacantAplicationId == id);
            if (vacantAplication == null)
            {
                return NotFound();
            }

            return View(vacantAplication);
        }

        // POST: VacantAplications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var vacantAplication = await _context.VacantAplications.SingleOrDefaultAsync(m => m.VacantAplicationId == id);
            _context.VacantAplications.Remove(vacantAplication);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool VacantAplicationExists(int id)
        {
            return _context.VacantAplications.Any(e => e.VacantAplicationId == id);
        }
    }
}
