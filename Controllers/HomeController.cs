﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using sng_web.Common;
using sng_web.Data;
using sng_web.Models;
using sng_web.Services;

namespace sng_web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;


        public HomeController(ApplicationDbContext context,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender iemailSender)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = iemailSender;
        }
        public async Task<IActionResult> Index()
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            var CustomClaimTypes_AplicationVacants = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.AplicationVacants);

            var userId = _userManager.GetUserId(User);
            if (userId != null)
            {
                var usuario = await _userManager.FindByIdAsync(userId);
                if (usuario != null)
                {
                    var count = 0;
                    if (!User.IsInRole("Admin"))
                    {
                        var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                        if (comerce_claim == null)
                            return View();
                        var aplicaciones = await _context.VacantAplications
                            .Include(v => v.Vacant)
                            .Where(v => v.Vacant.ComerceId == Convert.ToInt32(comerce_claim.Value))
                            .Where(v => v.Reviewed == false)
                            .OrderBy(v => v.Reviewed).ThenBy(v => v.AplicationDate).ToListAsync();
                        count = aplicaciones.Count;
                        
                    }
                    else
                    {
                        var applicationDbContext = await _context.VacantAplications
                            .Include(v => v.Vacant)
                            .Where(v => v.Reviewed == false)
                            .OrderBy(v => v.Reviewed).ThenBy(v => v.AplicationDate).ToListAsync();
                        count = applicationDbContext.Count;

                    }

                    if (CustomClaimTypes_AplicationVacants != null)
                        await _userManager.RemoveClaimAsync(usuario, CustomClaimTypes_AplicationVacants);
                    await _userManager.AddClaimAsync(usuario, new Claim(CustomClaimTypes.AplicationVacants, count.ToString()));
                    await _signInManager.RefreshSignInAsync(usuario);
                }
            }

            return View();
        }

        [HttpPost]
        public async Task<List<IdentityError>> SendEmailAsync(string email, string subject, string message)
        {

            List<IdentityError> errors = new List<IdentityError>();
            try
            {
                message = "<strong>Mensaje de: " + subject + "(" + email + ")</strong>: <br/><br/>" + message;
                await _emailSender.SendEmailAsync("sng.info.2018@gmail.com", subject, message);
                errors.Add(new IdentityError()
                {
                    Code = "Send",
                    Description = "¡Su correo ha sido enviado con exito!"
                });
            }
            catch(Exception ex)
            {
                errors.Add(new IdentityError()
                {
                    Code = "Error",
                    Description = "¡Verificar si los datos  son correctos al enviar su correo! \n"  + ex
                });
            }
            return errors;
        }


        [HttpPost]
        //[Route("home/send-email")]
        public async Task<List<IdentityError>> SubscribeAsync(string email)
        {
            List<IdentityError> errors = new List<IdentityError>();
            try
            {
                var Subscriptions = new Subscriptions(){
                    Email = email,
                    CreatedAt = DateTime.Now
                };
                _context.Add(Subscriptions);
                await _context.SaveChangesAsync();

                errors.Add(new IdentityError()
                {
                    Code = "Saved",
                    Description = "¡Suscripcion exitosa, ahora podras recibir actualizaciones de productos y ofertas exclusivas!"
                });
            }
            catch (Exception ex)
            {
                errors.Add(new IdentityError()
                {
                    Code = "Error",
                    Description = "¡Lo sentimos, NO fue posible suscribirse! -> " + ex,
                    
                });
            }
            return errors;
        }


        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
