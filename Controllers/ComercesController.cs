﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OneSignal.CSharp.SDK;
using OneSignal.CSharp.SDK.Resources;
using OneSignal.CSharp.SDK.Resources.Notifications;
using sng_web.Data;
using sng_web.Models;
using sng_web.Services;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.IO;
using sng_web.Common;
using System.Security.Claims;
using static sng_web.Models.ApplicationLog;
using sng_web.Models.ViewModels;

namespace sng_web.Controllers
{
    public class ComercesController : Controller
    {

        private readonly INotifications _notification;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly INotifications _notifications;
        private readonly ILogger _logger;

        private readonly RoleManager<IdentityRole> _roleManager;

        public static string ROLE_ADMIN = "Admin";
        public static string ROLE_COMERCE = "Comerce";
        public static string ROLE_CLIENT = "Client";



        private readonly ApplicationDbContext _context;

        //Upload Images to server
        private readonly IHostingEnvironment _iHostingEnvironment;

        public ComercesController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            INotifications notification,
            IEmailSender emailSender,
            ILogger<AccountController> logger,
            IHostingEnvironment env,
            ApplicationDbContext context,
            RoleManager<IdentityRole> roleManager)
        {

            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
            _roleManager = roleManager;

            _context = context;
            _notification = notification;

            _iHostingEnvironment = env;
        }

        //TODO: Config ajax method
        public async Task<List<IdentityError>> AddComerce(string Name, string Description, string Address, string Ruc, 
            string UrlLogo, int Categoryid, int OwnerId) {
            List<IdentityError> errors = new List<IdentityError>();
            Comerce comerce = new Comerce()
            {
                Name =  Name,
                Description =  Description,
                Address =  Address,
                Ruc =  Ruc,
                UrlLogo =  UrlLogo,
                CategoryId =  Categoryid,
                OwnerId =  OwnerId,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                Longitude = (long) 12.1328201,
                Latitude = (long) -86.2503967,
                Rating = 1,
                IsActived = true,
                IsAproved = false
            };
             _context.Add(comerce);
            await _context.SaveChangesAsync();

            errors.Add(new IdentityError()
            {
                Code = "Saved",
                Description = "Comercio guardado exitosamente."
            });

            return errors;
        }

        public async Task<List<IdentityError>> Updatecomerce(int idComerce, string  Name, string  Description, string  Address, string  Ruc, 
            string  UrlLogo, int  CategoryId, int  OwnerId, DateTime createdAt, Boolean isActived, Boolean isAproved)   
        {
            List<IdentityError> errors = new List<IdentityError>();
            Comerce comerce = new Comerce()
            {
                ComerceId = idComerce,
                Name = Name,
                Description = Description,
                Address = Address,
                Ruc = Ruc,
                UrlLogo = UrlLogo,
                CategoryId = CategoryId,
                OwnerId = OwnerId,
                CreatedAt = createdAt,
                UpdatedAt = DateTime.Now,
                Longitude = (long) 12.1328201,
                Latitude = (long) -86.2503967,
                Rating = 1,
                IsActived = isActived,
                IsAproved = isAproved
            };
            _context.Update(comerce);
            await _context.SaveChangesAsync();

            errors.Add(new IdentityError()
            {
                Code = "Saved",
                Description = "Comercio guardado exitosamente."
            });

            return errors;
        }

        // GET: GetComerces/
        public async Task<ICollection<Comerce>> GetComerces(int skip, int take, string orderBy)
        {
            var temp_comerces = await _context.Comerce.Include(c => c.Owner).Include(c => c.Category)
                                    .OrderBy(c => c.UpdatedAt)
                                    .ToListAsync();
            switch (orderBy)
            {
                case "any":
                    temp_comerces = await _context.Comerce.Include(c => c.Owner).Include(c => c.Category)
                                    //.Skip(skip)
                                    //.Take(take)
                                    .ToListAsync();
                    break;

                case "rating":
                    temp_comerces = await _context.Comerce.Include(c => c.Owner).Include(c => c.Category)
                                    //.Skip(skip)
                                    //.Take(take)
                                    .OrderByDescending(c => c.Rating)
                                    .ToListAsync();
                    break;

                case "createdAt":
                    temp_comerces = await _context.Comerce.Include(c => c.Owner).Include(c => c.Category)
                                    //.Skip(skip)
                                    //.Take(take)
                                    .OrderByDescending(c => c.CreatedAt)
                                    .ToListAsync();
                    break;

                case "see":
                    temp_comerces = await _context.Comerce.Include(c => c.Owner).Include(c => c.Category)
                                    //.Skip(skip)
                                    //.Take(take)
                                    .OrderByDescending(c => c.ComerceId)
                                    .ToListAsync();
                    break;

                default:
                    temp_comerces = await _context.Comerce.Include(c => c.Owner).Include(c => c.Category)
                                    //.Skip(skip)
                                    //.Take(take)
                                    .OrderByDescending(c => c.UpdatedAt)
                                    .ToListAsync();
                    break;


            }

            List<Comerce> comerces = new List<Comerce>();

            foreach ( var c in temp_comerces)
            {
                var products = await _context.Product.Where(product => product.Comerce.ComerceId == c.ComerceId).ToListAsync();
                if (products.Count > 0)
                    c.Products = products;
                else c.Products = new List<Product>();

                comerces.Add(c);
            }

            return (comerces);
        }

        public async Task<List<Comerce>> GetOnlyComerces()
        {
            var applicationDbContext = _context.Comerce.Include(c => c.Category)
                                                       .Include(c => c.Owner);

            return (await applicationDbContext.ToListAsync());
        }

        //   Action methods  :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        

        // GET: Comerces/ SOLO EL USUARIO ADMINISTRADOR PUEDE VER LA LISTA DE TODOS LOS COMERCIOS
        //Calma
        [Authorize(Roles ="Admin")]
        public async Task<IActionResult> Index()
        {
            await _context.Comerce.ToListAsync();
            return View();
        }

        public async Task<IActionResult> Activities()
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            if (!User.IsInRole("Admin"))
            {
                var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                ViewData["ComerceId"] = comerce_claim.Value;
            }
            await _context.ApplicationLog.ToListAsync();
            return View();
        }

        public IActionResult Reports()
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            if (!User.IsInRole("Admin"))
            {
                var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                ViewData["ComerceId"] = comerce_claim.Value;
            }
            return View();
        }


        public async Task<ICollection<ApplicationLogViewModel>> GetLogs()
        {
            var query = from l in _context.ApplicationLog
                   join u in _context.Users on l.UserId equals u.Id 
                   join c in _context.Comerce on l.ComerceId equals c.ComerceId
                   select new ApplicationLogViewModel()
                   { Firstname = u.FirstName,
                       LastName = u.LastName,
                       Date = l.Date,
                       Commerce = c.Name,
                       Message = l.Message,
                       Level = l.Level,
                       Action = l.Action
                   };
            
            var logs = await query.ToListAsync();
            return logs;
        }

        public async Task<ICollection<ApplicationLogViewModel>> GetLogsByComerce(int ComerceId, int skip, int take, string orderBy)
        {
            var query = from l in _context.ApplicationLog
                        where l.ComerceId == ComerceId
                        join u in _context.Users on l.UserId equals u.Id
                        join c in _context.Comerce on l.ComerceId equals c.ComerceId
                        select new ApplicationLogViewModel()
                        {
                            Firstname = u.FirstName,
                            LastName = u.LastName,
                            Date = l.Date,
                            Commerce = c.Name,
                            Message = l.Message,
                            Level = l.Level,
                            Action = l.Action
                        };

            var logs = await query.ToListAsync();
            return logs;
        }

        public ICollection<ApplicationLogViewModel> GetReports()
        {

            return null;
        }

        public async Task<IActionResult> GetUserComment()
        {

            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            if (User.IsInRole("Admin"))
            {
                string comerceId = HttpContext.Request.Query["comerceId"].ToString();
                if (comerceId!=null) {
                    var messages = await _context.UserMessageComment
                                            .Include(c => c.applicationUser)
                                            .Include(c=> c.comerce)
                                            .Where(c=>c.ComerceId==Convert.ToInt32(comerceId)).ToListAsync();
                    return StatusCode(200,  messages );
                }else {
                    var messages = await _context.UserMessageComment.Include(c => c.applicationUser).ToListAsync();
                    return StatusCode(200,  messages );
                }
                
            }
            else // is comerce roles
            {
                var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                var ComerceId = int.Parse(comerce_claim.Value);
                var messages = await _context.UserMessageComment.Include(c => c.applicationUser).ToListAsync();
                return StatusCode(200,  messages );
            }
        }

        public async  Task<List<IdentityError>> DeleteUserComment(int id)
        {
            List<IdentityError> errors = new List<IdentityError>();
            var message = await _context.UserMessageComment.Where(c=> c.Id == id).FirstOrDefaultAsync();
            _context.UserMessageComment.Remove(message);
            await _context.SaveChangesAsync();
            
            errors.Add(new IdentityError()
            {
                Code = "Deleted",
                Description = "Comentario eliminado exitosamente."
            });
            return errors; 
        }

        // GET: Comerces/Details/5
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
                return NotFound();

            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;

            //AQUI VALIDO QUE SI EL USUARIO NO ES ADMINISTRADOR
            if (!User.IsInRole("Admin"))
            {
                var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                //SOLO PUEDA VER EL DETALLE DE SU COMERCIO
                if(comerce_claim==null || Convert.ToInt32(comerce_claim.Value)!=id )
                    return BadRequest("Comercio no pertenece a este usuario, bad request");
            }

            TempData["url_comerce_details"] = "/Comerces/Details/";
            
            var comerce = await _context.Comerce
                .Include(c => c.Category)
                .Include(c => c.Owner)
                .SingleOrDefaultAsync(m => m.ComerceId == id);

            if (comerce == null)
                return NotFound();

            var products = await _context.Product.Where(product => product.Comerce.ComerceId == id).ToListAsync();
            var promotions = await _context.Promotion.ToListAsync();
            var cupons = await _context.Cupon.Where(cupon => cupon.ComerceId == id).ToListAsync();
            var vacants = await _context.Vacant.Where(vacant => vacant.ComerceId == id).ToListAsync();

            if (products.Count > 0)
                comerce.Products = products;
            else comerce.Products = new List<Product>();

            if (promotions.Count > 0)
                comerce.Promotions = promotions;
            else comerce.Promotions = new List<Promotion>();

            if (cupons.Count > 0)
                comerce.Cupons = cupons;
            else comerce.Cupons = new List<Cupon>();

            if (vacants.Count > 0)
                comerce.Vacants = vacants;
            else comerce.Vacants = new List<Vacant>();

            return View(comerce);
        }

        // GET: Comerces/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            var categories = _context.Category
                                    .OrderByDescending(p => p.CategoryId)
                                    .ToList();

            ViewData["CategoryId"] = new SelectList(categories, "CategoryId", "Name");
            ViewData["OwnerId"] = new SelectList(_context.Set<Owner>(), "OwnerId", "FullName");
            return View();
        }

        // POST: Comerces/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("ComerceId,Name,Description,Address,Ruc,Phone,Rating,UrlLogo,CreatedAt,UpdatedAt," +
            "IsAproved,IsActived,CategoryId,OwnerId")] Comerce c)
        {
            c.Longitude = (Double) 12.1328201;
            c.Latitude = (Double) (-86.2503967);

            Comerce comerce = new Comerce()
            {
                Name = c.Name,
                Description = c.Description,
                Phone = c.Phone,
                Address = c.Address,
                Ruc = c.Ruc,
                CategoryId = c.CategoryId,
                OwnerId = c.OwnerId,
                IsAproved = c.IsAproved,
                IsActived = c.IsActived,
                Rating = c.Rating,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                Longitude = c.Longitude,
                Latitude = c.Latitude
            };

            if (ModelState.IsValid)
            {
                var file = HttpContext.Request.Form.Files.FirstOrDefault(f => f.Name == "UrlLogo");
                if ((file != null && file.Length > 0) || comerce.UrlLogo == null)
                    comerce.UrlLogo = await Utilities.upload_fileAsync(HttpContext, "images/uploads/comerce/");
                //await UploadImage(comerce);
                _context.Add(comerce);
                await _context.SaveChangesAsync();
                await ApplicationLog.RegisterLog(_context, 
                    "Comercio", 
                    comerce.Name, 
                    ActionType.Crear, 
                    LevelType.Information, 
                    _userManager.GetUserId(User), 
                    Convert.ToInt32(comerce.ComerceId));

                return RedirectToAction(nameof(Index));
            }

            var categories = _context.Category
                                    .OrderByDescending(p => p.CategoryId)
                                    .ToList();

            ViewData["CategoryId"] = new SelectList(categories, "CategoryId", "Name", comerce.CategoryId);
            ViewData["OwnerId"] = new SelectList(_context.Set<Owner>(), "OwnerId", "FullName", comerce.OwnerId);
            return View(comerce);
        }

        public IActionResult Register()
        {
            var categories = _context.Category
                                    .OrderByDescending(p => p.CategoryId)
                                    .ToList();

            ViewData["CategoryId"] = new SelectList(categories, "CategoryId", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(UserComerce ucomerce)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = ucomerce.Email,
                    Email = ucomerce.Email,
                    //extended properties
                    FirstName = ucomerce.FirstName, //necesitamos el nombre del representatne de la empresa
                    LastName = ucomerce.LastName, //tambien el apellido
                    AvatarURL = "/images/user.png",
                    DateRegistered = DateTime.UtcNow.ToString(),
                    Position = "",
                    NickName = ""
                };

                var result = await _userManager.CreateAsync(user, ucomerce.Password);
                if (result.Succeeded)
                {
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.EmailConfirmationLink(user.Id, code, Request.Scheme);
                    await _emailSender.SendEmailConfirmationAsync(ucomerce.Email, callbackUrl);
                    var exist_roleComerce = await _roleManager.RoleExistsAsync(ROLE_COMERCE);

                    if (!exist_roleComerce)
                    {
                        var role = new IdentityRole(ROLE_COMERCE);
                        var response = await _roleManager.CreateAsync(role);

                        if (response.Succeeded)
                        {
                            _logger.LogInformation("Client role created.");
                            await _userManager.AddToRoleAsync(user, ROLE_COMERCE);
                            await _signInManager.SignInAsync(user, isPersistent: false);
                            _logger.LogInformation("User created a new account with password.");
                        }
                    }
                    else
                    {
                        await _userManager.AddToRoleAsync(user, ROLE_COMERCE);
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        _logger.LogInformation("User created a new account with password.");
                    }
                    _logger.LogInformation(3, "User created a new account with password.");

                    var owner = new Owner()
                    {
                        IsActive = true,
                        Name = ucomerce.FirstName,
                        LastName = ucomerce.LastName,
                        Phone = ucomerce.Phone,
                        Address = ucomerce.Address,
                    };

                    _context.Add(owner);
                    var ownerId = owner.OwnerId;
                    var lng = ucomerce.Longitud > 0 ? ucomerce.Longitud : 12.1328201;
                    var ltd = ucomerce.Latitud > 0 ? ucomerce.Latitud : -86.2503967;
                    Comerce comerce = new Comerce()
                    {
                        Name = ucomerce.Name,
                        Description = "",
                        Address = ucomerce.Address,
                        Ruc = ucomerce.Ruc,
                        CategoryId = ucomerce.CategoryId,
                        OwnerId = ownerId,
                        IsAproved = false,
                        IsActived = true,
                        Rating = 1,
                        CreatedAt = DateTime.Now,
                        UpdatedAt = DateTime.Now,
                        Longitude = (long) lng,
                        Latitude = (long) ltd,
                        Phone = ucomerce.Phone
                    };

                    
                    _context.Add(comerce);
                    await _context.SaveChangesAsync();

                    await _userManager.AddClaimAsync(user, new Claim(CustomClaimTypes.GivenName, user.FirstName));
                    await _userManager.AddClaimAsync(user, new Claim(CustomClaimTypes.Surname, user.LastName));
                    await _userManager.AddClaimAsync(user, new Claim(CustomClaimTypes.AvatarURL, user.AvatarURL));
                    await _userManager.AddClaimAsync(user, new Claim(CustomClaimTypes.ComerceId, Convert.ToString(comerce.ComerceId)));

                    user.Comerce = comerce;
                    await _userManager.UpdateAsync(user);
                    await _signInManager.RefreshSignInAsync(user);


                    return RedirectToAction("Details", new { id = comerce.ComerceId});
                }
                else
                {
                    foreach(var error in result.Errors)
                    {
                        ModelState.AddModelError("Email", error.Description);
                    }
                    
                }
                
            }

            var categories = _context.Category
                                    .OrderByDescending(p => p.CategoryId)
                                    .ToList();

            ViewData["CategoryId"] = new SelectList(categories, "CategoryId", "Name", ucomerce.CategoryId);
            return View(ucomerce);
        }

        // GET: Comerces/Edit/5
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
                return NotFound();

            var comerce = await _context.Comerce.SingleOrDefaultAsync(m => m.ComerceId == id);
            if (comerce == null)
                return NotFound();

            //Validate authentication from user
            ViewBag.Refer = Request.Headers["Referer"].ToString();
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;
            if (!User.IsInRole("Admin"))
            {
                var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);
                if (comerce_claim == null || Convert.ToInt32(comerce_claim.Value) != comerce.ComerceId)
                {
                    await ApplicationLog.RegisterLog(_context,
                            "Comercio",
                            comerce.Name,
                            ActionType.Actualizar,
                            LevelType.Error,
                            _userManager.GetUserId(User),
                            Convert.ToInt32(comerce.ComerceId));

                    return NotFound();
                }
            }

            ViewData["Categoria"] = new SelectList(_context.Category, "CategoryId", "Name", comerce.CategoryId);
            ViewData["Propietario"] = new SelectList(_context.Set<Owner>(), "OwnerId", "FullName", comerce.OwnerId);
            ViewBag.Refer = Request.Headers["Referer"].ToString();
            return View(comerce);
        }

        // POST: Comerces/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Comerce")]
        public async Task<IActionResult> Edit(long id, [Bind("ComerceId,Name,Description,Address,Ruc,Phone,Rating,UrlLogo,CreatedAt,UpdatedAt,Longitude,Latitude,IsAproved,IsActived,CategoryId,OwnerId")] Comerce comerce)
        {
            if (id != comerce.ComerceId)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    var file = HttpContext.Request.Form.Files.FirstOrDefault(f => f.Name == "UrlLogo");
                    if ((file != null && file.Length > 0) || comerce.UrlLogo == null)
                        comerce.UrlLogo = await Utilities.upload_fileAsync(HttpContext, "images/uploads/comerce/");

                    comerce.UpdatedAt = DateTime.Now;
                    //todo: Check if comerce is aproved ? send notifications
                    var oldcommerce= _context.Comerce.AsNoTracking().Where(cm=>cm.ComerceId==comerce.ComerceId).FirstOrDefault();
                    //if commerce isAproved status has changed to aproved so we send the notification
                    if(!oldcommerce.IsAproved && comerce.IsAproved)
                    {
                        var data = new Dictionary<string,string>(){
                            {"Model", "comerce"},
                            {"ObjectId", comerce.ComerceId.ToString()},
                            {"ComerceId", comerce.ComerceId.ToString()}
                        };
                        await _notification.CreateNotification("¡Un nuevo Comercio es parte de Saves N' GO!", data);
                    }
                    _context.Update(comerce);
                    await _context.SaveChangesAsync();
                    await ApplicationLog.RegisterLog(_context, 
                            "Comercio", 
                            comerce.Name, 
                            ActionType.Actualizar, 
                            LevelType.Information, 
                            _userManager.GetUserId(User), 
                            Convert.ToInt32(comerce.ComerceId));
                    
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ComerceExists(comerce.ComerceId))
                    {
                        await ApplicationLog.RegisterLog(_context,
                            "Comercio",
                            comerce.Name,
                            ActionType.Actualizar,
                            LevelType.Error,
                            _userManager.GetUserId(User),
                            Convert.ToInt32(comerce.ComerceId));
                        return NotFound();
                    }
                    else
                        throw;
                }

                if (Request.Form["Refer"].ToString() != "")
                    return Redirect(Request.Form["Refer"].ToString());
                else
                    return RedirectToAction(nameof(Index));
            }

            ViewData["CategoryId"] = new SelectList(_context.Category, "CategoryId", "Name", comerce.CategoryId);
            ViewData["OwnerId"] = new SelectList(_context.Set<Owner>(), "OwnerId", "FullName", comerce.OwnerId);

            return View(comerce);
        }

        // GET: Comerces/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
                return NotFound();

            var comerce = await _context.Comerce.Include(c => c.Category)
                                                .Include(c => c.Owner)
                                                .SingleOrDefaultAsync(m => m.ComerceId == id);

            if (comerce == null)
            {
                await ApplicationLog.RegisterLog(_context,
                            "Comercio",
                            comerce.Name,
                            ActionType.Borrar,
                            LevelType.Error,
                            _userManager.GetUserId(User),
                            Convert.ToInt32(comerce.ComerceId));
                return NotFound();
            }

            return View(comerce);
        }

        // POST: Comerces/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var comerce = await _context.Comerce.SingleOrDefaultAsync(m => m.ComerceId == id);
            _context.Comerce.Remove(comerce);
            await _context.SaveChangesAsync();
            await ApplicationLog.RegisterLog(_context,
                            "Comercio",
                            comerce.Name,
                            ActionType.Borrar,
                            LevelType.Information,
                            _userManager.GetUserId(User),
                            Convert.ToInt32(comerce.ComerceId));

            return RedirectToAction(nameof(Index));
        }

        private bool ComerceExists(long id)
        {
            return _context.Comerce.Any(e => e.ComerceId == id);
        }
    }
}
