[1mdiff --git a/ViewComponents/MenuMessageViewComponent.cs b/ViewComponents/MenuMessageViewComponent.cs[m
[1mindex a59b344..8757479 100644[m
[1m--- a/ViewComponents/MenuMessageViewComponent.cs[m
[1m+++ b/ViewComponents/MenuMessageViewComponent.cs[m
[36m@@ -32,27 +32,29 @@[m [mnamespace sng_web.ViewComponents[m
         //private async  Task<IEnumerable<UserMessageComment>> GetData()[m
         private async Task<IEnumerable<UserMessageComment>> GetData()[m
         {[m
[31m-            var messages = new List<UserMessageComment>();[m
 [m
             var identity = (ClaimsIdentity)User.Identity;[m
             IEnumerable<Claim> claims = identity.Claims;[m
             if (User.IsInRole("Admin"))[m
             {[m
[31m-                messages = await _context.UserMessageComment.ToListAsync();[m
[32m+[m[32m                var messages = await _context.UserMessageComment[m
[32m+[m[32m                                            .Include(c => c.applicationUser)[m
[32m+[m[32m                                            .Include(c=> c.comerce)[m
[32m+[m[32m                                            .ToListAsync();[m
[32m+[m
[32m+[m[32m                return messages;[m
             }[m
             else // is comerce roles[m
             {[m
                 var comerce_claim = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ComerceId);[m
                 var ComerceId = int.Parse(comerce_claim.Value);[m
[31m-                messages = await _context.UserMessageComment[m
[31m-                    .Where(comment => comment.ComerceId == ComerceId)[m
[31m-                    .ToListAsync();[m
[32m+[m[32m               var messages = await _context.UserMessageComment[m
[32m+[m[32m                                            .Include(c => c.applicationUser)[m
[32m+[m[32m                                            .Include(c=> c.comerce)[m
[32m+[m[32m                                            .Where(comment => comment.ComerceId == ComerceId)[m
[32m+[m[32m                                            .ToListAsync();[m
[32m+[m[32m                return messages;[m
             }[m
[31m-[m
[31m-            //messages = await _context.UserMessageComment.ToListAsync();[m
[31m-[m
[31m-[m
[31m-            return messages;[m
         }[m
     }[m
 }[m
[1mdiff --git a/Views/Shared/Components/MenuMessage/Default.cshtml b/Views/Shared/Components/MenuMessage/Default.cshtml[m
[1mindex cd910b2..b0ac573 100644[m
[1m--- a/Views/Shared/Components/MenuMessage/Default.cshtml[m
[1m+++ b/Views/Shared/Components/MenuMessage/Default.cshtml[m
[36m@@ -24,6 +24,7 @@[m
                         <br />[m
                         <h4>[m
                             @message.DisplayName[m
[32m+[m[41m                            [m
                         </h4>[m
                         <!-- The message -->[m
                         <p>@message.Comment</p>[m
