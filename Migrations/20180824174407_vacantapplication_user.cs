﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace sng_web.Migrations
{
    public partial class vacantapplication_user : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "VacantAplications",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VacantAplications_UserId",
                table: "VacantAplications",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_VacantAplications_AspNetUsers_UserId",
                table: "VacantAplications",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VacantAplications_AspNetUsers_UserId",
                table: "VacantAplications");

            migrationBuilder.DropIndex(
                name: "IX_VacantAplications_UserId",
                table: "VacantAplications");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "VacantAplications");
        }
    }
}
