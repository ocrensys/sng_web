﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace sng_web.Migrations
{
    public partial class add_comment_model : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserMessageComment",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    AvatarURL = table.Column<string>(nullable: true),
                    ComerceId = table.Column<int>(nullable: true),
                    ComerceId1 = table.Column<long>(nullable: true),
                    Comment = table.Column<string>(maxLength: 500, nullable: true),
                    DisplayName = table.Column<string>(maxLength: 100, nullable: true),
                    TimeSpan = table.Column<DateTime>(nullable: false),
                    URLPath = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: true),
                    applicationUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserMessageComment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserMessageComment_Comerce_ComerceId1",
                        column: x => x.ComerceId1,
                        principalTable: "Comerce",
                        principalColumn: "ComerceId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserMessageComment_AspNetUsers_applicationUserId",
                        column: x => x.applicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserMessageComment_ComerceId1",
                table: "UserMessageComment",
                column: "ComerceId1");

            migrationBuilder.CreateIndex(
                name: "IX_UserMessageComment_applicationUserId",
                table: "UserMessageComment",
                column: "applicationUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserMessageComment");
        }
    }
}
