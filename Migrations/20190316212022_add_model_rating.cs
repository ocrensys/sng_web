﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace sng_web.Migrations
{
    public partial class add_model_rating : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserComerceRating",
                columns: table => new
                {
                    UserComerceRatingId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ComerceId = table.Column<int>(nullable: true),
                    ComerceId1 = table.Column<long>(nullable: true),
                    Rating = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: true),
                    applicationUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserComerceRating", x => x.UserComerceRatingId);
                    table.ForeignKey(
                        name: "FK_UserComerceRating_Comerce_ComerceId1",
                        column: x => x.ComerceId1,
                        principalTable: "Comerce",
                        principalColumn: "ComerceId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserComerceRating_AspNetUsers_applicationUserId",
                        column: x => x.applicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserComerceRating_ComerceId1",
                table: "UserComerceRating",
                column: "ComerceId1");

            migrationBuilder.CreateIndex(
                name: "IX_UserComerceRating_applicationUserId",
                table: "UserComerceRating",
                column: "applicationUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserComerceRating");
        }
    }
}
