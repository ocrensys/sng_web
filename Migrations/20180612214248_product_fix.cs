﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace sng_web.Migrations
{
    public partial class product_fix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Product_Comerce_ComerceId1",
                table: "Product");

            migrationBuilder.DropIndex(
                name: "IX_Product_ComerceId1",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "ComerceId1",
                table: "Product");

            migrationBuilder.AlterColumn<long>(
                name: "ComerceId",
                table: "Product",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_Product_ComerceId",
                table: "Product",
                column: "ComerceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Product_Comerce_ComerceId",
                table: "Product",
                column: "ComerceId",
                principalTable: "Comerce",
                principalColumn: "ComerceId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Product_Comerce_ComerceId",
                table: "Product");

            migrationBuilder.DropIndex(
                name: "IX_Product_ComerceId",
                table: "Product");

            migrationBuilder.AlterColumn<int>(
                name: "ComerceId",
                table: "Product",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ComerceId1",
                table: "Product",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Product_ComerceId1",
                table: "Product",
                column: "ComerceId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Product_Comerce_ComerceId1",
                table: "Product",
                column: "ComerceId1",
                principalTable: "Comerce",
                principalColumn: "ComerceId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
