﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace sng_web.Migrations
{
    public partial class user_favorites : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "User_Favorite_Comerces",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ComerceId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_Favorite_Comerces", x => new { x.Id, x.ComerceId });
                    table.ForeignKey(
                        name: "FK_User_Favorite_Comerces_Comerce_ComerceId",
                        column: x => x.ComerceId,
                        principalTable: "Comerce",
                        principalColumn: "ComerceId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_User_Favorite_Comerces_AspNetUsers_Id",
                        column: x => x.Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "User_Favorite_Products",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ProductId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_Favorite_Products", x => new { x.Id, x.ProductId });
                    table.ForeignKey(
                        name: "FK_User_Favorite_Products_AspNetUsers_Id",
                        column: x => x.Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_User_Favorite_Products_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_User_Favorite_Comerces_ComerceId",
                table: "User_Favorite_Comerces",
                column: "ComerceId");

            migrationBuilder.CreateIndex(
                name: "IX_User_Favorite_Products_ProductId",
                table: "User_Favorite_Products",
                column: "ProductId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "User_Favorite_Comerces");

            migrationBuilder.DropTable(
                name: "User_Favorite_Products");
        }
    }
}
