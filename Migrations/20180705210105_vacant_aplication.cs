﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace sng_web.Migrations
{
    public partial class vacant_aplication : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "VacantAplications",
                columns: table => new
                {
                    VacantAplicationId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    AplicationDate = table.Column<DateTime>(nullable: false),
                    Comments = table.Column<string>(maxLength: 200, nullable: false),
                    Response = table.Column<string>(maxLength: 200, nullable: true),
                    Reviewed = table.Column<bool>(nullable: false),
                    UrlCurriculum = table.Column<string>(nullable: true),
                    VacantId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VacantAplications", x => x.VacantAplicationId);
                    table.ForeignKey(
                        name: "FK_VacantAplications_Vacant_VacantId",
                        column: x => x.VacantId,
                        principalTable: "Vacant",
                        principalColumn: "VacantId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VacantAplications_VacantId",
                table: "VacantAplications",
                column: "VacantId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VacantAplications");
        }
    }
}
